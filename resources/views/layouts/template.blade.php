<!DOCTYPE html>
<html lang="en">
<head>
    <title>چهارسو همکار </title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="default">
	<meta http-equiv="Content-Security-Policy" content="default-src * 'self' 'unsafe-inline' 'unsafe-eval' data: gap:">
	<link rel="shortcut icon" href="{{asset('webapp-assets/images/charsoo_logo.webp')}}">
	<link rel="apple-touch-icon" href="{{asset('webapp-assets/images/charsooiconapple.png')}}">
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700,900&display=swap" rel="stylesheet">
	<!-- <link rel="stylesheet" href="webapp-assets/css/bootstrap-rtl.min.css"> -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="{{asset('webapp-assets/css/font-awesome.css')}}">
	<link rel="stylesheet" href="{{asset('webapp-assets/css/style.css')}}">
	<!---media--->
	<link media="only screen and (min-width:0px) and (max-width:327px)"rel="stylesheet" href="{{asset('webapp-assets/css/mobile.css')}}" type="text.css">
	<link media ="only screen and (min-width:328px) and (max-width:768px)"rel="stylesheet" href="{{asset('webapp-assets/css/tablet.css')}}" type="text.css">
	<!---media-->
	<link rel="stylesheet" href="{{asset('webapp-assets/css/newStyles.css')}}">
	<link rel="stylesheet" href="{{asset('webapp-assets/css/4sooStyle.css')}}">
	<link rel="stylesheet" href="{{asset('webapp-assets/css/rtlStyle.css')}}">
	<link rel="stylesheet" href="{{asset('webapp-assets/css/ScrollbarStyle.css')}}">
	<link rel="stylesheet" href="{{asset('webapp-assets/plugins/number/app/css/handle-counter.css')}}">
	<link rel="stylesheet" href="{{asset('webapp-assets/plugins/alertify/themes/alertify.core.css')}}">
	<link rel="stylesheet" href="{{asset('webapp-assets/plugins/alertify/themes/alertify.default.css')}}">
	<link rel="stylesheet" href="{{asset('webapp-assets/plugins/Scripts/bootstrap.min.css')}}">

	<link rel="stylesheet" href="{{asset('webapp-assets/plugins/datetimepicker/css/persianDatepicker-default.css')}}">




   


<style type="text/css">
	.se-pre-con {
		position: fixed;
		left: 0px;
		top: 0px;	
		width: 100%;
		height: 100%;
		z-index: 9999;
		background: url('{{asset('webapp-assets/images/load.gif')}}') center no-repeat #fff;
		/*display: none;*/
	}
	.add-button {
	  position: absolute;
	  top: 1px;
	  left: 1px;
	}
	.popover{
		max-width:500px !important;
		font-family:tahoma !important;
	}
	.fade:not(.show) {
		opacity: 1 !important;
	}
	.image-magnifier-el{
		background: #111625ee;
		width: 100vw;
		height: 100vh;
		position: fixed;
		top:0;
		left:0;
		bottom:0;
		right:0;
		z-index: 12000;
		padding:0;
		margin:0;
		display: none;
	}
	.image-magnifier-el .close-image-magnifier{
		position: absolute;
		top: 0;
		right: 0;
	}
	.image-magnifier-el .close-image-magnifier i{
		font-size: 35px;
		text-align: right;
		color: rgb(240, 58, 58);
		padding: 15px 25px;
		cursor: pointer;
		transition: 0.3s;
	}
	.image-magnifier-el .close-image-magnifier i:hover{
		font-size: 45px;
		color: rgb(235, 110, 110);
	}
	.image-magnifier-el .image-magnifier-wrapper{
		width: 100%;
		height: 100%;
	}
	.image-magnifier-el .image-magnifier-wrapper img{
		width: 100%;
		height: 100%;
		object-fit: contain;
		margin: auto;
	}
    </style>
</head>
<body>

    @yield('content')

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://github.com/Modernizr/Modernizr/raw/master/modernizr.js"></script>
	<script src="{{asset('webapp-assets/js/jquery.scrollbar.js')}}"></script>
	<script src="{{asset('webapp-assets/plugins/number/app/js/handleCounter.js')}}"></script>
	<script src="{{asset('webapp-assets/js/scripts.js')}}"></script>
	<script src="{{asset('webapp-assets/js/hsscript.js')}}"></script>
	<script src="{{asset('webapp-assets/plugins/alertify/lib/alertify.min.js')}}"></script>
	<script src="{{asset('webapp-assets/js/number-divider.js')}}"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

	<script src="{{asset('webapp-assets/plugins/datetimepicker/js/jquery-1.10.1.min.js')}}"></script>
	<script src="{{asset('webapp-assets/plugins/datetimepicker/js/persianDatepicker.min.js')}}"></script>

	
	


	@yield('js')
	<script>

		$('.divide').divide({delimiter: ',',
		divideThousand: true});
	</script>
	@if (session('success'))
		<script>
		$(document).ready(function(){
		
		function reset() {
		alertify.set({
		labels : {
		ok     : "OK",
		cancel : "Cancel"
		},
		delay : 5000,
		buttonReverse : false,
		buttonFocus   : "ok"
		});
		}
		reset();
		alertify.success('{{ session("success") }}');
		return false;
		})
		</script>
	@endif
	@if (session('error'))
		<script>
		$(document).ready(function(){
		function reset() {
		alertify.set({
		labels : {
		ok     : "OK",
		cancel : "Cancel"
		},
		delay : 5000,
		buttonReverse : false,
		buttonFocus   : "ok"
		});
		}
		reset();
		alertify.error('{{ session("error") }}');
		return false;
		})
		</script>
		
		
	@endif
<script>
	
	$(document).ready(function(){

		// Animate loader off screen
		$(".se-pre-con").fadeOut();
		$(".loader").submit(function(){
			$(".se-pre-con").show();
		});

		$("#openModal").click(function(){
			$(".my-modal").fadeIn();
		});
		$("#closeModal").click(function(){
			$(".my-modal").fadeOut();
		});

		$(function() {
			$("#input1, #span1").persianDatepicker();       
		});

		$(function() {
			$("#input1").persianDatepicker();       
		});


		
	
	});
	
</script>
	
</body>
</html>