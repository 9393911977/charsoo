	@extends('layouts.template')
	@section('content')
	
    <div id="tab-home" class="tab tab-active tab-home" style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}') ; background-size:cover;">
   		
   		<div class="big-div-second-page">
    	<div class="register-message"><span>شماره شما در چهارسو ثبت نشده </span></div>
    	<div class="compietion-message">
    		<span>لطفا برای تکمیل اطلاعات خود فرم زیر را کامل کنید</span></div>

    	<div class="inputs-box">
			<form action="{{route('submitRegister')}}" method="post" id="registerForm">
			@csrf
				<input type="text" placeholder="نام"  name="firstname" id="firstname">
				<input type="text" placeholder="نام خانوادگی"  name="lastname" id="lastname">
				<input type="hidden" name="number" value="{{$phone}}">
				<!-- <input type="text" placeholder="شهر خود را وارد کنید">  -->
				<select class="cities" name="cities" id="cities">
					<option   value="" >شهر خود را انتخاب کنید</option>
					@foreach($result['data'] as $city)
					<option value="{{$city['city_name']}}"  name="city">{{$city['city_name']}}</option>
					@endforeach
				</select>
				<div class="accept-message">
					<div class="check-div"><input type="checkbox" class="check-box-login" 
					id="checkRegister" value="1">
					</div>
					<span>با ثبت نام در چهارسو قوانین و ضوابط چهارسو را می پذیرم</span>
				</div>

				<div class="register-btn"> <button type="submit" id="registerBtn">ثبت نام</button> </div>
			</form>
    	</div>

    	</div>

    	<div class="footer-image"><img src="{{asset('webapp-assets\images\login_bg_bottom.webp')}}" alt="">
    	</div>

    </div>

	@endsection

	@section('js')

	@endsection
