	@extends('layouts.template')
	@section('content')
	
    <div id="tab-home" class="tab tab-active tab-home" style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}') ; background-size:cover;">
			<div class="wellcome"><span>به چهارسو خوش آمدید</span></div>
			<div class="logo-image"><img src="{{asset('webapp-assets\images\logo-image.png')}}" alt=""></div>
			<div class="enter-phone-number-message"><span>شماره تلفن همراه خود را وارد کنید</span></div>

			<div class="input-phone-number">
				<form action="{{route('submitPersonalNumber')}}" method="post">
					@csrf
					<input required type="number" placeholder="09 - - - - - - - - -"  name="phone" id="number" class="getnumber">
				<div class="box-assurance">
					<div class="assurance-message">
						<div class="container-assurance-message">
							<span>
								<img src="{{asset('webapp-assets\images\ic_privacy_person.png')}}" alt="">
							شماره شما نزد ما محفوظ است و بدون اجازه شما آن را در اختیار شخص دیگری قرار نخواهیم داد.
							</span>
						</div>
					</div>
				</div>

				<div class="continue-btn"> <button type="submit">ادامه</button> </div>
				<div class="temp"></div>
				</form>
				<!---bottom image---->
			</div>
		<div class="footer-image"><img src="{{asset('webapp-assets\images\login_bg_bottom.webp')}}" alt="">
		</div>
    </div>
	@endsection

