	@extends('layouts.template')
	@section('content')
	
    <div id="tab-home" class="tab tab-active tab-home" style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}') ; background-size:cover;">
		<div class="container-box">
			<div  class="enter-phone-number-message" style="line-height: 2.2;margin-top: 42px;">
				<span>کد تایید را وارد کنید</span><br>
				<span style="font-size: 10px;line-height: 2.2;">یک کد چهار رقمی برای  شماره زیر  ارسال شد</span><br>
				<span style="font-size: 16px;line-height: 2.2;">{{$phone}}</span><br>
				<form action="{{route('submitCode')}}" method="post" id="code_form">
					@csrf
					<div class="input-phone-number">
						<input type="text" required placeholder="-  -  -  -"  name="code" id="four_code">
					</div>
					<input type="hidden" value="{{$phone}}" name="phone_number">
				<div class="div-ok-btn"> 
					<button type="submit" class="ok-btn" id="ok_btn">تایید</button> 
				</div>
				<!---bottom image---->
				</form>
				<br><br><br><br><br><br><br>
				
			</div>
		</div>
		<div class="footer-image" style="position: fixed;margin-top: 0;"><img src="{{asset('webapp-assets\images\login_bg_bottom.webp')}}" alt="">
		</div>
	</div>
	@endsection

	@section('js')
	@endsection