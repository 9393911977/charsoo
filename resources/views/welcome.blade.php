﻿@extends('layouts.template')
@section('content')

<div id="tab-home" class="tab tab-active tab-home" style="background-image:url('webapp-assets/images/login_bg.png') ; background-size:cover;">


        <!-- navbar home -->
        <div class="navbar-wrapper">
        	<div class="img-div">
        		<img src="webapp-assets\images\topbackground2.png" alt="">
        	</div>
            
            <div class="nav-titr" style="background-image: url('webapp-assets/images/top-bg.png');">
            	<p class="bolder">چهارسو</p>
            	<p class="titr-day">چهارشنبه 26 شهریور</p>
            	<div class="toggle">
            		<p class="checker">آفلاین</p>
            		<div class="sub-toggle"></div>
            	</div>
            </div>
                
        
        	<div class="big-box">
            	<div class="container-fluid p-0">
	                    
                    <div class="row img">
                        <div class="col-md-4 col-sm-4 col-xs-4 p-0 card">
                        	<img src="webapp-assets\images\person.png" class="person" alt="">
                        	<p class="p-person-name">حمزه سهرابی </p><br>
                        	<p class="p-person-enter"href="">ورود به پروفایل</p>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-4 p-0 card">
                        	<p>درآمد</p>
                        	<p class="nmb">70</p>
                        	<p class="const">هزار تومان</p>
                        	<p style="color:#2b368c;font-weight: bold;">گردش درآمد</p>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-4 p-0 card">
                        	<p>شارژ</p>
                        	<p class="nmb">100</p>
                        	<p class="const">هزار تومان</p>
                        	<p style="color:#2b368c;font-weight: bold;">افزایش شارژ</p>
                        </div>
					</div>

					<div class="row hrz">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 scard"> <img src="webapp-assets\images\headset.png" alt="" class="hrz-img"><div class="hrz-p">پشتیبانی</div>
                    	</div>

                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 scard"><img src="webapp-assets\images\c-cart.png" class="hrz-img"alt=""><div class="hrz-p">وضعیت حساب بانکی</div> 
                    	</div>
					</div>

					<div class="row ">

	                    <div class="col md-12 col sm-12 t" >
			                 <span class="title-body-right">اطلاعیه 1</span>
			                  <span class="title-body-left"> 1397/12/18</span>
			                  <br>
			                  <br>
		                    <div class="text-body"> 
							لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.چاپگرها ومتون بلکه روزنامه ومجله در ستون و سطر آنچنان که لازم است و برای شرایط فعلی 
تکنولوژی مورد نیاز و کاربردهای متوع با هدف بهبود ابزار های کاربردی می باشد.
							</div>
							<div class="news">لینک اطلاعیه</div>
	                    </div>
                    </div>

                    <div class="row ">

	                    <div class="col md-12 col sm-12 t" >
			                 <div class="title-body-right">اطلاعیه 2</div>
			                  <div class="title-body-left"> 18/12/1397</div>
			                  <br>
			                  <br>
		                    <div class="text-body"> 
							لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.چاپگرها ومتون بلکه روزنامه ومجله در ستون و سطر آنچنان که لازم است و برای شرایط فعلی 
تکنولوژی مورد نیاز و کاربردهای متوع با هدف بهبود ابزار های کاربردی می باشد.
							</div>
							<div class="news">لینک اطلاعیه</div>
	                    </div>
                    </div>

                    <div class="row ">

	                    <div class="col md-12 col sm-12 t" >
			                 <div class="title-body-right">اطلاعیه 3</div>
			                  <div class="title-body-left"> 18/12/1397</div>
			                  <br>
			                  <br>
		                    <div class="text-body"> 
							لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.چاپگرها ومتون بلکه روزنامه ومجله در ستون و سطر آنچنان که لازم است و برای شرایط فعلی 
تکنولوژی مورد نیاز و کاربردهای متوع با هدف بهبود ابزار های کاربردی می باشد.
							</div>
							<div class="news">لینک اطلاعیه</div>
	                    </div>
                    </div>


		            <div class="row"><!---state--->
	            		<div class="col md-4 col sm-4 s "><img src="webapp-assets\images\chart.png" alt=""><div class="tlonger">آمار و  جزییات فعالیت</div></div>
	            		<div class="col md-4 col sm-4 s "><img src="webapp-assets\images\helphand.png" alt=""><div>مهارت های من</div></div>
	            		<div class="col md-4 col sm-4 s "><img src="webapp-assets\images\gift.png" alt=""><div>شارژ رایگان</div></div>
            		</div><!----end of state---->

            		<!----progres---->
            		<div class="row">
            			<div class="col md-12 col sm-12 progress">
            				<p class="p-progress">امتیاز کل  شما</p>
            				<div class="progress-div">
            					<img src="webapp-assets\images\comment.png" alt="">
            					<span class="p-progress-div">10</span>
            				</div>
            				<div class="blue-bar">
								<progress id="prg-value" value="90" max="100"> 90% </progress>
								<p class="right-number">50</p>
								<p class="left-number">100</p>
							</div>
            			</div>
            		</div><!---end of progress---->  

				<br>
				<br><br>
            	</div>
            </div>

            <div class="footer-wrapper"><!---footer---->
                <div class="footer-div"><!---footer---->

              
	                	<!-- <div class="home"><img src="webapp-assets\images\home.png" alt=""><div style="color:#82817d;text-align: center;margin-top: 8px;">خانه</div></div>
	                	<div class="products"><img src="webapp-assets\images\basket.png" alt=""><div style="color:#82817d;text-align: center;margin-top: 8px;"> کالاها</div></div>
	                	<div class="sales"><img src="webapp-assets\images\sales.png" alt=""><div style="color:#82817d;margin: auto;">فروش ها</div></div>
	                	<div class="services"><img src="webapp-assets\images\checklist.png" alt=""><div style="color:#82817d;margin: auto;">خدمت ها</div></div>
	                	<div class="comments"><img src="webapp-assets\images\mail.png" alt=""><div style="color:#82817d;margin: auto;margin-right: 5px;">پیشنهادها</div></div>
	                	 -->
                	<div><img src="webapp-assets\images\home.png" alt=""><div>خانه</div></div>
                	<div><img src="webapp-assets\images\ic_product.png" alt=""><div>کالاها</div></div>
                	<div><img src="webapp-assets\images\sales.png" alt=""><div>فروش ها</div></div>
                	<div><img src="webapp-assets\images\mail.png" alt=""><div>پیشنهادها</div></div>
                	<div><img src="webapp-assets\images\checklist.png" alt=""><div>خدمت ها</div></div>
		            			
			    </div><!---footer----> 
		    </div><!---footer----> 

        </div>
    </div>


@endsection