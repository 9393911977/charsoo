@extends('layouts.template')
	@section('content')
	<div id="tab-hom" class="tab tab-active tab-home" style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}') ; background-size:cover;">
		<div class="img-top">
			<div class="row top">
				<div class="col-md-4 col-sm-4 col-xs-4 right-img">
					<div class="serach-icon-awesome">
						<a href="{{route('store')}}"><img  class="arrow-img"src="{{asset('webapp-assets\images\ic_arrow_back.png')}}" alt=""></a>
					</div>	
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 middle-txt">
					ایجاد فروشگاه
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 on-left-img pl-0">
				</div>       		
			</div>
			<img src="{{asset('webapp-assets/images/top-bg.png')}}" alt="">
		</div>
		<br>
		<div class="desc-wrapper">
			<div class="desc-inner pt-2 pb-2" style="position:relative;">
				<form action="{{ route("submitStore") }}" enctype="multipart/form-data" method="post">
					@csrf
					<div class="form-row">
						<div class="form-group col-sm-6 col-xs-12">
							<label for="name">نام فروشگاه</label>
							<input required type="text" name="name" id="name" class="form-control bfinput">
						</div>
						<div class="form-group col-sm-6 col-xs-12">
							<label for="store_number">تلفن فروشگاه</label>
							<input required type="text" name="number" id="store_number" class="form-control bfinput">
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-sm-6 col-xs-12">
							<label for="description">معرفی فروشگاه</label>
							<textarea required name="description" id="description" class="form-control"></textarea>
						</div>
						<div class="form-group col-sm-6 col-xs-12">
							<label for="address">آدرس فروشگاه</label>
							<textarea required name="address" id="address" class="form-control"></textarea>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-12">
							<label for="picture">تصویر فروشگاه</label>
							<input required type="file" name="picture" id="picture" class="form-control">
						</div>
					</div>
					<div class="text-center">
						<button class="btn btn-success">ثبت فروشگاه</button>
					</div>
				</form>
			</div>
		</div>
		<br><br>
	</div>  
@endsection