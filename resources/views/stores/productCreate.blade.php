@extends('layouts.template')
	@section('content')
	<div id="tab-hom" class="tab tab-active tab-home" style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}') ; background-size:cover;">
		<div class="img-top">
			<div class="row top">
				<div class="col-md-4 col-sm-4 col-xs-4 right-img">
					<div class="serach-icon-awesome">
						<a href="{{route('store')}}"><img  class="arrow-img"src="{{asset('webapp-assets\images\ic_arrow_back.png')}}" alt=""></a>
					</div>	
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 middle-txt">
					ایجاد محصول
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 on-left-img pl-0">
				</div>       		
			</div>
			<img src="{{asset('webapp-assets/images/top-bg.png')}}" alt="">
		</div>
		<br>
		<div class="desc-wrapper">
			<div class="desc-inner pt-2 pb-2" style="position:relative;max-width: 400px !important;">
				<form class="submitproduct" action="{{ route('submitProduct') }}" enctype="multipart/form-data" method="post">
					@csrf
					<input type="hidden" name="store_id" value="{{$results['id']}}">
					<div class="form-row">
						<div class="form-group col-sm-6 col-xs-12">
							<label for="product_type">نوع محصول *</label>
							<select class="form-control" id="product_type" name="product_type">
								<option value="primary_product">محصول اصلی</option>
								<option value="secondary_product">محصول اضافی</option>
							</select>
						</div>
						<div class="form-group col-sm-6 col-xs-12">
							<label for="product_name">نام محصول *</label>
							<input required type="text" name="product_name" id="product_name" class="form-control bfinput" style="font-size: 16px;">
						</div>
					</div>
					<div class="form-row">
						<div class="col-4">
							<label for="fienterprofileimage">تصویر اصلی * 
								<div class="center-circle" style="width: 80px; height: 80px;">
									<img id="duc_img" src="
									{{asset('webapp-assets\images\empty-picture.jpg')}}" alt="" >
									<input required id="fienterprofileimage" accept="image/*" onchange="fishowDocumentsImage(this)" type="file" name="product_image">
								</div>
							</label>
						</div>
						<span id="images-wrapper" style="display: contents">
							<div class="col-4">
								<label for="seenterprofileimage">تصویر دوم
									<div class="center-circle" style="width: 80px; height: 80px;">
										<img id="duc_img1" src="
										{{asset('webapp-assets\images\empty-picture.jpg')}}" alt="" >
										<input id="seenterprofileimage" accept="image/*" onchange="seshowDocumentsImage(this)" type="file" name="product_image1">
									</div>
								</label>
							</div>
							<div class="col-4">
								<label for="thenterprofileimage">تصویر سوم
									<div class="center-circle" style="width: 80px; height: 80px;">
										<img id="duc_img2" src="
										{{asset('webapp-assets\images\empty-picture.jpg')}}" alt="" >
										<input id="thenterprofileimage" accept="image/*" onchange="thshowDocumentsImage(this)" type="file" name="product_image2">
									</div>
								</label>
							</div>
						</span>
					</div>
					<div class="form-row">
						<div class="form-group col-sm-6 col-xs-12">
							<label for="withoutoff_price">قیمت بدون تخفیف *</label>
							<input required type="number" min="1" name="withoutoff_price" id="withoutoff_price" class="form-control bfinput">
						</div>
						<div class="form-group col-sm-6 col-xs-12" id="second-price">
							<label for="withoff_price">قیمت تخفیف دار</label>
							<input type="number" name="withoff_price" min="1" id="withoff_price" class="form-control bfinput">
						</div>
					</div>
					<div class="form-row" id="pdesc">
						<div class="form-group col-sm-12 col-xs-12">
							<label for="description">توضیحات</label>
							<textarea name="description" id="description" class="form-control"></textarea>
						</div>
					</div>

					
					@if($results['categorize'])
						@if(!empty($results['categories']))
						  <hr>
							<h6 class="p-2">مدیریت دسته بندی محصول</h6>
							<small>دسته بندی های مورد نظر راانتخاب کنید</small>
							<div class="desc-wrapper first store">
								<div style="padding: 0px;" class="desc-inner category">
									<div class="storeCatswrapper">
										@foreach ($results['categories'] as $key => $category)
											@if ($category['main_category'])
											<input class="catinput main" type="checkbox" value="{{$category['id']}}" name="product_category[]" id="category{{$category['id']}}">
											<label class="catlbl" for="category{{$category['id']}}"><div class="categoryitem">{{$category['title']}}</div></label>
											@endif
										@endforeach
									</div>
								</div>
							</div>
							<hr>
						@endif
					@endif
					@if($results['inventory'])
					<div class="row mr-2">
            <div class="col-10">نوع انبارداری این محصول :
              <select style="font-size:10px;" class="form-control" name="inventory_type">
              <option value="رنگ">رنگ</option>
              <option value="سایز">سایز</option>
              <option value="وزن">وزن</option>
              </select>
            </div>
          </div>

        	<hr>
          <div class="row"><span class="badge badge-danger">ثبت انبارداری</span></div>
          <div class="form-row warehouse-detail">
          	<input type="hidden" name="new_inventory_id[]" value="">
            <div class="form-group mb-0 col-4">
              <label for="new_title" class="col-form-label">عنوان</label>
              <input style="font-size:10px;" id="new_title" class="form-control text-right bfinput" name="new_title[]" value="" type="text">
            </div>
            <div class="form-group mb-0 col-4">
              <label for="new_count" class="col-form-label">تعداد</label>
              <input style="font-size:10px;" id="new_count" class="form-control text-right" name="new_count[]" value="" type="number">
            </div>
            <div class="form-group mb-0 col-4">
              <label for="new_warehouse" class="col-form-label">انبار</label>
              <select style="font-size:10px;" id="new_warehouse" class="form-control" name="new_warehouse[]">
								@foreach($results['warehouses'] as $ware=>$warehouse){	
									<option value="{{$warehouse['id']}}">{{$warehouse['name']}}</option>
								@endforeach
              </select>
            </div>
          </div>
          <div class="clone"></div>
					<a href="#" class="clone-inv badge badge-primary">افزودن آیتم جدید</a>
					@endif
					<div class="text-center">
						<button type="submit" class="btn btn-success">ذخیره سازی محصول جدید</button>
					</div>
				</form>
			</div>
		</div>
		<br><br>
	</div>  
@endsection
@section('js')
<script>
$(document).ready(function(){
	$("#product_type").change(function(){
		if($(this).val() == "primary_product"){
			$("#images-wrapper").show();
			$("#second-price").show();
			$("#pdesc").show();
		}else{
			$("#images-wrapper").hide();
			$("#second-price").hide();
			$("#pdesc").hide();
		}
	});

	$('.submitproduct').submit(function(){
				withoutoff = $("#withoutoff_price").val();
				withoff = $("#withoff_price").val();
				if(withoff !== null || withoff !== ''){
					intwithoutoff = parseInt(withoutoff);
					intwithoff = parseInt(withoff);
					if(intwithoutoff < intwithoff){
						function reset() {
							alertify.set({
							labels : {
							ok     : "OK",
							cancel : "Cancel"
							},
							delay : 6000,
							buttonReverse : false,
							buttonFocus   : "ok"
							});
						}
						reset();
						alertify.error('قیمت تخفیف دارباید کمترازقیمت بدون تخفیف باشد');
						return false;
					}
				}
			});
});
function fishowDocumentsImage(fileInput) {
	var files = fileInput.files;
	for (var i = 0; i < files.length; i++) {
		var file = files[i];
		var imageType = /image.*/;
		if (!file.type.match(imageType)) {
		continue;
		}
		var img=document.getElementById("duc_img");
		img.file = file;
		var reader = new FileReader();
		reader.onload = (function(aImg) {
			return function(e) {
			aImg.src = e.target.result;
			};
		})(img);
		reader.readAsDataURL(file);
	}
}
function seshowDocumentsImage(fileInput) {
	var files = fileInput.files;
	for (var i = 0; i < files.length; i++) {
		var file = files[i];
		var imageType = /image.*/;
		if (!file.type.match(imageType)) {
		continue;
		}
		var img=document.getElementById("duc_img1");
		img.file = file;
		var reader = new FileReader();
		reader.onload = (function(aImg) {
			return function(e) {
			aImg.src = e.target.result;
			};
		})(img);
		reader.readAsDataURL(file);
	}
}
function thshowDocumentsImage(fileInput) {
	var files = fileInput.files;
	for (var i = 0; i < files.length; i++) {
		var file = files[i];
		var imageType = /image.*/;
		if (!file.type.match(imageType)) {
		continue;
		}
		var img=document.getElementById("duc_img2");
		img.file = file;
		var reader = new FileReader();
		reader.onload = (function(aImg) {
			return function(e) {
			aImg.src = e.target.result;
			};
		})(img);
		reader.readAsDataURL(file);
	}
}
</script>
@endsection