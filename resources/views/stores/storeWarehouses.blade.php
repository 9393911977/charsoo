@extends('layouts.template')
	@section('content')
	<div id="tab-hom" class="tab tab-active tab-home" style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}') ; background-size:cover;">
		<div class="img-top">
			<div class="row top">
				<div class="col-md-4 col-sm-4 col-xs-4 right-img">
					<div class="serach-icon-awesome">
						<a href="{{route('store')}}"><img  class="arrow-img"src="{{asset('webapp-assets\images\ic_arrow_back.png')}}" alt=""></a>
					</div>	
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 middle-txt">
					انبارداری
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 on-left-img pl-0">
				</div>       		
			</div>
			<img src="{{asset('webapp-assets/images/top-bg.png')}}" alt="">
		</div>
		<br>
		<form method="post" class="warehouse-form" action="{{route('submitStoreWarehouses')}}">
			@csrf
			<input type="hidden" name="store_id" value="{{ $results['id'] }}">
			<div class="row catcon">
				<div class="col-12 mnone" style="padding:0 !important;">
					<div class="card" style="max-width: 600px;margin:auto">
							<div class="card-header">
									<h5 class="text-center"><i class="fa fa-list"></i> لیست انبار ها</h5>
							</div>
							<div class="card-body catcardb">
								<table class="table table-sm">
									<thead>
										<tr>
											<td>نام</td>
											<td>آدرس</td>
											<td>شماره تلفن</td>
											<td>حذف؟</td>
										</tr>
									</thead>
									<tbody>
										@foreach ($results['warehouses'] as $item)
											<tr>
												<td>
												<div class="form-group">
												<input type="text" required name="oldwarename[]" class="form-control form-control-sm bfinput" style="height: 2em !important;" value="{{ $item['name'] }}" id="title">
													<input type="hidden" name="oldwareid[]" class="form-control" value="{{ $item['id'] }}" id="title">
												</div>
												</td>
												<td>
												<div class="form-group">
												<input type="text" required name="oldwareaddress[]" class="form-control form-control-sm bfinput" style="height: 2em !important;" value="{{ $item['address'] }}" id="title">
												</div>
												</td>
												<td>
												<div class="form-group">
												<input type="text" required name="oldwarephone[]" class="form-control form-control-sm bfinput" style="height: 2em !important;" value="{{ $item['phone'] }}" id="title">
												</div>
												</td>
												<td>
												<div class="form-group">
													<input type="checkbox" name="delete_ware[]" value="{{ $item['id'] }}" class="">
												</div>
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
					</div>
				</div>
				<div class="col-12" style="padding:0 !important;">
					<br>
					<div class="card" style="max-width: 600px;margin:auto">
							<div class="card-header">
									<h5 class="text-center"><i class="fa fa-plus"></i> افزودن انبار</h5>
							</div>
							<div class="card-body">
								<div class="form-row product-cate">
									<div class="form-group col-4">
										<label for="newwarename">نام انبار</label>
										<input type="text" name="newwarename[]" class="form-control form-control-sm bfinput" style="height: 2em !important;" id="newwarename">
									</div>
									<div class="form-group col-4">
										<label for="newwareaddress">آدرس</label>
										<input type="text" name="newwareaddress[]" class="form-control form-control-sm bfinput" style="height: 2em !important;" id="newwareaddress">
									</div>
									<div class="form-group col-4">
										<label for="newwarephone">شماره تلفن</label>
										<input type="text" name="newwarephone[]" class="form-control form-control-sm bfinput" style="height: 2em !important;" id="newwarephone">
									</div>
								</div>
								<div class="clonecat"></div>
								<button type="button" class="badge badge-primary clone-bottom">جدید</button>
							</div>
					</div>
				</div>
			</div>
			<div class="text-center mt-2 col-12">
			<button class="btn btn-success btn-sm" type="submit">ذخیره</button>
			</div>
		<form>
		<br><br>
	</div>  
@endsection