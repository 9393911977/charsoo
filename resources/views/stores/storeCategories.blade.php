@extends('layouts.template')
	@section('content')
	<div id="tab-hom" class="tab tab-active tab-home" style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}') ; background-size:cover;">
		<div class="img-top">
			<div class="row top">
				<div class="col-md-4 col-sm-4 col-xs-4 right-img">
					<div class="serach-icon-awesome">
						<a href="{{route('store')}}"><img  class="arrow-img"src="{{asset('webapp-assets\images\ic_arrow_back.png')}}" alt=""></a>
					</div>	
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 middle-txt">
					دسته بندی
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 on-left-img pl-0">
				</div>       		
			</div>
			<img src="{{asset('webapp-assets/images/top-bg.png')}}" alt="">
		</div>
		<br>
			<form method="post" action="{{ route('submitStoreCategories') }}">
				@csrf
				<input type="hidden" name="store_id" value="{{ $results['id'] }}">
				<div class="row catcon">
					<div class="col-12">
						<div class="card" style="max-width: 500px;margin:auto">
								<div class="card-header">
										<h5 class="text-center"><i class="fa fa-list"></i> دسته بندی ها</h5>
								</div>
								<div class="card-body catcardb">
									<table class="table table-sm">
										<thead>
											<tr>
												<td>عنوان</td>
												<td>زیرمجموعه</td>
												<td>حذف؟</td>
											</tr>
										</thead>
										<tbody>
											@foreach ($results['categories'] as $item)
											<tr>
												<td>
												<div class="form-group">
													<input type="text" required name="oldcattitle[]" class="form-control form-control-sm bfinput" style="height: 2em !important;" value="{{ $item['title'] }}" id="title">
													<input type="hidden" name="oldcatid[]" class="form-control" value="{{ $item['id'] }}" id="title">
												</div>
												</td>
												<td>
												<div class="form-group">
													<select class="form-control form-control-sm" name="oldsub_cat[]" id="sub_cat">
														<option @if($item['main_category']) selected @endif value="main">__ دسته اصلی __</option>';
														@foreach ($results['categories'] as $cat)
															@if($cat['main_category'])
																<option @if($cat['id'] == $item['sub_category']) selected @endif value="{{ $cat['id'] }}">{{ $cat['title'] }}</option>
															@endif
														@endforeach
													</select>
												</div>
												</td>
												<td>
												<div class="form-group">
													<input type="checkbox" name="delete_cat[]" value="{{ $item['id'] }}" class="">
												</div>
												</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
						</div>
					</div>
					<div class="col-12">
						<br>
						<div class="card" style="max-width: 500px;margin:auto">
								<div class="card-header">
										<h5 class="text-center"><i class="fa fa-plus"></i> افزودن دسته بندی</h5>
								</div>
								<div class="card-body">
									<div class="form-row product-cate">
										<div class="form-group col-6">
											<label for="title">عنوان دسته بندی</label>
											<input type="text" name="newcattitle[]" class="form-control bfinput" style="height: 2em !important;" id="title">
										</div>
										<div class="form-group col-6">
											<label for="sub_cat">زیر مجموعه</label>
											<select class="form-control form-control-sm" name="newsub_cat[]" id="sub_cat">
												<option value="main">__ دسته اصلی __</option>
												@foreach ($results['categories'] as $cat)
													@if($cat['main_category'])
														<option value="{{ $cat['id'] }}">{{ $cat['title'] }}</option>
													@endif
												@endforeach
											</select>
										</div>
									</div>
									<div class="clonecat"></div>
									<button type="button" class="badge badge-primary clone-bottom">جدید</button>
								</div>
						</div>
					</div>
				</div>
				<div class="text-center mt-2 col-12 " >
				<button class="btn btn-danger btn-sm" type="submit">ذخیره</button>
				</div>
			<form>
		<br><br>
	</div>  
@endsection