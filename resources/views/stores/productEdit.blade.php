@extends('layouts.template')
	@section('content')
	<div id="tab-hom" class="tab tab-active tab-home" style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}') ; background-size:cover;">
		<div class="img-top">
			<div class="row top">
				<div class="col-md-4 col-sm-4 col-xs-4 right-img">
					<div class="serach-icon-awesome">
						<a href="{{route('store')}}"><img  class="arrow-img"src="{{asset('webapp-assets\images\ic_arrow_back.png')}}" alt=""></a>
					</div>	
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 middle-txt">
					ویرایش محصول
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 on-left-img p-3">
					<a onclick="return confirm('از حذف محصول اطمینان دارید ؟');" href="{{route('deleteProduct' , $results['id'])}}"><img style="height:30px;width:30px;float:left;" src="{{asset('webapp-assets/images/ic_trash.png')}}" alt="" class="rgt"></a>
				</div>       		
			</div>
			<img src="{{asset('webapp-assets/images/top-bg.png')}}" alt="">
		</div>
		<br>
		<div class="desc-wrapper">
			<div class="desc-inner pt-2 pb-2" style="position:relative;max-width: 400px !important;">
				<form action="{{ route('submitEditProduct') }}" enctype="multipart/form-data" method="post">
					@csrf
					<input type="hidden" name="store_id" value="{{$results['store_id']}}">
					<input type="hidden" name="product_id" value="{{$results['id']}}">
					<input type="hidden" name="product_type" value="{{$results['type']}}">
					<div class="form-row">
						<div class="form-group col-sm-6 col-xs-12">
							<label for="product_type">نوع محصول *</label>
							<select disabled class="form-control" id="product_type">
								<option @if($results['type'] == "primary_product") selected @endif value="primary_product">محصول اصلی</option>
								<option @if($results['type'] == "secondary_product") selected @endif value="secondary_product">محصول اضافی</option>
							</select>
						</div>
						<div class="form-group col-sm-6 col-xs-12">
							<label for="product_name">نام محصول *</label>
							<input style="color: rgba(0, 0, 0, 0.89);font-size: 16px !important;" required type="text" value="{{ $results['product_name'] }}" name="product_name" id="product_name" class="form-control bfinput">
						</div>
					</div>
					<div class="form-row">
						<div class="col-4">
							<label for="fienterprofileimage">تصویر اصلی * 
								<div class="center-circle" style="width: 80px; height: 80px;">
									<img id="duc_img" src="
									@if($results['product_picture'])
									{{asset('https://panel.4sooapp.com/uploads/'.$results['product_picture'])}}
									@else
									{{asset('webapp-assets\images\empty-picture.jpg')}}
									@endif 
									" alt="" >
									<input id="fienterprofileimage" accept="image/*" onchange="fishowDocumentsImage(this)" type="file" name="product_image">
								</div>
							</label>
						</div>
						@if($results['type'] == "primary_product")
						<span id="images-wrapper" style="display: contents">
							<div class="col-4">
								<label for="seenterprofileimage">تصویر دوم
									<div class="center-circle" style="width: 80px; height: 80px;">
										<img id="duc_img1" src="
										@if($results['product_picture2'])
										{{asset('https://panel.4sooapp.com/uploads/'.$results['product_picture2'])}}
										@else
										{{asset('webapp-assets\images\empty-picture.jpg')}}
										@endif 
										" alt="" >
										<input id="seenterprofileimage" accept="image/*" onchange="seshowDocumentsImage(this)" type="file" name="product_image1">
									</div>
								</label>
							</div>
							<div class="col-4">
								<label for="thenterprofileimage">تصویر سوم
									<div class="center-circle" style="width: 80px; height: 80px;">
										<img id="duc_img2" src="
										@if($results['product_picture3'])
										{{asset('https://panel.4sooapp.com/uploads/'.$results['product_picture3'])}}
										@else
										{{asset('webapp-assets\images\empty-picture.jpg')}}
										@endif 
										" alt="" >
										<input id="thenterprofileimage" accept="image/*" onchange="thshowDocumentsImage(this)" type="file" name="product_image2">
									</div>
								</label>
							</div>
						</span>
						@endif
					</div>
					@if($results['type'] == "primary_product")
						<div class="form-row" id="pdesc">
							<div class="form-group col-sm-12 col-xs-12">
								<label for="description">توضیحات</label>
								<textarea autocomplete="on" name="description" id="description" class="form-control">{{ $results['product_description'] }}</textarea>
							</div>
						</div>
					@endif
					@if($results['store']['categorize'])
						@if(!empty($results['store']['categories']))
						  <hr>
							<h6 class="p-2">مدیریت دسته بندی محصول</h6>
							<small>دسته بندی های مورد نظر راانتخاب کنید</small>
							<div class="desc-wrapper first store">
								<div style="padding: 0px;" class="desc-inner category">
									<div class="storeCatswrapper">
										@foreach ($results['store']['categories'] as $key => $category)
											@if ($category['main_category'])
											<input @if ($category['in_category']) checked @endif class="catinput main" type="checkbox" value="{{$category['id']}}" name="product_category[]" id="category{{$category['id']}}">
											<label class="catlbl" for="category{{$category['id']}}"><div class="categoryitem">{{$category['title']}}</div></label>
											@endif
										@endforeach
									</div>
								</div>
							</div>
							<hr>
						@endif
					@endif
					@if($results['store']['inventory'])
					<div class="row mr-2">
            <div class="col-10">نوع انبارداری این محصول :
              <select style="font-size:10px;" class="form-control" name="inventory_type">
              <option @if($results['inventory_type'] == "رنگ") selected @endif value="رنگ">رنگ</option>
              <option @if($results['inventory_type'] == "سایز") selected @endif value="سایز">سایز</option>
              <option @if($results['inventory_type'] == "وزن") selected @endif value="وزن">وزن</option>
              </select>
            </div>
          </div>
        
          <div style="max-height:350px;overflow-y:auto;overflow-x:hidden;">
						@foreach($results['inventory'] as $inv=>$inventory)
							<div class="form-row">
								<input type="hidden" name="inventory_id[]" value="{{$inventory['id']}}" >
								<div class="form-group mb-0 col-4">
									<label for="title" class="col-form-label">عنوان</label>
									<input style="font-size:10px;" id="title" class="form-control text-right bfinput" name="title[]" value="{{$inventory['title']}}" type="text">
								</div>
								<div class="form-group mb-0 col-3">
									<label for="count" class="col-form-label">تعداد</label>
									<input style="font-size:10px;" min="0" id="count" class="form-control text-right" name="count[]" value="{{$inventory['count']}}" type="number">
								</div>
								<div class="form-group mb-0 col-4">
									<label for="warehouse" class="col-form-label">انبار</label>
									<select style="font-size:10px;" id="warehouse" class="form-control" name="warehouse[]">';
										@foreach($results['store']['warehouses'] as $ware=>$warehouse){
											<option @if($inventory['warehouse_id'] == $warehouse['id']) selected @endif value="{{$warehouse['id']}}">{{$warehouse['name']}}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group mb-0 col-1">
									<label for="" class="col-form-label"><i style="color:rgb(234, 43, 43)" class="fa fa-trash"></i></label>
									<input id="deleteinventory" class="form-control" name="deleteinventory[]" value="{{$inventory['id']}}" type="checkbox">
								</div>
							</div>
						@endforeach
					</div>
        	<hr>
          <div class="row"><span class="badge badge-danger">ثبت مورد جدید</span></div>
          <div class="form-row warehouse-detail">
          	<input type="hidden" name="new_inventory_id[]" value="">
            <div class="form-group mb-0 col-4">
              <label for="new_title" class="col-form-label">عنوان</label>
              <input style="font-size:10px;" id="new_title" class="form-control text-right bfinput" name="new_title[]" value="" type="text">
            </div>
            <div class="form-group mb-0 col-4">
              <label for="new_count" class="col-form-label">تعداد</label>
              <input style="font-size:10px;" id="new_count" class="form-control text-right" name="new_count[]" value="" type="number">
            </div>
            <div class="form-group mb-0 col-4">
              <label for="new_warehouse" class="col-form-label">انبار</label>
              <select style="font-size:10px;" id="new_warehouse" class="form-control" name="new_warehouse[]">
								@foreach($results['store']['warehouses'] as $ware=>$warehouse){
									<option value="{{$warehouse['id']}}">{{$warehouse['name']}}</option>
								@endforeach
              </select>
            </div>
          </div>
          <div class="clone"></div>
					<a href="#" class="clone-inv badge badge-primary">افزودن آیتم جدید</a>
					@endif
					<div class="text-center">
						<button type="submit" class="btn btn-success">ویرایش محصول</button>
					</div>
				</form>
			</div>
		</div>
		<br><br>
	</div>
@endsection
@section('js')
<script>
$(document).ready(function(){
	$("#product_type").change(function(){
		if($(this).val() == "primary_product"){
			$("#images-wrapper").show();
			$("#second-price").show();
			$("#pdesc").show();
		}else{
			$("#images-wrapper").hide();
			$("#second-price").hide();
			$("#pdesc").hide();
		}
	});
});
function fishowDocumentsImage(fileInput) {
	var files = fileInput.files;
	for (var i = 0; i < files.length; i++) {
		var file = files[i];
		var imageType = /image.*/;
		if (!file.type.match(imageType)) {
		continue;
		}
		var img=document.getElementById("duc_img");
		img.file = file;
		var reader = new FileReader();
		reader.onload = (function(aImg) {
			return function(e) {
			aImg.src = e.target.result;
			};
		})(img);
		reader.readAsDataURL(file);
	}
}
function seshowDocumentsImage(fileInput) {
	var files = fileInput.files;
	for (var i = 0; i < files.length; i++) {
		var file = files[i];
		var imageType = /image.*/;
		if (!file.type.match(imageType)) {
		continue;
		}
		var img=document.getElementById("duc_img1");
		img.file = file;
		var reader = new FileReader();
		reader.onload = (function(aImg) {
			return function(e) {
			aImg.src = e.target.result;
			};
		})(img);
		reader.readAsDataURL(file);
	}
}
function thshowDocumentsImage(fileInput) {
	var files = fileInput.files;
	for (var i = 0; i < files.length; i++) {
		var file = files[i];
		var imageType = /image.*/;
		if (!file.type.match(imageType)) {
		continue;
		}
		var img=document.getElementById("duc_img2");
		img.file = file;
		var reader = new FileReader();
		reader.onload = (function(aImg) {
			return function(e) {
			aImg.src = e.target.result;
			};
		})(img);
		reader.readAsDataURL(file);
	}
}
</script>
@endsection