@extends('layouts.template')
	@section('content')
	<div class="image-magnifier-el">
			<div class="close-image-magnifier">
					<i id="colse-magnifier" class="fa fa-times"></i>
			</div>
			<div class="image-magnifier-wrapper">
					<img id="img-magnifier" src="" alt="">
			</div>
	</div>
	<div id="tab-hom" class="tab tab-active tab-home" style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}') ; background-size:cover;">
		<div class="img-top">
			<div class="row top">
				<div class="col-md-4 col-sm-4 col-xs-4 right-img">
					<div class="serach-icon-awesome">
					
					</div>	
				</div>
				@if ($results !== 'error' && $results !== 'confirming')
				<div class="col-md-4 col-sm-4 col-xs-4 middle-txt">
					<span>{{ $results['store_name'] }}</span>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 on-left-img pl-0">
					@if($results['store_status'] == 1)
					<a href="{{ route('changeStoreStatus' , 0) }}">
						<div class="on-toggle">
							<p class="on-checker">باز</p>
							<div class="on-sub-toggle"></div>
						</div>
					</a>
					@else
					<a href="{{ route('changeStoreStatus' , 1) }}">
						<div class="on-toggle">
							<p style="color: red;" class="on-checker">بسته</p>
							<div style="background: red;" class="on-sub-toggle"></div>
						</div>
					</a>
					@endif
				</div>       		
				@endif
			</div>
			<img src="{{asset('webapp-assets/images/top-bg.png')}}" alt="">
		</div>

		@if ($results !== 'error' && $results !== "confirming")

		<div class="my-modal">
			<div class="modal-wrapper">
					<div class="modal-inner">
						<form class="submitprice" action="{{ route("changeProductPrice") }}" enctype="multipart/form-data" method="post">
							<div class="modal-head">
									<span id="closeModal" class="dismis"><b><i class="fa fa-times"></i></b></span>
									قیمت جدید
							</div>
							<div class="modal-body">
									@csrf
									<input type="hidden" name="product_id" id="inputid" value="">
									<div class="form-row">
										<div class="form-group col-12">
											<input required type="number" name="withoutoff" id="withoutoff" placeholder="قیمت بدون تخفیف" class="form-control bfinput">
										</div>
										<div class="form-group col-12">
											<input type="number" name="withoff" id="withoff" placeholder="قیمت تخفیف دار" class="form-control bfinput">
										</div>
									</div>
							</div>
							<div class="modal-foot"><button type="submit" style="position: absolute; left: 12px;top:4px;" class="mybtn" >تایید</button></div>
						</form>
					</div>
			</div>
		</div>
		
		<div class="desc-wrapper">
			<div class="desc-inner pt-2 pb-2" style="position:relative;">
				<div class="products-div-container">
					<img src="{{asset('https://panel.4sooapp.com/uploads/'.$results['store_picture'])}}" alt="">
				</div>
			</div>
		</div>
		@if($results['count_change'] > 0)
		<div class="desc-wrapper">
			<div class="desc-inner pt-2 pb-2" style="position:relative;min-height:auto;background: #ffa025;color:#fff !important;font-size:12px;">
				تعداد {{$results['count_change']}} درخواست ویرایش شما در حال بررسی است .
			</div>
		</div>
		@endif
		<div class="desc-wrapper">
			<div class="desc-inner pt-2 pb-2" style="position:relative;">
				<div class="desc-wrapper">
					<div class="row">
						<div class="col-6 text-center">
							@if($results['categorize'])
							<a class="btn btn-sm btn-secondary" href="{{ route("storeCategories") }}">دسته بندی</a>
							@endif
						</div>
						<div class="col-6 text-center">
							@if($results['inventory'])
							<a class="btn btn-sm btn-success" href="{{ route("storeWarehouses") }}">انبارداری</a>
							@endif
						</div>
					</div>
				</div>
				<div class="products-div-container">
					<p>{{ $results['store_description'] }}</p>
					<hr style="margin:8px 0;">
					<div class="products-div-container">
						<p style="display: inline-block;float: right;">آدرس:</p>
						<p style="display: inline-block; float: left;">{{ $results['store_address'] }}</p>
					</div><br style="margin:5px;">
					<div class="products-div-container">
						<p style="display: inline-block;float: right;">شماره تماس:</p>
						<p style="display: inline-block; float: left;"><a href="tel:{{ $results['mobile'] }}">{{ $results['mobile'] }}</a></p>
					</div>
					<br>
				</div>
			</div>
		</div>
		<div style="width: 200px;" class="m-auto text-center"><a href="{{ route('createProduct') }}" class="btn btn-success"><i class="fa fa-plus"></i> افزودن محصول جدید</a></div>
		
		<div class="desc-wrapper">
			@foreach ($results['unconfirmed_products'] as $item)
			<div class="desc-inner my-2 pt-2 pb-2 pr-0 pl-0" style="position:relative;">
				<div class="row">
					<div class="col-10"><b><p style="font-size: 22px;color:#000;">{{ $item['product_name'] }}</p></b><button class="btn btn-sm" style="background: aquamarine;border-radius: 12px;">در حال بررسی</button></div>
				</div>
				<div class="row">
					<div class="col-sm-6 col-xs-12">
						<div class="row">
							<div class="col-5 products-content p-0 m-auto">
								<div class="col-products-content2">
									<img class="thumb-picture" width="30px" src="{{asset('https://panel.4sooapp.com/uploads/'.$item['product_picture'])}}" alt="">
								</div>
								@if (!empty($item['product_picture2']))
								<div class="col-products-content2">
									<img class="thumb-picture" width="30px" src="{{asset('https://panel.4sooapp.com/uploads/'.$item['product_picture2'])}}" alt="">
								</div>
								@endif
								@if (!empty($item['product_picture3']))
								<div class="col-products-content2">
									<img class="thumb-picture" width="50px" src="{{asset('https://panel.4sooapp.com/uploads/'.$item['product_picture3'])}}" alt="">
								</div>
								@endif
							</div>
							<div class="col-7 products-content p-0 m-0">
								<div class="col-products-content" style="border-radius: 0px; box-shadow:none;border:2px solid #ddd0;cursor:pointer;">
									<img class="main-picture magnify" src="{{asset('https://panel.4sooapp.com/uploads/'.$item['product_picture'])}}" alt="">
								</div>                    			         		                  		    
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<p style="color:#aba3a2;">{{ $item['product_description'] }}</p>
					</div>
				</div>
				<div class="row p-2">
					<div class="col-3">{{ number_format($item['product_price']) }}  تومان</div>
					<div class="col-3">@if (!empty($item['product_beforeoffer_price'])) <strike>{{ number_format($item['product_beforeoffer_price']) }}  تومان</strike> @endif</div>
					<div class="col-3 pl-0 products-content left-content">
						<div data-id="{{ $item['id'] }}" class="openModal left-products-icon">
							<i class="fas fa-coins"></i>
						</div>
					</div>
				</div>
				@if(!empty($item['inventory']))
				<div class="storeCatswrapper px-4">
					{{$item['inventory_type']}} :<br>
					@foreach ($item['inventory'] as $key => $inv)
					<input data-inv="{{ $inv['count'] }}" class="invinput catinput" @if($key == 0) checked @endif
					class="tablink" 
						type="radio" value="{{ $inv['id'] }}" name="inventory{{ $item['id'] }}" id="item{{ $inv['id'] }}">
					<label class="catlbl" for="item{{ $inv['id'] }}"><div class="categoryitem">{{ $inv['title'] }}</div></label>
					@endforeach
					<div class="inv-printer">موجودی : <b class="inv">{{$item['inventory'][0]['count'] }}</b></div>
				</div>
				<br>
				@endif
				<div class="products-div-container">
					@if($item['product_status'])
					<div class="right-btn"><a href="{{route('changeProductStatus' , [$item['id'] , 0])}}"><p>موجود</p></a></div>
					@else
					<div style="background: #db4040;" class="right-btn"><a href="{{route('changeProductStatus' , [$item['id'] , 1])}}"><p>ناموجود</p></a></div>
					@endif
					<br><br>
				</div>
					
			</div>
			@endforeach
		</div>

		<div class="desc-wrapper">
			@foreach ($results['products'] as $item)
			<div class="desc-inner my-2 pt-2 pb-2 pr-0 pl-0" style="position:relative;">
				<div class="row">
					<div class="col-10"><b><p style="font-size: 22px;color:#000;">{{ $item['product_name'] }}</p></b></div>
					<div class="col-sm-2 col-md-2 col-xs-2 products-content left-content p-0 m-0" >
						<a href="{{ route('editProduct' , $item['id']) }}">
							<div class="left-products-icon">
								<i class="fas fa-pencil-alt"></i>
							</div>
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 col-xs-12">
						<div class="row">
							<div class="col-5 products-content p-0 m-auto">
								<div class="col-products-content2">
									<img class="thumb-picture" width="30px" src="{{asset('https://panel.4sooapp.com/uploads/'.$item['product_picture'])}}" alt="">
								</div>
								@if (!empty($item['product_picture2']))
								<div class="col-products-content2">
									<img class="thumb-picture" width="30px" src="{{asset('https://panel.4sooapp.com/uploads/'.$item['product_picture2'])}}" alt="">
								</div>
								@endif
								@if (!empty($item['product_picture3']))
								<div class="col-products-content2">
									<img class="thumb-picture" width="50px" src="{{asset('https://panel.4sooapp.com/uploads/'.$item['product_picture3'])}}" alt="">
								</div>
								@endif
							</div>
							<div class="col-7 products-content p-0 m-0">
								<div class="col-products-content" style="border-radius: 0px; box-shadow:none;border:2px solid #ddd0;cursor:pointer;">
									<img class="main-picture magnify" src="{{asset('https://panel.4sooapp.com/uploads/'.$item['product_picture'])}}" alt="">
								</div>                    			         		                  		    
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<p style="color:#aba3a2;">{{ $item['product_description'] }}</p>
					</div>
				</div>
				<div class="row p-2">
				  <div class="col-3">{{ number_format($item['product_price']) }}  تومان</div>
				  <div class="col-3">@if (!empty($item['product_beforeoffer_price'])) <strike>{{ number_format($item['product_beforeoffer_price']) }}  تومان</strike> @endif</div>
				  <div class="col-3 pl-0 products-content left-content">
						<div data-id="{{ $item['id'] }}" class="openModal left-products-icon">
							<i class="fas fa-coins"></i>
						</div>
					</div>
				</div>
				@if(!empty($item['inventory']))
				<div class="storeCatswrapper px-4">
					{{$item['inventory_type']}} :<br>
					@foreach ($item['inventory'] as $key => $inv)
					<input data-inv="{{ $inv['count'] }}" class="invinput catinput" @if($key == 0) checked @endif
					class="tablink" 
						type="radio" value="{{ $inv['id'] }}" name="inventory{{ $item['id'] }}" id="item{{ $inv['id'] }}">
					<label class="catlbl" for="item{{ $inv['id'] }}"><div class="categoryitem">{{ $inv['title'] }}</div></label>
					@endforeach
					<div class="inv-printer">موجودی : <b class="inv">{{$item['inventory'][0]['count'] }}</b></div>
				</div>
				<br>
				@endif
				<div class="products-div-container">
					@if($item['product_status'])
					<div class="right-btn"><a href="{{route('changeProductStatus' , [$item['id'] , 0])}}"><p>موجود</p></a></div>
					@else
					<div style="background: #db4040;" class="right-btn"><a href="{{route('changeProductStatus' , [$item['id'] , 1])}}"><p>ناموجود</p></a></div>
					@endif
					<br><br>
				</div>
					
			</div>
			@endforeach
		</div>
		@elseif($results == "confirming")
		<br><br><br>
		<br><br><br>
		<div class="text-center">فروشگاه شما در انتظار تایید است .</div>
		@else
		<br><br>
		<div style="width: 200px;" class="m-auto text-center"><a href="{{ route('createStore') }}" class="btn btn-success">افزودن فروشگاه جدید</a></div>
		<br><br><br><br><br>
		<div class="text-center">هنوز فروشگاهی برای شما ثبت نشده است.</div>
		@endif
		<br><br>
		<br><br>
		@include('footer.footer')
	</div>  
@endsection
@section('js')
	<script>
		$(document).ready(function(){
			
			$('.magnify').click(function(){
					imgsrc = $(this).attr('src');
					$('#img-magnifier').attr('src',imgsrc);
					$('.image-magnifier-el').fadeIn();
			});
			$('#colse-magnifier').click(function(){
					$('.image-magnifier-el').fadeOut();
			});

			$('.submitprice').submit(function(){
				withoutoff = $("#withoutoff").val();
				withoff = $("#withoff").val();
				if(withoff !== null || withoff !== ''){
					intwithoutoff = parseInt(withoutoff);
					intwithoff = parseInt(withoff);
					if(intwithoutoff < intwithoff){
						function reset() {
							alertify.set({
							labels : {
							ok     : "OK",
							cancel : "Cancel"
							},
							delay : 6000,
							buttonReverse : false,
							buttonFocus   : "ok"
							});
						}
						reset();
						alertify.error('قیمت تخفیف دارباید کمترازقیمت بدون تخفیف باشد');
						return false;
					}
				}
			});

		});
	</script>
@endsection