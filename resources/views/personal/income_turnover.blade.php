    @extends('layouts.template')
    @section('content')
		<div class="se-pre-con" style="display:block">
	 </div>
    <div id="tab-hom" class="tab tab-active tab-home" style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}') ; background-size:cover;">
        
        <div class="bigger-img-top" style="background-image: url('{{asset('webapp-assets/images/top-bg.png')}}')">
        <div class="img-top" style="margin-bottom: 10px;">
            <div class="row top ">
                <div class="col-md-3 col-sm-3 col-xs-3 right-img">
                    <a href="{{route('dashboard')}}"><img  class="arrow-img"src="{{asset('webapp-assets\images\ic_arrow_back.png')}}" alt=""></a>   
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 top-title">
                    <span>گردش مالی</span>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-3 p-0 left-img">             
                </div>              
            </div>
        </div>

            @if($arrayLenght < 3)
          <div class="row btm-div ">
            <div class="col-md-6 col-sm-6 col-xs-6 btm-col " id="charge-linear-div">

              <p>شارژ</p>
              <div class="right-linear-div" ></div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6 btm-col" id="income-linear-div">
              <p>درآمد</p>
              <div class="middle-linear-div" ></div>
            </div>
          </div>
            @else
          <div class="row btm-div">
            <div class="col-md-4 col-sm-4 col-xs-4 btm-col" id="charge-linear-div">
              <p>شارژ</p>
              <div class="right-linear-div" ></div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 btm-col" id="income-linear-div">
              <p>درآمد</p>
              <div class="middle-linear-div" ></div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 btm-col" id="deficits-linear-div">
              <p>کسورات قانونی</p>
              <div class="left-linear-div" ></div>
            </div>
          </div>
            @endif
        </div> 
  <div class="view view-main view-init ios-edges" id="charge-show-tab" data-url="/">
    <div class="page">

      <div id="tab-search" class="tab tab-search getnumpage"  style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}'); background-size:cover;">

          <div class="transaction">
              <div class="tran-wrapper">
                  <div class="tran-inner">
                      <div class="tran-item numbers">
                          <span class="right">تعداد تراکنش ها</span>
                          <span class="left "> {{$transactions}} </span>
                      </div>
                      <div class="tran-item charge">
                          <span class="right">شارژ</span>
                          <span class="left ">{{number_format($charge)}}</span>
                      </div>
                      <div class="tran-item Deduction">
                          <span class="right">کسورات</span>
                          <span class="left ">{{number_format($Deduction)}}</span>
                      </div>
                      <div class="tran-item Remaining">
                          <span class="right">مانده</span>
                          <span class="left ">{{number_format($Remaining)}}</span>
                      </div>
                  </div><hr>
              </div>
          </div>
        @if(!empty($results))
        @foreach($results['data'] as $charge)
          <div class="transaction">
              <div class="tran-wrapper">
                      <div class="tranlist-item 
                      @if($charge['type'] =='واریز') success
                      @else($charge['type'] =='برداشت')
                       danger
                       @endif
                       " >
                          <div class="accordion-heading" data-open="open">
                              <span class="price"><b class="icon"><i class="fa fa-sort-up"></i></b>
                              &nbsp;
                                {{number_format($charge['amount'])}}
                              <i class="fa"></i> تومان</span>
                              
                              <span class="amount"> <b>{{$charge['for']}}</b> </span>
                              <span class="date">
                              @if($charge['method']=='اعتباری') <img src="{{asset('webapp-assets/images/credit_icon.png')}} " alt=""> 
                              @else($charge['method']=='نقدی')
                              <img src="{{asset('webapp-assets/images/coin_icon.png')}}" alt=""> 
                              @endif
                              {{\Morilog\Jalali\Jalalian::forge($charge['created_at'])->format('%d/ %m/ %Y')}}
                               </span>
                          </div>

                          <div data-status="0" class="accordion accmenu">
                            <div class="row pr-0 mr-0">
                                <div class="col-md-6 col-sm-6 col-xs-6 pr-0 turnovers">
                                بابت : {{$charge['for']}}
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 pr-0 turnovers">
                                شناسه تراکنش :{{$charge['user_acounts_id']}}
                                </div>
                            </div>
                            @if($charge['customer_name'] || $charge['customer_name'] !==null ) 
                            <div class="row pr-0 mr-0">
                                <div class="col-md-6 col-sm-6 col-xs-6 pr-0 turnovers">
                                نام مشتری: {{$charge['customer_name']}}
                                </div>
                            </div>
                            @endif
                            @if($charge['order_unique_code'] || $charge['order_unique_code'] !==null )
                            <div class="row pr-0 mr-0">
                                <div class="col-md-6 col-sm-6 col-xs-6 pr-0 turnovers">
                                شناسه سفارش : {{$charge['order_unique_code']}}
                                </div>
                            </div>
                            @endif
                            <div class="row pr-0 mr-0">
                                <div class="col-md-12 col-sm-12 col-xs-12 pr-0 turnovers">
                                   منتهی‌به: {{$charge['from_to']}}
                                </div>
                            </div>
                          </div>
                      </div>
              </div>             
          </div>           
        @endforeach
        @endif 
      </div>
    </div>
  </div>
 
  <div class="view view-main view-init ios-edges" id="income-show-tab" data-url="/">
    <div class="page">

      <div id="tab-search" class="tab tab-search getnumpage"  style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}'); background-size:cover;">
          <!-- <div class="navbar-wrapper" style="height:60px !important;">
              <div class="navbar navbar-home">
                  <div class="navbar-inner">
                      <div class="title" style="color:#fff !important; text-align:center;margin-top:10px;">
                             گردش مالی
                      </div>
                      <i style="margin-top:0px;" id="backLink" class="fa fa-arrow-right"></i>
                  </div>
              </div>
          </div> -->
          <div class="transaction">
              <div class="tran-wrapper">
                  <div class="tran-inner">
                      <div class="tran-item numbers">
                          <span class="right">تعداد تراکنش ها</span>
                          <span class="left "> {{$transactionsB}} </span>
                      </div>
                      <div class="tran-item charge">
                          <span class="right">شارژ</span>
                          <span class="left ">{{number_format($chargeB)}}</span>
                      </div>
                      <div class="tran-item Deduction">
                          <span class="right">کسورات</span>
                          <span class="left ">{{number_format($DeductionB)}}</span>
                      </div>
                      <div class="tran-item Remaining">
                          <span class="right">مانده</span>
                          <span class="left ">{{number_format($RemainingB)}}</span>
                      </div>
                  </div><hr>
              </div>
          </div>
          @if(!empty($resultsb))
          @foreach($resultsb['data'] as $income)
          <div class="transaction">
              <div class="tran-wrapper">
                      <div class="tranlist-item 
                      @if($income['type'] =='واریز') success
                      @else($income['type'] =='برداشت')
                       danger
                       @endif
                       " id="accbtn">
                          <div class="accordion-heading" data-open="close">
                              <span class="price"><b class="icon"><i class="fa fa-sort-up"></i></b>&nbsp;
                              {{number_format($income['amount'])}}
                              <i class="fa"></i> تومان</span>
                              <span class="amount"> <b>{{$income['for']}}</b> </span>
                              <span class="date">
                              @if($income['method']=='اعتباری')
                              <img src="{{asset('webapp-assets/images/credit_icon.png')}}" alt="">
                              @else($income['method']=='نقدی')
                                <img src="webapp-assets/images/coin_icon.png" alt=""> 
                              @endif  
                              {{\Morilog\Jalali\Jalalian::forge($charge['created_at'])->format('%d/ %m/ %Y')}}
                            </span>
                          </div>
                          <div data-status="0" class="accordion accmenu">

                          <div class="row pr-0 mr-0">
                                <div class="col-md-6 col-sm-6 col-xs-6 pr-0 turnovers">
                                بابت : {{$income['for']}}
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 pr-0 turnovers">
                                شناسه تراکنش :{{$income['user_acounts_id']}}
                                </div>
                            </div>
                            @if($income['customer_name'] || $charge['customer_name'] !==null ) 
                            <div class="row pr-0 mr-0">
                                <div class="col-md-6 col-sm-6 col-xs-6 pr-0 turnovers">
                                نام مشتری: {{$income['customer_name']}}
                                </div>
                            </div>
                            @endif
                            @if($income['order_unique_code'] || $charge['order_unique_code'] !==null )
                            <div class="row pr-0 mr-0">
                                <div class="col-md-6 col-sm-6 col-xs-6 pr-0 turnovers">
                                شناسه سفارش : {{$charge['order_unique_code']}}
                                </div>
                            </div>
                            @endif
                            <div class="row pr-0 mr-0">
                                <div class="col-md-12 col-sm-12 col-xs-12 pr-0 turnovers">
                                   منتهی‌به: {{$income['from_to']}}
                                </div>
                            </div>
                          </div>
                      </div>
              </div>
          </div>
          @endforeach
          @endif
      </div>
    </div>
  </div>

  <div class="view view-main view-init ios-edges" id="deficits-show-tab" data-url="/">
    <div class="page">

      <div id="tab-search" class="tab tab-search getnumpage"  style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}'); background-size:cover;">
          <div class="transaction">
              <div class="tran-wrapper">
                  <div class="tran-inner">
                      <div class="tran-item numbers">
                          <span class="right">تعداد تراکنش ها</span>
                          <span class="left "> {{$transactionsK}} </span>
                      </div>
                      <div class="tran-item charge">
                          <span class="right">شارژ</span>
                          <span class="left ">{{number_format($chargeK)}}</span>
                      </div>
                      <div class="tran-item Deduction">
                          <span class="right">کسورات</span>
                          <span class="left ">{{number_format($DeductionK)}}</span>
                      </div>
                      <div class="tran-item Remaining">
                          <span class="right">مانده</span>
                          <span class="left ">{{number_format($RemainingK)}}</span>
                      </div>
                  </div><hr>
              </div>
          </div>
          @if(!empty($resultsk))
          @foreach($resultsk['data'] as $deficits)
          <div class="transaction">
              <div class="tran-wrapper">
                      <div class="tranlist-item  
                      @if($deficits['type'] =='واریز') success
                      @else($deficits['type'] =='برداشت')
                       danger
                       @endif
                      " id="accbtn">
                          <div class="accordion-heading" data-open="open">
                              <span class="price"><b class="icon"><i class="fa fa-sort-up"></i></b>&nbsp;
                              {{number_format($deficits['amount'])}}
                              <i class="fa"></i> تومان</span>
                              <span class="amount"> <b>{{$deficits['for']}}</b> </span>
                              <span class="date"> 
                              @if($deficits['method'] == 'اعتباری')
                              <img src="{{asset('webapp-assets/images/credit_icon.png')}}" alt=""> 
                              @else($deficits['method'] == 'نقدی')
                              <img src="webapp-assets/images/coin_icon.png" alt="">
                              @endif
                              {{\Morilog\Jalali\Jalalian::forge($charge['created_at'])->format('%d/ %m/ %Y')}}
                            </span>
                          </div>
                          <div data-status="0" class="accordion accmenu">

                          <div class="row pr-0 mr-0">
                                <div class="col-md-6 col-sm-6 col-xs-6 pr-0 turnovers">
                                بابت : {{$deficits['for']}}
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 pr-0 turnovers">
                                شناسه تراکنش :{{$deficits['user_acounts_id']}}
                                </div>
                            </div>
                            @if($deficits['customer_name'] || $charge['customer_name'] !==null ) 
                            <div class="row pr-0 mr-0">
                                <div class="col-md-6 col-sm-6 col-xs-6 pr-0 turnovers">
                                نام مشتری: {{$deficits['customer_name']}}
                                </div>
                            </div>
                            @endif
                            @if($deficits['order_unique_code'] || $charge['order_unique_code'] !==null )
                            <div class="row pr-0 mr-0">
                                <div class="col-md-6 col-sm-6 col-xs-6 pr-0 turnovers">
                                شناسه سفارش : {{$deficits['order_unique_code']}}
                                </div>
                            </div>
                            @endif
                            <div class="row pr-0 mr-0">
                                <div class="col-md-12 col-sm-12 col-xs-12 pr-0 turnovers">
                                   منتهی‌به: {{$deficits['from_to']}}
                                </div>
                            </div>
                          </div>
                      </div>
              </div>
          </div>
          @endforeach
          @endif
      </div>
    </div>
  </div>

              
  </div>

    @endsection


    @section('js')
    <script> 
$(document).ready(function(){
  $('.se-pre-con').css('display' ,'none');
    @if($inc == 'income')
    $('#charge-show-tab').hide(10);
    $('.right-linear-div').hide(10);
    $('#income-show-tab').show(10);
    $('.middle-linear-div').show(10);
    @endif
    
    $('.accordion-heading').click(function(){
		if($(this).attr('data-open') == 'open'){
			$('.accordion').slideUp();
			$(this).find('.icon').html('<i class="fa fa-sort-down"></i>');
			$(this).next().slideDown();
			$('.accordion-heading').attr('data-open','open');
			$(this).attr('data-open','close');
		} else {
			$(this).find('.icon').html('<i class="fa fa-sort-up"></i>');
			$(this).next().slideUp();
			$('.accordion-heading').attr('data-open','open');
		}
    });
  
    $('#charge-linear-div').click(function(){
        $('#income-show-tab').hide();
        $('#deficits-show-tab').hide();
        $('.left-linear-div').hide(500);
        $('.middle-linear-div').hide(100);
        $('#charge-show-tab').fadeIn(1000);
        $('.right-linear-div').fadeIn(100);
     });
      $('#income-linear-div').click(function(){
        $('#charge-show-tab').hide();
        $('#deficits-show-tab').hide();
        $('.right-linear-div').hide(100);
        $('.left-linear-div').hide(100);
        $('#income-show-tab').fadeIn(1000);
        $('.middle-linear-div').fadeIn(100);  
    });
    $('#deficits-linear-div').click(function(){
        $('#charge-show-tab').hide();
        $('#income-show-tab').hide();
        $('.right-linear-div').hide(100);
        $('.middle-linear-div').hide(100);
        $('#deficits-show-tab').fadeIn(1000);
        $('.left-linear-div').fadeIn(100);  
    });

    
    
});
</script>
@endsection