	@extends('layouts.template')
	@section('content')
		<div id="tab-hom" class="tab tab-active tab-home" style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}') ; background-size:cover;">
        <!-- home -->
        	<div class="img-top">
	        	<div class="row top ">
	        		<div class="col-md-3 col-sm-3 col-xs-3 right-img">
                        <a href="{{route('dashboard')}}"><img  class="arrow-img"src="{{asset('webapp-assets\images\ic_arrow_back.png')}}" alt=""></a>	
	        		</div>
	        		<div class="col-md-6 col-sm-6 col-xs-6 middle-txt">
	        			<span> اطلاعات حساب من</span>
	        		</div>
	        		<div class="col-md-3 col-sm-3 col-xs-3 left-img">	        
	        		</div>       		
	        	</div>
        		<img src="{{asset('webapp-assets/images/top-bg.png')}}" alt="">
        	</div>
        	<form action="{{route('send_account_info')}}" method="post" id="accunt_form">
                @csrf
            <div class="desc-wrapper">
                <div class="desc-inner" style="position:relative;padding: 0px 15px;margin-top:12px;min-height: 68px">
                    <div class="explain-div">
                        <p>دستمزد خدمات شما با توجه به توافق قبلی و طبق برنامه ریزی
                        به حساب شما واریز می شود.به همین منظور باید  شماره شبای حساب بانکی خود را 
                        درکادر زیر وارد کنید.</p>
                    </div>
                    <div class="input-containner">
                        <label for="accunt-inputr">شماره شبا:</label>
                        <input type="text" class="accunt-input" id="shaba_number"
                         value="@if($check){{$results['shaba']}}@endif" 
                         name="shaba_number">
                    </div>

                    <div class="input-containner">
                        <label for="accunt-input"> نام صاحب حساب:</label>
                        <input type="text" class="accunt-input" id="accunt_name" 
                        value="@if($check){{$results['name']}}@endif" 
                        name="account_name">
                    </div>
                    <div class="input-containner">
                        <label for="accunt-input">بانک صادر کننده :</label>
                        <input type="text" class="accunt-input" id="bank_name" 
                        value="@if($check){{$results['bankname']}}@endif" 
                        name="bank_name">
                    </div> 
                    @if($check)
                    <div class="explain-div">
                        <p>اینجانب 
                        {{$results['personaldata']}} 
                        تایید میکنم حساب فوق  متعلق به من است و رضایت دارم دستمزد 
                        خدماتی را که انجام داده ام، به این حساب واریز گردد.</p>
                    </div> 
                    @endif 
                    <div class="div-btn">
                        <button 
                        type="submit" 
                        id="accunt_info_btn">ثبت و تایید </button>

                        </form>
                    </div> 
                </div>
           </div>
           
		              <br>   
			</div>

			@endsection
	