	@extends('layouts.template')
	@section('content')
	<div class="se-pre-con">
	</div>	
	<div id="tab-home" class="tab tab-active tab-home" style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}') ; background-size:cover;">
        <!-- home -->
			<div class="div-menu">
					<div class="menu-img">
						<img src="{{asset('webapp-assets\images\ic_menu_dots.png')}}" class="left-menu" alt="">
					</div>
					<form action="{{route('submit_profile_send_data')}}" method="post" enctype="multipart/form-data" class="loader">
					@csrf
					<div class="background-menu">
						<ul>
								<li><a href="{{route('income_turnover','income')}}">گردش مالی</a></li>
								<li><a href="{{route('account_info')}}">اطلاعات حساب بانکی</a></li>
								<li><a href="{{route('activity_details')}}">آمار و جزییات</a></li>
								<li><a href="{{route('increase_charge')}}">افزایش شارژ</a></li>
								<li><a href="tel:{{$results['shomareposhtibani']}}">تماس با پشتیبانی</a></li>
								<li><a target="_blank"  href="{{$results['aboutlink']}}">درباره چهارسو</a></li>
								<li><a href="{{route('logout')}}">خروج</a></li>
						</ul>
					</div>
			</div>

				<div class="div-arrow">
					<div class="arrow-img">
							<a href="{{route('dashboard')}}"><img src="{{asset('webapp-assets\images\ic_arrow_back.png')}}" class="arrow" alt=""></a>	
					</div>
				</div>

				<div class="top-gradient">
					<div class="circle-div">
							<label for="enterprofileimage">
								<div class="center-circle" >
									<img id="duc_img_profile" src="
									@if(!$results['personal_profile'])
									{{asset('webapp-assets\images\avatar-profile.png')}}
									@else
									https://panel.4sooapp.com/uploads/{{$results['personal_profile']}}
									@endif
									" alt="" >
									<input id="enterprofileimage" accept="image/*" onchange="showDocumentsProfileImage(this)" type="file" name="profileImg">
								</div>
							</label>
							<div class="person-name-profile">
								<span>{{$results['personal_firstname']}} 
									  {{$results['personal_lastname']}}
								</span>
							</div>
					</div>
				</div>

				<div class="big-info-box">
					<div class="info-box">
						<div class="top-info-box">
							<div class="row first-info">
								<div class="col-md-4 col-sm-4 col-xs-4 box rgt">
									<img class="history" src="{{asset('webapp-assets\images\ic_date_logo.png')}}" alt="">
									<h5>{{$results['weeks']}} ماه</h5>
									<h6>سابقه کار</h6>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4  box mdl">
									<img src="{{asset('webapp-assets\images\ic_sefaresh_logo.png')}}"
									alt="">
									<h5>{{$results['orders']}} </h5>
									<h6> سفارش تاکنون</h6>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4 box lft">
									<img src="{{asset('webapp-assets\images\ic_location_logo.png')}}"
									alt="">
									<h5>{{$results['personal_city']}} </h5>
									<h6>شهر‌محل‌خدمت</h6>
								</div>
								
							</div>
						</div>
						@if($defects > 0)
						<div class="bottom-info-box">
							<img src="{{asset('webapp-assets\images\ic_info.png')}}" class="img-sign"alt="">
							<span class="defect-message ">{{$defects}} مورد نقصی پرونده دارید</span>
						</div>
						@endif
						
					</div>
				</div>	
<!-- end of container-div -->

				<div class="container-profile-box">

					<div class="profile-box">
						<div class="row profile @if($defects == 0) nodefects  @endif">
							<div class="col-md-12 col-sm-12 col-xs-12 title "><h5><i id="edit_person_info" class="fa fa-edit " aria-hidden="true"></i>مشخصات فردی</h5>

										<hr>						
							</div>
						</div>

						<div class="person-info" id="person_info">
							<div class="row txt pl-0">
								<div class="col-md-6 col-sm-6 col-xs-6 text-info pr-2"><h5 class="bolder">نام : </h5> <h6 class="smaller">{{$results['personal_firstname']}} </h6>
							</div>
								<div class="col-md-6 col-sm-6 col-xs-6 text-info"><h5 class="bolder">نام خانوادگی :</h5> <h6 class="smaller">{{$results['personal_lastname']}}</h6>
								</div>
							</div>
							<div class="row txt">
								<div class="col-md-6 col-sm-6 col-xs-6 text-info pr-2"><h5 class="bolder">کد ملی:  </h5> <h6 class="smaller">{{$results['personal_national_code']}}</h6>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6 text-info"><h5 class="bolder">تاریخ تولد:</h5> <h6 class="smaller">{{$results['personal_birthday']}}</h6>
								</div>
							</div>
							<div class="row txt">
								<div class="col-md-6 col-sm-6 col-xs-6 text-info pr-2"><h5 class="bolder">وضعیت تاهل:</h5> <h6 class="smaller">{{$results['personal_marriage']}}</h6>
							</div>
								<div class="col-md-6 col-sm-6 col-xs-6 text-info"><h5 class="bolder"> تلفن همراه:</h5> <h6 class="smaller">{{$results['personal_mobile']}}</h6>
								</div>
							</div>
							<div class="row txt">
								<div class="col-md-12 col-sm-12 col-xs-12 p-2"><h5 class="bolder"> آخرین  مدرک تحصیلی:</h5> <h6 class="smaller">{{$results['personal_last_diploma']}}</h6>
								</div>
							</div>
						</div>

						<div  id="input_person_info" class="input-person-info p-0">
							<div class="row txt pl-0">
								<div class="col-md-6 col-sm-6 col-xs-6 text-info p-1">
									<h4 class="bolder"> نام : </h4>
									@if($results['personal_firstname'] == null)
									<input type="text" name="fname" value="{{$results['personal_firstname']}}">
									@else
									<input type="text" name="fname" readonly value="{{$results['personal_firstname']}}">
									@endif
								</div> 
								
								<div class="col-md-6 col-sm-6 col-xs-6 text-info p-1">
									<h4 class="bolder"> نام خانوادگی : </h4>
									@if($results['personal_lastname'] == null )
									<input type="text" name="lname" value="{{$results['personal_lastname']}}">
									@else
									<input type="text" name="lname" readonly value="{{$results['personal_lastname']}}">
									@endif
								</div>
							</div>
							<div class="row txt pl-0">
								<div class="col-md-6 col-sm-6 col-xs-6 text-info p-1">
									<h4 class="bolder"> کدملی : </h4>
									@if($results['personal_national_code'] == null )
									<input type="text" name="national_code" value="{{$results['personal_national_code']}}">
									@else
									<input type="text" name="national_code" readonly value="{{$results['personal_national_code']}}">
									@endif
								</div> 
								<div class="col-md-6 col-sm-6 col-xs-6 text-info p-1">
									<h4 class="bolder"> تاریخ تولد : </h4>
									<input type="text" id="input1"  name="birthday" readonly   value="{{$results['personal_birthday']}}">
								</div>
							</div>
							<div class="row txt pl-0">
								<div class="col-md-6 col-sm-6 col-xs-6 text-info p-1">
									<h4 class="bolder"> وضعیت تاهل : </h4>
									<select name="marriage" id="">
										<option @if($results["personal_marriage"] == 'مجرد') selected @endif value="مجرد"> مجرد </option>
										<option @if($results["personal_marriage"] == 'متاهل') selected @endif value="متاهل"> متاهل </option>
									</select>
								</div>								
								<div class="col-md-6 col-sm-6 col-xs-6 text-info p-1">
									<h4 class="bolder" style="display:block"> تلفن همراه : </h4>
									<h6 class="smaller">{{$results['personal_mobile']}}</h6>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6 text-info p-1">
									<h4 class="bolder"> اخرین وضعیت تحصیلی : </h4>
									<select name="study_status" id="">
										<option @if($results["personal_last_diploma"] == 'سیکل') selected @endif value="سیکل">سیکل</option>
										<option @if($results["personal_last_diploma"] == 'دیپلم') selected @endif value="دیپلم">دیپلم</option>
										<option @if($results["personal_last_diploma"] == 'فوق دیپلم') selected @endif value="فوق دیپلم">فوق دیپلم</option>
										<option @if($results["personal_last_diploma"] == 'لیسانس') selected @endif value="لیسانس">لیسانس</option>
										<option @if($results["personal_last_diploma"] == ' فوق لیسانس') selected @endif value="فوق لیسانس">فوق لیسانس</option>
									</select>
								</div>
							</div>
						</div>
						

					</div>

					
				<!-- second box -->

					<div class="profile-box">

						<div class="row profile-nexts">
							<div class="col-md-12 col-sm-12 col-xs-12 title "><h5><i class="fa fa-edit" aria-hidden="true"></i>مشخصات حرفه ای</h5>



										<hr>						
							</div>
							<!-- <div class="professional-info">
								<p class="details-info-right">نام:</p> <p class="details-info-left">fdhdj</p>
							</div> -->
						</div>

						<div class="row professional">
							<div class="col col-md-12 col-sm-12 col-xs-12 one">
								<h5>سابقه کار:</h5> <h6>
									@if($year !== null && $year !== 0)
									{{$year}}
									@else
									{{$results['personal_work_experience_month']}}
									@endif
									ماه
								</h6>
							</div>
							
						</div>


						<div class="row professional">
							<div class="col col-md-12 col-sm-12 col-xs-12 two">
								<h5> نام کارگزاری:</h5> <h6>{{$results['broker_name']}}</h6><br>
								<h5>زمینه فعالیت:</h5><h6></h6>
							</div>
							
						</div>
						
						<div class="row professional">
							<div class="col col-md-12 col-sm-12 col-xs-12 four">
								<h5> زمینه تخصص آزاد :</h5> <h6></h6><br>
								<h5>درباره متخصص:</h5><h6> </h6>
							</div>			
						</div>
					</div>

					<div class="profile-box">
						<div class="row profile-nexts">
							<div class="col-md-12 col-sm-12 col-xs-12 title "><h5> مدارک اولیه</h5>

										<hr>						
							</div>
							<!-- <div class="professional-info">
								<p class="details-info-right">نام:</p> <p class="details-info-left">fdhdj</p>
							</div> -->
						</div>

						<div class="row professional m-0">
							<div class="col-md-8 col-sm-8 col-xs-8 pl-0 pr-2 documents">
								<h5>
								@if(!$results['personal_identity_card_first_pic'])
								<i class="fa fa-times"></i>								
								@else
									@if($results['f_identify_checkout'] == 1)
									<i class="fa fa-check"></i>   
									@else
										<i class="far fa-clock"></i>
									@endif
								@endif
								تصویر صفحه اول شناسنامه
								</h5>
							</div>
								<div class="col-md-4 col-sm-4 col-xs-4 p-2 documents">
									<div class="img-input">	
									@if($results['f_identify_checkout'] == 1)
									<img id="duc_img_show" src="
										@if(!$results['personal_identity_card_first_pic'])
										{{asset('webapp-assets\images\empty-picture.jpg')}}
										@else
											https://panel.4sooapp.com/uploads/{{$results['personal_identity_card_first_pic']}}
										@endif
										" alt="">
									@else
									<label for="enter-image">
									<input id="enter-image" accept="image/*" onchange="showDocumentsImage(this)" type="file" name="identity_card_first_pic">
									<img id="duc_img" src="
										@if(!$results['personal_identity_card_first_pic'])
										{{asset('webapp-assets\images\empty-picture.jpg')}}
										@else
											https://panel.4sooapp.com/uploads/{{$results['personal_identity_card_first_pic']}}
										@endif
										" alt="">
									</label>
									@endif		
									
									</div>
								</div>											
						</div>

						<div class="row professional m-0">
							<div class="col-md-8 col-sm-8 col-xs-8 pl-0 pr-2 documents">
								<h5>
								@if(!$results['personal_identity_card_second_pic'])
								<i class="fa fa-times"></i>								
								@else
									@if($results['s_identify_checkout'] == 1)
									<i class="fa fa-check"></i>   
									@else
										<i class="far fa-clock"></i>
									@endif
								@endif
									تصویر صفحه دوم شناسنامه
								</h5>
							</div>
								<div class="col-md-4 col-sm-4 col-xs-4 p-2 documents">
									<div class="img-input">	
									@if($results['s_identify_checkout'] == 1)	
									<img id="duc_img_showTwo" src="
										@if(!$results['personal_identity_card_second_pic'])
										{{asset('webapp-assets\images\empty-picture.jpg')}}
										@else
											https://panel.4sooapp.com/uploads/{{$results['personal_identity_card_second_pic']}}
										@endif
										" alt="">
									@else						
									<label for="enter-image2">
										<input id="enter-image2" accept="image/*" onchange="showDocumentsImage2(this)" type="file" name="identity_card_second_pic">
										<img id="duc_img2" src="
										@if(!$results['personal_identity_card_second_pic'])
										{{asset('webapp-assets\images\empty-picture.jpg')}}
										@else
											https://panel.4sooapp.com/uploads/{{$results['personal_identity_card_second_pic']}}
										@endif
										" alt="">
									</label>
									@endif
									</div>
								</div>											
						</div>

						<div class="row professional m-0">
							<div class="col-md-8 col-sm-8 col-xs-8 pl-0 pr-2 documents">
								<h5>
								@if(!$results['personal_status_duty'])
								<i class="fa fa-times"></i>								
								@else
									@if($results['duty_checkout'] == 1)
									<i class="fa fa-check"></i>   
									@else
										<i class="far fa-clock"></i>
									@endif
								@endif
									تصویر کارت پایان خدمت
								</h5>
							</div>	
								<div class="col-md-4 col-sm-4 col-xs-4 p-2 documents">
									<div class="img-input">	
									@if($results['duty_checkout'] == 1)	
									<img id="duc_img_showThree" src="
										@if(!$results['personal_status_duty'])
										{{asset('webapp-assets\images\empty-picture.jpg')}}
										@else
											https://panel.4sooapp.com/uploads/{{$results['personal_status_duty']}}
										@endif
										" alt="">
									@else						
									<label for="enter-image3">
										<input id="enter-image3" accept="image/*" onchange="showDocumentsImage3(this)" type="file" name="status_duty">

										<img id="duc_img3" src="
										@if(!$results['personal_status_duty'])
										{{asset('webapp-assets\images\empty-picture.jpg')}}
										@else
											https://panel.4sooapp.com/uploads/{{$results['personal_status_duty']}}
										@endif
										" alt="">
									</label>
									@endif
									</div>
								</div>											
						</div>

						<div class="row professional m-0">
							<div class="col-md-8 col-sm-8 col-xs-8 pl-0 pr-2 documents">
								<h5>
								@if(!$results['personal_backgrounds_status'])
								<i class="fa fa-times"></i>								
								@else
									@if($results['antecedent_checkout'] == 1)
									<i class="fa fa-check"></i>   
									@else
										<i class="far fa-clock"></i>
									@endif
								@endif
									تصویر گواهی عدم سو پیشینه
								</h5>
							</div>	
								<div class="col-md-4 col-sm-4 col-xs-4 p-2 documents">
									<div class="img-input">	
									@if($results['antecedent_checkout'] == 1)	
									<img id="duc_img_showFour" src="
										@if(!$results['personal_backgrounds_status'])
										{{asset('webapp-assets\images\empty-picture.jpg')}}
										@else
											https://panel.4sooapp.com/uploads/{{$results['personal_backgrounds_status']}}
										@endif
									" alt="">
									@else					
									<label for="enter-image4">
										<input id="enter-image4" accept="image/*" onchange="showDocumentsImage4(this)" type="file" name="backgrounds_status">
										<img id="duc_img4" src="
										@if(!$results['personal_backgrounds_status'])
										{{asset('webapp-assets\images\empty-picture.jpg')}}
										@else
											https://panel.4sooapp.com/uploads/{{$results['personal_backgrounds_status']}}
										@endif
										" alt="">
									</label>
									@endif
									</div>
								</div>											
						</div>

						<div class="row professional m-0">
							<div class="col-md-8 col-sm-8 col-xs-8 pl-0 pr-2 documents">
								<h5>
								@if(!$results['personal_national_card_front_pic'])
								<i class="fa fa-times"></i>								
								@else
									@if($results['f_national_checkout'] == 1)
									<i class="fa fa-check"></i>   
									@else
										<i class="far fa-clock"></i>
									@endif
								@endif
								تصویر کارت ملی
								</h5>
							</div>	
								<div class="col-md-4 col-sm-4 col-xs-4 p-2 documents">
									<div class="img-input">	
									@if($results['f_national_checkout'] == 1)	
									<img id="duc_img_showFive" src="
										@if(!$results['personal_national_card_front_pic'])
										{{asset('webapp-assets\images\empty-picture.jpg')}}
										@else
											https://panel.4sooapp.com/uploads/{{$results['personal_national_card_front_pic']}}
										@endif
									" alt="">	
									@else					
									<label for="enter-image5">
										<input id="enter-image5" accept="image/*" onchange="showDocumentsImage5(this)" type="file" name="national_card_front_pic">

										<img id="duc_img5" src="
										@if(!$results['personal_national_card_front_pic'])
										{{asset('webapp-assets\images\empty-picture.jpg')}}
										@else
											https://panel.4sooapp.com/uploads/{{$results['personal_national_card_front_pic']}}
										@endif
										" alt="">
									</label>
									@endif
									</div>
								</div>
							
						</div>

						<div class="row professional m-0 ">
							<div class="col-md-8 col-sm-8 col-xs-8 pl-0 pr-2 documents">
									<h5>
									@if(!$results['personal_national_card_back_pic'])
									<i class="fa fa-times"></i>								
									@else
										@if($results['b_national_checkout'] == 1)
										<i class="fa fa-check"></i>   
										@else
										<i class="far fa-clock"></i>
										@endif
									@endif
									تصویر پشت کارت ملی
									</h5>
							</div>	 
									<div class="col-md-4 col-sm-4 col-xs-4 p-2 documents">
									<div class="img-input">
									@if($results['b_national_checkout'] == 1)								
										<img id="duc_img_showSix" src="
										@if(!$results['personal_national_card_back_pic'])
										{{asset('webapp-assets\images\empty-picture.jpg')}}
										@else
											https://panel.4sooapp.com/uploads/{{$results['personal_national_card_back_pic']}}
										@endif
										" alt="">
										@else					
									<label for="enter-image6">
										<input id="enter-image6" accept="image/*" onchange="showDocumentsImage6(this)" type="file" name="national_card_back_pic">

										<img id="duc_img6" src="
										@if(!$results['personal_national_card_back_pic'])
										{{asset('webapp-assets\images\empty-picture.jpg')}}
										@else
											https://panel.4sooapp.com/uploads/{{$results['personal_national_card_back_pic']}}
										@endif
										" alt="">
									</label>
									@endif
									</div>
									</div>					
						</div>
					</div>

					<div class="profile-box">
						<div class="row profile-nexts ">
								<div class="col-md-12 col-sm-12 col-xs-12 title "><h5><i id="edit_adress" class="fa fa-edit" aria-hidden="true"></i> اطلاعات تماس</h5>

											<hr>						
								</div>
						</div>
						<div id="show_adress" class="show_container">

							<div class="row professional m-0">
								<div class="col col-md-12 col-sm-12 col-xs-12 adress">
									<h5> شهر:</h5>
									<h6>{{$results['personal_city']}}</h6>
								</div>					
							</div>

							<div class="row professional m-0">
								<div class="col col-md-12 col-sm-12 col-xs-12 adress">
									<h5>تلفن محل کار:</h5>
									<h6>{{$results['personal_office_phone']}}</h6>
								</div>					
							</div>

							<div class="row professional m-0">
								<div class="col col-md-12 col-sm-12 col-xs-12 adress">
									<h5>کد پستی:</h5>
									<h6>{{$results['personal_postal_code']}}</h6>
								</div>					
							</div>

							<div class="row professional m-0">
								<div class="col col-md-12 col-sm-12 col-xs-12 adress">
									<h5> تلفن منزل:</h5>
									<h6>{{$results['personal_home_phone']}}</h6>
								</div>					
							</div>

							<div class="row professional m-0">
								<div class="col-md-12 col-sm-12 col-xs-12 pl-0 adress">
									<h5>نشانی منزل:</h5>
									<h6>{{$results['personal_address']}}</h6>
								</div>					
							</div>
						</div>
						

						<div  id="hidden_adress" class="hidden_container">
							<div class="row professional m-0">
								<div class="col-md-4 col-sm-4 col-xs-4 pl-0 adress">
									<h5> شهر:</h5>
								</div>
								<div class="col-md-8 col-sm-8 col-xs-8 mr-0 pr-0 pl-1 adress">	
								<select name="city_name" id="">
								@foreach($cityResults['data'] as $city)
										<option @if($results["personal_city"] == $city['city_name']) selected @endif value="{{$city['city_name']}}">{{$city['city_name']}}</option>
								@endforeach	
								</select>
								</div>					
							</div>
							<div class="row professional m-0">
								<div class="col-md-4 col-sm-4 col-xs-4 pl-0 adress">
									<h5>تلفن محل کار:</h5>
								</div>
								<div class="col-md-8 col-sm-8 col-xs-8 mr-0 pr-0 pl-1 adress">
								<input type="text" name="office_phone" value="{{$results['personal_office_phone']}}">	
								</div>					
							</div>
							<div class="row professional m-0">
								<div class="col-md-4 col-sm-4 col-xs-4 pl-0 adress">
									<h5>کدپستی:</h5>
								</div>
								<div class="col-md-8 col-sm-8 col-xs-8 mr-0 pr-0 pl-1 adress">
								<input type="text" name="post_code" value="{{$results['personal_postal_code']}}">	
								</div>					
							</div>
							<div class="row professional m-0">
								<div class="col-md-4 col-sm-4 col-xs-4 pl-0 adress">
									<h5>تلفن منزل:</h5>
								</div>
								<div class="col-md-8 col-sm-8 col-xs-8 mr-0 pr-0 pl-1 adress">
								<input type="text" name="home_phone" value="{{$results['personal_home_phone']}}">	
								</div>					
							</div>
							<div class="row professional m-0">
								<div class="col-md-4 col-sm-4 col-xs-4 pl-0 adress">
									<h5>نشانی منزل:</h5>
								</div>
								<div class="col-md-8 col-sm-8 col-xs-8 mr-0 pr-0 pl-1 adress">
								<input type="text" name="adress" value="{{$results['personal_address']}}">	
								</div>					
							</div>
						</div>
					</div>
					<!-- end of container profile-box -->
				<div class="down-btn-record">
					<button type="submit" id="record_change">ثبت تغییرات</button>
				</div>
			</form>
		</div>
				

				<div id="myModal_identify" class="modal-show">
                        <!-- The Close Button -->
                        <span class="close" id="close_btn">&times;</span>                       
                        <!-- Modal Content (The Image) -->
                        <img class="modal-content-show" id="imgIdentify">
            	</div>
		
    </div>
	@endsection

	@section('js')
	<script>
	$(document).ready(function(){
  			$(".left-menu").click(function(){
    		$(".background-menu").show(100);
    		return false;
  			});

  			$('#tab-home').click(function(){
  				$(".background-menu").hide(100);
  			});

  			$("#exit-btn").click(function(){
    		$(".exit-modal").show(100);
    		return false;
  			});
  			$("#tab-home").click(function(){
    		$(".exit-modal").hide(100);
  			});

			$("#edit_adress").click(function(){
  			$("#show_adress").hide(100);
    		$("#hidden_adress").show();   		
    		$("#edit_adress").hide(); 
			$("#record_change").show(500);   		
  			}); 

			$("#edit_person_info").click(function(){
  			$(".person-info").hide(100);
    		$(".input-person-info").show();   		
    		$("#edit_person_info").hide();   		
    		$("#record_change").show(500);   		
  			}); 

			  
			$("#enterprofileimage").click(function(){  		
    		$("#record_change").show(500);   		
  			}); 
  			
			  
			$("#duc_img").click(function(){
			$("#record_change").show(1000);
			}); 
			$("#duc_img2").click(function(){
			$("#record_change").show(1000);
			}); 
			$("#duc_img3").click(function(){
			$("#record_change").show(1000);
			}); 
			$("#duc_img4").click(function(){
			$("#record_change").show(1000);
			});
			$("#duc_img5").click(function(){
			$("#record_change").show(1000);
			});
			$("#duc_img6").click(function(){
			$("#record_change").show(1000);
			});

		});

	function showDocumentsImage(fileInput) {
    	var files = fileInput.files;
	    for (var i = 0; i < files.length; i++) {
		    var file = files[i];
		    var imageType = /image.*/;
		    if (!file.type.match(imageType)) {
		    continue;
		    }
		    var img=document.getElementById("duc_img");
		    img.file = file;
		    var reader = new FileReader();
		    reader.onload = (function(aImg) {
			    return function(e) {
			    aImg.src = e.target.result;
			    };
		    })(img);
		    reader.readAsDataURL(file);
	    }
	}


	function showDocumentsImage2(fileInput) {
    	var files = fileInput.files;
	    for (var i = 0; i < files.length; i++) {
		    var file = files[i];
		    var imageType = /image.*/;
		    if (!file.type.match(imageType)) {
		    continue;
		    }
		    var img=document.getElementById("duc_img2");
		    img.file = file;
		    var reader = new FileReader();
		    reader.onload = (function(aImg) {
			    return function(e) {
			    aImg.src = e.target.result;
			    };
		    })(img);
		    reader.readAsDataURL(file);
	    }
	}


	function showDocumentsImage3(fileInput) {
    	var files = fileInput.files;
	    for (var i = 0; i < files.length; i++) {
		    var file = files[i];
		    var imageType = /image.*/;
		    if (!file.type.match(imageType)) {
		    continue;
		    }
		    var img=document.getElementById("duc_img3");
		    img.file = file;
		    var reader = new FileReader();
		    reader.onload = (function(aImg) {
			    return function(e) {
			    aImg.src = e.target.result;
			    };
		    })(img);
		    reader.readAsDataURL(file);
	    }
	}


	function showDocumentsImage4(fileInput) {
    	var files = fileInput.files;
	    for (var i = 0; i < files.length; i++) {
		    var file = files[i];
		    var imageType = /image.*/;
		    if (!file.type.match(imageType)) {
		    continue;
		    }
		    var img=document.getElementById("duc_img4");
		    img.file = file;
		    var reader = new FileReader();
		    reader.onload = (function(aImg) {
			    return function(e) {
			    aImg.src = e.target.result;
			    };
		    })(img);
		    reader.readAsDataURL(file);
	    }
	}

	

	function showDocumentsImage5(fileInput) {
    	var files = fileInput.files;
	    for (var i = 0; i < files.length; i++) {
		    var file = files[i];
		    var imageType = /image.*/;
		    if (!file.type.match(imageType)) {
		    continue;
		    }
		    var img=document.getElementById("duc_img5");
		    img.file = file;
		    var reader = new FileReader();
		    reader.onload = (function(aImg) {
			    return function(e) {
			    aImg.src = e.target.result;
			    };
		    })(img);
		    reader.readAsDataURL(file);
	    }
	}

	function showDocumentsImage6(fileInput) {
    	var files = fileInput.files;
	    for (var i = 0; i < files.length; i++) {
		    var file = files[i];
		    var imageType = /image.*/;
		    if (!file.type.match(imageType)) {
		    continue;
		    }
		    var img=document.getElementById("duc_img6");
		    img.file = file;
		    var reader = new FileReader();
		    reader.onload = (function(aImg) {
			    return function(e) {
			    aImg.src = e.target.result;
			    };
		    })(img);
		    reader.readAsDataURL(file);
	    }
	}


	function showDocumentsProfileImage(fileInput) {
    	var files = fileInput.files;
	    for (var i = 0; i < files.length; i++) {
		    var file = files[i];
		    var imageType = /image.*/;
		    if (!file.type.match(imageType)) {
		    continue;
		    }
		    var img=document.getElementById("duc_img_profile");
		    img.file = file;
		    var reader = new FileReader();
		    reader.onload = (function(aImg) {
			    return function(e) {
			    aImg.src = e.target.result;
			    };
		    })(img);
		    reader.readAsDataURL(file);
	    }
	}

var modal = document.getElementById("myModal_identify");
var close_btn = document.getElementById("close_btn");
window.onclick = function(event) {
  if ((event.target == modal) || (event.target == close_btn) ){
    modal.style.display = "none";
  }
}

// Get the image and insert it inside the modal - use its "alt" text as a caption
var modalImg = document.getElementById("imgIdentify");

@if($results['f_identify_checkout'] == 1)
var img = document.getElementById("duc_img_show");
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
}
@endif

@if($results['s_identify_checkout'] == 1)
var imgTwo = document.getElementById("duc_img_showTwo");
imgTwo.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
}
@endif

@if($results['duty_checkout'] == 1)	
var imgThree = document.getElementById("duc_img_showThree");
imgThree.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
}
@endif

@if($results['antecedent_checkout'] == 1)
var imgFour = document.getElementById("duc_img_showFour");
imgFour.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
}
@endif

@if($results['f_national_checkout'] == 1)	
var imgFive = document.getElementById("duc_img_showFive");
imgFive.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
}
@endif

@if($results['b_national_checkout'] == 1)	
var imgSix = document.getElementById("duc_img_showSix");
imgSix.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
}
@endif
// Get the <span> element that closes the modal


</script>
	
	@endsection