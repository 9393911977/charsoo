	@extends('layouts.template')
	@section('content')
		
		<div id="tab-hom" class="tab tab-active tab-home" style="background-image:url({{asset('webapp-assets/images/login_bg.png')}}') ; background-size:cover;">
        <!-- home -->
        	<div class="img-top">
	        	<div class="row top ">
	        		<div class="col-md-3 col-sm-3 col-xs-3 right-img">
	        			<a href="{{route('dashboard')}}"><img  class="arrow-img"src="{{asset('webapp-assets\images\ic_arrow_back.png')}}" alt=""></a>	
	        		</div>
	        		<div class="col-md-6 col-sm-6 col-xs-6 middle-txt">
	        			<span>ویرایش اطلاعات حرفه ای</span>
	        		</div>
	        		<div class="col-md-3 col-sm-3 col-xs-3 left-img">	        
	        		</div>       		
	        	</div>
        		<img src="{{asset('webapp-assets/images/top-bg.png')}}" alt="">
        	</div>
        	<div class="content-question">
                		<p>زمینه تخصص</p>
                	</div>
            <div class="desc-wrapper">
                <div class="desc-inner" style="position:relative;padding: 0px 15px;margin-top:2px;min-height: 68px">
                	<div class="row professional-inf">
                		<div class="col-md-10 col-ms-10 col-xs-10 expertise-info">
                			<div class="expertise-img-div"><img src="{{asset('webapp-assets\images\computer.png')}}" alt="">
                			</div>
                			<p>عنوان را انتخاب کنید</p>
                		</div>
                		<div class="col-md-2 col-ms-2 col-xs-2 check-expertise">
                			<div class="inner-check-expertise">
                				<input type="checkbox">
                			</div>
                		</div>
                	</div>
                	<hr style="margin-top:0;margin-bottom: .5rem;">
                	<div class="row professional-inf">
                		<div class="col-md-10 col-ms-10 col-xs-10 expertise-info">
                			<div class="expertise-img-div"><img src="{{asset('webapp-assets\images\computer.png')}}" alt="">
                			</div>
                			<p>عنوان را انتخاب کنید</p>
                		</div>
                		<div class="col-md-2 col-ms-2 col-xs-2 check-expertise">
                			<div class="inner-check-expertise">
                				<input type="checkbox">
                			</div>
                		</div>
                	</div>
                </div>
           </div>

           <div class="desc-wrapper">
                <div class="desc-inner" style="position:relative;padding: 0px 15px;margin-top:2px;min-height: 68px">
                	<div class="center-title">
                		<p>سابقه تخصص</p>
                	</div>
                	<hr style="margin-top:0;margin-bottom: 0rem;">
                	<div class="center-title">
                		<p style="font-weight:100;font-size: 12px;margin-bottom: 5px; ">چه مدت است در این تخصص ها فعالیت داشته اید؟</p>
                	</div>
                	<div class="row p-0 m-0">
                		<div class="col-md-6 col-ms-6 col-xs-6 input-time">
                			<div class="div-input-time">
                				<input type="text">                				
                			</div>
                			<div class="time-label"><p>ماه</p></div>
                		</div>
                		<div class="col-md-6 col-ms-6 col-xs-6   input-time">
                			<div class="year-time-label"><p>سال</p>
                			</div>
                			<div class="div-input-time-year">
                				<input type="text">                				
                			</div>
                			
                		</div>
                	</div>
                </div>
           </div>

           <div class="desc-wrapper">
                <div class="desc-inner" style="position:relative;padding: 0px 15px;margin-top:2px;min-height: 68px">
                	<div class="center-title">
                		<p>درباره من</p>
                	</div>
                	<hr style="margin-top:0;margin-bottom: 0rem;">
                	<div class="center-title">
                		<p style="font-weight:100;font-size: 12px;margin-bottom: 5px; ">چند جمله ای درباره شما...</p>
                	</div>
                	<div class="input-about">
                		<textarea></textarea>
                	</div>
                </div>
           </div>
           <br>
	            <div class="start-btn">
	            	<input type="button" value="ثبت تغییرات" class="start-button">
	        	</div>   
			</div>

			@endsection
