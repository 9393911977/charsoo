	@extends('layouts.template')
	@section('content')
	
  <div class="big-all-container">
    <div id="tab-home" class="tab tab-active tab-home" style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}') ; background-size:cover;">
        <!-- home -->

        <!-- navbar home -->
        <div class="navbar-wrapper">
        	<div class="img-div">
        		<img src="{{asset('webapp-assets\images\topbackground2.png')}}" alt="">
        	</div>
            
            <div class="nav-titr" style="background-image: url('{{asset('webapp-assets/images/top-bg.png')}}');">
            	<p class="bolder">چهارسو</p>
            	<p class="titr-day">{{$date}}</p>
            	
            </div>
                
        
        	<div class="big-box">
            	<div class="container-fluid p-0">
	                    
                    <div class="row img">
						<div class="col-md-4 col-sm-4 col-xs-4 p-0 card">
						<a href="{{route('profile')}}">
                        	<div class="profile-image">
							<img src="@if(!$results['profilepic'])
							{{asset('webapp-assets\images\avatar-profile.png')}}
							@else
							https://panel.4sooapp.com/uploads/{{$results['profilepic']}}
							@endif
							" class="person" alt="">
							</div>
                        	<p class="p-person-name" >{{$results['namefname']}}</p><br>
							
								<p class="p-person-enter">ورود به پروفایل</p>
						</a>
                        </div>

						<a href="{{route('income_turnover','income')}}">
                        <div class="col-md-4 col-sm-4 col-xs-4 p-0 card">
							<p>درآمد</p>
                        	<p class="nmb">{{$incomecash}}</p>
                        	<p class="const">هزار تومان</p>
								<p style="color:#2b368c;font-weight: bold;">گردش درآمد</p>
						</div>
						</a>

						<a href="{{route('increase_charge')}}">
                        <div class="col-md-4 col-sm-4 col-xs-4 p-0 card">
                        	<p>شارژ</p>
                        	<p class="nmb">{{$chargecash}}</p>
                        	<p class="const">هزار تومان</p>
								<p style="color:#2b368c;font-weight: bold;">افزایش شارژ</p>
						</div>
						</a>
					</div>

					<div class="row hrz">
                        <div class="col-md-4 col-sm-4 col-xs-4 p-1 scard" id="support">
							 <img src="{{asset('webapp-assets\images\headset.png')}}" alt="" class="hrz-img"><a href=""><div class="hrz-p" id="suport_btn">پشتیبانی</div></a>
                    	</div>

						<a href="{{route('account_info')}}">
						<div class="col-md-8 col-sm-8 col-xs-8 p-1 scard">
							<img style="margin-right:5px;" src="{{asset('webapp-assets\images\c-cart.png')}}" class="hrz-img"alt="">
							<div class="hrz-p">وضعیت حساب بانکی</div>
                    	</div>
					</a> 
					</div>

						@if($results['notifications'])
							@foreach($results ['notifications'] as $reports )
					<div class="row ">
							<div class="col md-12 col sm-12 t p-2" >
			                 <div class="title-body-right"> {{$reports['title']}}</div>
			                  <div class="title-body-left">
							  {{\Morilog\Jalali\Jalalian::forge($reports['created_at'])->format('%d/ %m/ %Y')}}
							 </div>
			                  <br>
			                  <br>
		                    <div class="text-body"> 
							{{$reports['text']}}
							</div>
							@if($reports['link'] !== null)
							<a target="_blank" href="{{$reports['link']}}"><div class="news">لینک اطلاعیه</div></a>
							@endif
	                    </div>
                    </div>
							@endforeach
						@endif


		            <div class="row"><!---state--->
					<a href="{{route('activity_details')}}">
						<div class="col md-4 col sm-4 s "><img src="{{asset('webapp-assets\images\chart.png')}}" alt="">
							<div class="tlonger">آمار و  جزییات فعالیت</div>
						</div>
					</a>
					<a href="{{route('my_skills')}}">
						<div class="col md-4 col sm-4 s "><img src="{{asset('webapp-assets\images\helphand.png')}}" alt=""><div>مهارت های من</div></div>
					</a>
						<a href="{{route('invite_friends')}}">
						<div class="col md-4 col sm-4 s "><img src="{{asset('webapp-assets\images\gift.png')}}" alt="">
						<div>شارژ رایگان</div></div>
						</a>
            		</div><!----end of state---->

            		<!----progres---->
            		<div class="row">
            			<div class="col md-12 col sm-12 progress">
            				<p class="p-progress">امتیاز کل  شما: <span style="font-size:14px">{{$results['emtiaz']}}</span></p>
            				<div class="progress-div">
            					<img src="{{asset('webapp-assets\images\comment.png')}}" alt="">
            					<span class="p-progress-div"></span>
            				</div>
            				<div class="blue-bar">
								<progress id="prg-value" value="{{$results['emtiaz']}}" max="100"> </progress>
								<p class="right-number">0</p>
								<p class="left-number">100</p>
							</div>
            			</div>
            		</div><!---end of progress---->  

				<br>
				<br><br>
            	</div>
			</div>
			@include('footer.footer')
        </div>
    </div>
	
    <div class="media-modal">
	    	<div class="modal-div">
		    	<div class="content-media">
		    		<p>از چه طریقی قصد ارتباط با  واحد  پشتیبانی را دارید؟</p>
		    		<br><br>
					<a href="tel:{{$results['shomareposhtibani']}}"><button >تماس</button></a>
					<a href="https:\\t.me\{{$results['telegramposhtibani']}}"><button class="lft-btn">تلگرام</button></a>
		    	</div>
	    	</div>
	</div>

 </div>
	@endsection
	
	@section('js')

	<script>

		$('.divide').divide({delimiter: ',',
		divideThousand: true});
	</script>
	<script>
	$(document).ready(function(){


			$("#openModal").click(function(){
				$(".my-modal").fadeIn();
			});
			$("#closeModal").click(function(){
				$(".my-modal").fadeOut();
			});

			$("#support").click(function(){
				$(".media-modal").fadeIn(100);	
				return false;
			});
  			$(".big-all-container").click(function(){
    		$(".media-modal").hide(100);
  			});

	})
	
	</script>
	@endsection