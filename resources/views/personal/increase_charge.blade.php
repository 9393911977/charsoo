	@extends('layouts.template')
	@section('content')
		
		<div id="tab-hom" class="tab tab-active tab-home" style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}') ; background-size:cover;">
        <!-- home -->
        	<div class="img-top">
	        	<div class="row top ">
	        		<div class="col-md-4 col-sm-4 col-xs-4 right-img">
	        			<a href="{{route('dashboard')}}"><img  class="arrow-img"src="{{asset('webapp-assets\images\ic_arrow_back.png')}}" alt="">	</a>
	        		</div>
	        		<div class="col-md-4 col-sm-4 col-xs-4 middle-txt">
	        			<span> افزایش شارژ</span>
	        		</div>
	        		<div class="col-md-4 col-sm-4 col-xs-4 left-img">
	        			<div class="recycle-div">
	        				<a href="{{route('income_turnover' , 'charge')}}">
	        					<i class="fas fa-recycle"></i>
	        					<p>گردش شارژ</p>
	        				</a>
	        			</div>
	        			
	        		</div>       		
	        	</div>
        		<img src="{{asset('webapp-assets/images/top-bg.png')}}" alt="">
        	</div>
        	<div class="desc-wrapper">
                <div class="desc-inner" style="position:relative;padding: 0px 15px;margin-top:12px;min-height: 68px">
                    <div class="yellow-right-sign"><img src="{{asset('webapp-assets\images\ic_info.png')}}" alt="">
                    </div>
                    <div class="charge-alarm-msg"><p>شما میتوانید میزان شارژ حساب خود را از طریق درگاه بانکی یا از طریق میزان درآمد خود
 					افزایش دهید و برای کارهای مختلف هزینه کنید.</p>
					</div>
                </div>
           </div>

            <div class="desc-wrapper">
                <div class="desc-inner" style="position:relative;padding: 6px 0 6px 15px;">
                    <h4 style="text-align:center;font-size: 14px;">موجودی شما</h4>
                    <hr style="margin:.5rem 0;">
                    <div class="row charge mr-0">
                        <div class="col-md-4 col-sm-4 col-xs-4 p-0 btn">
                        	<button>شارژ</button>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8 p-0 supply">
                        	<img src="{{asset('webapp-assets/images/packet.png')}}" alt="">
                        	<h5 class="value">
							{{number_format($results['chargecash'])}}
							</h5>
                        	<h5 class="tooman">تومان</h5>
                        </div>

					</div>
					<div class="row charge mr-0">
                        <div class="col-md-4 col-sm-4 col-xs-4 p-0 btn">
                        	<button>درآمد</button>
                        </div>

                        <div class="col-md-8 col-sm-8 col-xs-8 p-0 supply">
                        	<img src="{{asset('webapp-assets/images/coin.png')}}" alt="">
                        	<h5 class="value">
							{{number_format($results['incomecash'])}}
							</h5>
                        	<h5 class="tooman">تومان</h5>
                        </div>

					</div>
                </div>
            </div>

            <div class="charge-question-txt">
            	<div class="content-question">
            		<p>شارژ خود را از چه طریقی پرداخت می کنید؟</p>
            	</div>
            </div>

            <div class="charge-ways">
			<form action="{{route('send_increase_charge')}}" method="post" id="chargeForm">
					@csrf 
            	<div class="charge-ways-content">
            		<div class="row charge-way">
					
            			<input type="radio" name="pay" id="bank" value="bank" class="pay-input">
            			<label for="bank">
            				<div class="col-md-6 col-sm-6 col-xs-6 right boxes">
            					<img src="{{asset('webapp-assets/images/hand-keyboard.png')}}" alt="">
            					<p>درگاه بانکی</p>
            					<small>پرداخت به صورت اینترنتی</small>
            				</div>
            			</label>
            			<input type="radio" name="pay" id="charge" value="income" class="pay-input">
            			<label for="charge">
            				<div class="col-md-6 col-sm-6 col-xs-6 left boxes">
            				<img src="{{asset('webapp-assets/images/charsoo-packet.png')}}" alt="">
            					<p>درآمد چهارسو</p>
            					<small>حداکثر300هزار تومان</small>
            				</div>
            			</label>
            			
            		</div>
            	</div>		
            </div>
             <div class="charge-question-txt">
            	<div class="content-question">
            		<p>مقدار شارژ را مشخص کنید</p>
            	</div>
            </div>
            <div class="charge-ways">
            	<div class="charge-ways-content">
		            <div class="row charge-value">
		            	<div class="col-md-4 col-sm-4 col-xs-4 right">
		            		<button type="button" id="right-btn">20 هزار تومان</button>
		            	</div>
		            	<div class="col-md-4 col-sm-4 col-xs-4 middle">
		            		<button type="button" id="middle-btn">50 هزار تومان</button>
		            	</div>
		            	<div class="col-md-4 col-sm-4 col-xs-4 left">
		            		<button type="button" id="left-btn">100 هزار تومان</button>
		            	</div>
		            </div>
				</div>    
			</div>
				<div class="charge-ways">
            	<div class="charge-ways-content">
            		<div class="input-container">
            			<div class="leftbtn"><p>تومان</p></div>
            			<input type="text"  id="input-value" name="amount" placeholder="افزودن اعتبار به میزان دلخواه...">
            		</div>
				</div>    
				</div>

				<div class="charge-ways">
	            	<div class="charge-ways-content" style="text-align: center;">
	            		<button class="sale-btn" type="submit">افزایش اعتبار</button>
						</form>
					</div>    
				</div>
							              <br>
			</div>

			@endsection

			@section('js')

    <script>
		$(document).ready(function(){
  			$("#right-btn").click(function(){
    		$("#input-value").val(20000);
  			});

  			$("#middle-btn").click(function(){
  				$("#input-value").val(50000);
  			});

  			$("#left-btn").click(function(){
  				$("#input-value").val(100000);
  			});
  			
			});

	</script>

			@endsection