	@extends('layouts.template')
	@section('content')
		
		<div id="tab-hom" class="tab tab-active tab-home" style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}') ; background-size:cover;">
        <!-- home -->
        	<div class="img-top " style="background-image: url('webapp-assets/images/top-bg.png')">
	        	<div class="row top ">
	        		<div class="col-md-3 col-sm-3 col-xs-3 right-img">	
                       <a href="{{route('dashboard')}}"><img  class="arrow-img"src="{{asset('webapp-assets\images\ic_arrow_back.png')}}" alt=""></a> 
	        		</div>
	        		<div class="col-md-6 col-sm-6 col-xs-6 middle-txt">
	        			<span>آمار و جزییات فعالیت من</span>
	        		</div>
	        		<div class="col-md-3 col-sm-3 col-xs-3 left-img">	        
	        		</div>       		
	        	</div>
        		
        	</div>

            <div class="wrapper-activity">
                <div class="inner-activity">
                    <div class="row m-0">
                       <div class="col-md-6 col-sm-6 col-xs-6 order-numbers pr-2 pl-1">
                            <div class="order-products-numbers">
                                <p>تعداد سفارشات کالاها:</p>
                                <div class="bottom-div"><p>{{$results['goodsorders']}}</p>
                                </div>
                            </div>
                       </div>
                       <div class="col-md-6 col-sm-6 col-xs-6 order-numbers pr-1 pl-2">
                            <div class="order-products-numbers-left">
                                <p>تعداد سفارش ها:</p>
                                <div class="bottom-div"><p>{{$results['orders']}}</p>
                                </div>
                            </div>
                       </div>
                   </div>
                   <div class="row mr-0 ml-0" style="margin-top: 10px;">
                       <div class="col-md-12 col-sm-12 col-xs-12 order-numbers pr-2 pl-2">
                            <div class="rating-div">
                                <p>امتیاز این هفته شما:</p>
                                <div class="bottom-div">
                                @if($remain > 0)
                                    <i class="fa fa-star-half" aria-hidden="true"></i>  
                                @endif  
                                @for($i = 1 ; $i <= $intScores ; $i++)                           
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                @endfor
                                                                               
                                </div>
                            </div>
                       </div>
                   </div>

                   <div class="row mr-0 ml-0" style="margin-top: 10px;">
                       <div class="col-md-12 col-sm-12 col-xs-12 order-numbers pr-2 pl-2">
                            <div class="activity-message">
                                <p>ویژگی های خوب و بد این هفته:</p>
                            </div>
                       </div>
                </div>
                
            </div>
        	
             
			</div>

			@endsection
	