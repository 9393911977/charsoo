@extends('layouts.template')
    @section('content')
	
	 <div id="tab-home" class="tab tab-active tab-home" style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}') ; background-size:cover;">
        <!-- home -->
        <div class="img-top">
        	<div class="row top ">
        		<div class="col-md-3 col-sm-3 col-xs-3 right-img">
                    <a href="{{route('dashboard')}}"><img  class="arrow-img"src="{{asset('webapp-assets\images\ic_arrow_back.png')}}" alt=""></a>
                </div>
        		<div class="col-md-6 col-sm-6 col-xs-6 middle-txt">
        			<span>دعوت از دوستان</span>
        		</div>
        		<div class="col-md-3 col-sm-3 col-xs-3 left-img">
        		</div>       		
        	</div>
        		<img src="{{asset('webapp-assets/images/top-bg.png')}}" alt="">
        </div>
        <div class="header-wrapper">
                <div class="header-inner">
                    <img class="image-header" src="{{ asset('webapp-assets/images/invite.jpg') }}" alt="">
                
                </div>
                <div class="box-wrapper">
                    <div class="box-inner">
                        <h3>دوستان خود را دعوت کنید</h3>
                        <h3>هردو اعتبار دریافت کنید</h3>
                    </div>
                </div>
                <div class="gift-wrapper">
                    <div class="text-inner">
                        پس از اتمام اولین سفارش دوستتان , مبلغ  تومان اعتبار دریافت خواهید کرد .
                    </div>
                    <div class="gift-inner">
                        <img class="image-gift" src="{{ asset('webapp-assets/images/gift-pack.png') }}" alt="">
                    </div>
                </div>
                
                <div class="btn-wrapper">
                    <h4>کد دعوت خود را با دوستان و آشنایان خود به اشتراک بگذارید.</h4>
                    <br><br>
                    <button id="openModal" class="mybtn">اشتراک گذاری</button>
                </div>
            </div>
            
     </div>
     <div id="closeModal" class="my-modal">
    <div class="modal-wrapper">
        <div class="modal-inner">
            <div class="modal-head">
                <!-- <span id="closeModal" class="dismis"><b><i class="fa fa-times"></i></b></span> -->
                متن زیر را کپی کنید و برای دوستان خود ارسال کنید .
            </div>
            <div class="modal-body" id="myText">
                هنگام ثبت نام در اپلیکیشن چهارسو در صورت وارد کردن کد معرف , مبلغ 15 هزار تومان شارژ هدیه دریافت کنید .<br>
                کد معرف : <b style="font-family:tahoma;"></b> <br>
                دانلود اپلیکیشن چهارسو از : <b style="font-family:tahoma;">https://cafebazaar.ir/app/com.mmbdev.charsoo</b>
            </div>
            <div class="modal-foot"><button onclick="copy('هنگام ثبت نام در اپلیکیشن چهارسو در صورت وارد کردن کد معرف , مبلغ 15 هزار تومان شارژ هدیه دریافت کنید . کد معرف :   دانلود اپلیکیشن چهارسو از : https://cafebazaar.ir/app/com.mmbdev.charsoo ')" class="mybtn">کپی متن</button></div>
            <input id="myInput" class="hiddentextforcopy" type="hidden" value="هنگام ثبت نام در اپلیکیشن چهارسو در صورت وارد کردن کد معرف , مبلغ 15 هزار تومان شارژ هدیه دریافت کنید . کد معرف :     دانلود اپلیکیشن چهارسو از : https://cafebazaar.ir/app/com.mmbdev.charsoo">
        </div>
    </div>
</div>
     @endsection
