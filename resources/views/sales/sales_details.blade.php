@extends('layouts.template')
@section('content')
        <form action="{{route('send_sales_details')}}" method="post" id="deliverForm" enctype="multipart/form-data">
        @csrf
            <input type="hidden" name="uniqueCode" value="{{$results['orderuniquecode']}}">
        <div id="tab-hom" class="tab tab-active tab-home" style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}') ; background-size:cover;">
        <!-- home -->
            <div class="img-top" style="margin-bottom: 10px;">
                <div class="row top ">
                    <div class="col-md-3 col-sm-3 col-xs-3 right-img">
                    @if($results['status'] == 'لغو شده' || $results['status'] == 'تحویل شده' )
                        <a href="{{route('sells_archive')}}"><img  class="arrow-img"src="{{asset('webapp-assets\images\ic_arrow_back.png')}}" alt=""></a>
                    @else
                        <a href="{{route('sales')}}"><img  class="arrow-img"src="{{asset('webapp-assets\images\ic_arrow_back.png')}}" alt=""></a>
                    @endif        
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 top-title">
                        <span>جزییات سفارش </span>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3 p-0 left-img"> 
                    @if($results['status'] == 'معلق' || $results['status'] == 'تایید فروشنده')
                    <a onclick="myFunction()"
                    href="{{route('sells_cancell' , $results['orderuniquecode'])}}">
                      <div class="cancellbtn" >
                      <i class="fa fa-window-close" aria-hidden="true"></i>
                      <p class="cnl">لغو</p>
                      </div>
                    </a>  
                    @endif
                                             
                    </div>              
                </div>
                <img src="{{asset('webapp-assets/images/top-bg.png')}}" alt="">
            </div>
           
           @if($results['status'] == 'آماده سازی')
           <div class="desc-wrapper">
                <div class="desc-inner" style="position:relative;padding: 10px 15px;min-height:32px;">
                    <h6 style="text-align: center;font-size: 13px;">سفارش در انتظار ارسال است...</h6>
                </div>
           </div>
           @endif

           @if($results['status'] == 'معلق')
           <div class="desc-wrapper">
                <div class="desc-inner" style="position:relative;padding: 10px 15px;min-height:32px;">
                    <h6 style="text-align: center;font-size: 13px;">سفارش در انتظار تایید است...</h6>
                </div>
           </div>
           <div class="desc-wrapper">
                <div class="desc-inner" style="position:relative;padding:5px">
                    <h6 style="text-align:center;">اطلاعات سفارش</h6>
                    <hr>
                    <div class="orderdetails">
                        <span class="right"><i style="color:brown;" class="fa fa-id-card"></i> کد سفارش :</span>
                        <span class="left">{{$results['orderuniquecode']}}</span>
                    </div>
                    <div class="orderdetails">
                        <span class="right"><i style="color:darkblue;" class="fa fa-shopping-bag"></i> مشتری:</span>
                        <span class="left"> 
                        {{$results['customer_name']}}
                        </span>
                    </div>
                    <div class="orderdetails">
                        <span class="right"><i style="color:darkgreen;" class="fa fa-phone"></i> تلفن :</span>
                        <span class="left">{{$results['customer_mobile']}}</span>
                    </div>
                    <div class="orderdetails">
                        <span class="right"><i style="color:darkred;" class="fa fa-calendar"></i> روز :</span>
                        <span class="left">{{\Morilog\Jalali\Jalalian::forge($results['deliverdate'])->format('%A %d %B %Y')}}</span>
                    </div>
                    <div class="orderdetails">
                        <span class="right"><i style="color:deeppink;" class="fa fa-clock"></i> ساعت :</span>
                        <span class="left">{{$results['delivertime']}}</span>
                    </div>
                    <div class="orderdetails">
                        <span class="right"><i style="color:gold;" class="fa fa-map-marker"></i> نشانی :</span>
                        <span class="left">{{$results['address']}}</span>
                    </div>
                    <div class="orderdetails">
                        <span class="right"><i style="color:gold;" class="fa fa-map-marker"></i> توضیحات مشتری :</span>
                        <span class="left"></span>
                    </div>
                </div>
            </div>
                @if($results['status'] == 'معلق' || $results['status'] == 'تایید فروشنده')
                    <div class="desc-wrapper">
                        <div class="desc-inner" style="position:relative;padding: 0px 15px;">
                            <div class="right-sign"><img src="{{asset('webapp-assets\images\ic_info.png')}}" alt=""></div>
                            <div class="alarm-msg"><p>خدمت رسان عزیز! در حال حاضر شارژ مشتری کمتر از مبلغ سفارش می باشد.مشتری تا قبل از زدن دکمه آماده سازی توسط شما برای شارژ کردن حساب خود فرصت دارد.
                            پس در صورت تمایل به دریافت اعتباری هزینه لطفا از مشتری بخواهید حساب خود را شارژ نماید.</p></div>
                        </div>
                    </div>
                @endif
           
           @endif

            <!-- if(2) -->
           @if($results['status'] == 'تایید فروشنده' || 
           $results['status'] == 'در حال آماده سازی' || 
           $results['status'] == 'ارسال شده' ||
           $results['status'] == 'تحویل شده' ||
           $results['status'] == 'لغو شده' )
                @if($results['status'] == 'تایید فروشنده')
           <div class="desc-wrapper">
                <div class="desc-inner" style="position:relative;padding: 10px 15px;min-height:32px;">
                    <h6 style="text-align: center;font-size: 13px;">سفارش مشتری در انتظار آماده سازی است...</h6>
                </div>
           </div>
                @elseif($results['status'] == 'در حال آماده سازی')
           <div class="desc-wrapper">
                <div class="desc-inner" style="position:relative;padding: 10px 15px;min-height:32px;">
                    <h6 style="text-align: center;font-size: 13px;">سفارش مشتری در انتظار ارسال است...</h6>
                </div>
           </div>
               
                @elseif($results['status'] == 'لغو شده')
                <div class="desc-wrapper">
                    <div class="desc-inner" style="position:relative;padding: 10px 15px;min-height:32px;">
                        <h6 style="text-align: center;font-size: 13px;">سفارش شما لغو گردیده است...</h6>
                    </div>
                </div>
                @endif
            @if($results['status'] !== 'لغو شده')
            <div class="desc-wrapper">
                <div class="desc-inner" style="position:relative;padding: 10px 15px;">
                    <h6 style="text-align:center;">نحوه تسویه حساب با مشتری</h6>
                    <hr>
                    <h6 class="value-price">با توجه به میزان شارژ حساب چهارسوی مشتری مقدار دریافتی شما برای تحویل کالا به شرح زیر می باشد</h6> 
                    <h6 class="value-price" style="margin-top: 10px;">مبلغ <p class="count-value" style="color:#444"><b>{{$results['creditamount']}}</b></p> تومان به صورت اعتبار در حساب درآمد شما واریز می گردد</h6> 
                    <h6 class="value-price">و شما باید مبلغ<p class="count-value" style="color:#444"><b>{{$results['cashamount']}}</b></p>  تومان به صورت نقدی از مشتری دریافت نمایید</h6> 
                </div>
            </div>
            @endif
                @if($results['status'] == 'ارسال شده')
                <div class="desc-wrapper">
                    <div class="desc-inner extra" style="position:relative;">
                        <hr style="margin-bottom: 0">
                        <div class="orderdetails">
                            <span class="right"> کد تایید تحویل را وارد کنید</span>
                            <br>
                            <input type="text" id="deliverCode" name="deliverCode" placeholder="کد را وارد کنید" 
                            style="border-radius:4px;margin: 6px auto auto;">
                        </div>
                    </div>
                </div>
                @endif

                @if($results['status'] == 'تحویل شده')
                <div class="desc-wrapper">
                    <div class="desc-inner" style="position:relative;padding: 10px 15px;min-height:32px;">
                        <h6 style="text-align: center;font-size: 12px;">تبریک! این سفارش هم به خوبی و خوشی تموم شد...</h6>
                    </div>
                </div>
                @endif
           <div class="desc-wrapper">
                <div class="desc-inner" style="position:relative;padding-top:14px">
                    <h6 style="text-align:center;">اطلاعات سفارش</h6>
                    <hr style="margin:8px 0">
                    <div class="orderdetails">
                        <span class="right"><i style="color:brown;" class="fa fa-id-card"></i> کد سفارش :</span>
                        <span class="left">{{$results['orderuniquecode']}}</span>
                    </div>
                    <div class="orderdetails">
                        <span class="right"><i style="color:darkblue;" class="fa fa-shopping-bag"></i> مشتری:</span>
                        <span class="left"> 
                        {{$results['customer_name']}}
                        </span>
                    </div>
                    <div class="orderdetails">
                        <span class="right"><i style="color:darkgreen;" class="fa fa-phone"></i> تلفن :</span>
                        <span class="left">{{$results['customer_mobile']}}</span>
                    </div>
                    <div class="orderdetails">
                        <span class="right"><i style="color:darkred;" class="fa fa-calendar"></i> روز :</span>
                        <span class="left">{{\Morilog\Jalali\Jalalian::forge($results['deliverdate'])->format('%A %d %B %Y')}}</span>
                    </div>
                    <div class="orderdetails">
                        <span class="right"><i style="color:deeppink;" class="fa fa-clock"></i> ساعت :</span>
                        <span class="left">{{$results['delivertime']}}</span>
                    </div>
                    <div class="orderdetails">
                        <span class="right"><i style="color:gold;" class="fa fa-map-marker"></i> نشانی :</span>
                        <span class="left">{{$results['address']}}</span>
                    </div>
                    <div class="orderdetails">
                        <span class="right"><i style="color:gold;" class="fa fa-map-marker"></i> توضیحات مشتری :</span>
                        <span class="left"></span>
                    </div>
                </div>
            </div>
            @if($results['status'] == 'در حال آماده سازی' || $results['status'] == 'تحویل شده')
           <div class="desc-wrapper">
                <div class="desc-inner" style="position:relative;padding-bottom: 8px;padding-top: 5px;">
                    <div class="orderdetails">
                    <div class="how-pay-div-right">
                        <span>نحوه پرداخت:</span><br>
                        <span>میزان نقدی:</span><br>
                        <span>میزان اعتباری:</span>
                    </div> 
                    <div class="how-pay-div-left">
                        <span>{{$results['paytype']}}</span><br>
                        <span>{{$results['cashamount']}}</span><br>
                        <span>{{$results['creditamount']}}</span>
                    </div>                              
                    </div>
                </div>
            </div>
            @endif
                @if($results['status'] == 'معلق' || $results['status'] == 'تایید فروشنده')
                <div class="desc-wrapper">
                    <div class="desc-inner" style="position:relative;padding: 0px 15px;">
                        <div class="right-sign"><img src="{{asset('webapp-assets\images\ic_info.png')}}" alt=""></div>
                        <div class="alarm-msg"><p>خدمت رسان عزیز! در حال حاضر شارژ مشتری کمتر از مبلغ سفارش می باشد.مشتری تا قبل از زدن دکمه آماده سازی توسط شما برای شارژ کردن حساب خود فرصت دارد.
                        پس در صورت تمایل به دریافت اعتباری هزینه لطفا از مشتری بخواهید حساب خود را شارژ نماید.</p></div>
                    </div>
                </div>
                @endif
           @endif
           <div class="desc-wrapper">
                <div class="desc-inner" style="position:relative;padding: 10px 15px;">
                    <h4 style="text-align:center;font-size: 14px;">فاکتور</h4>
                    <hr style="margin-top: .5rem;">
                    <div class="faktordetails head" style="font-size: 12px;">
                        <div class="faktor-item prod">محصول</div>
                        <div class="faktor-item name">نام</div>
                        <div class="faktor-item numb">تعداد</div>
                        <div class="faktor-item pric">قیمت</div>
                    </div>
                    @foreach($results['products'] as $key=>$item)
                        <div class="faktordetails body"style="font-size: 11px;">
                            <div class="faktor-item prod"><img src="{{asset('https://panel.4sooapp.com/uploads/'.$item['product_picture'])}}" id="firstProductsImage{{$item['id']}}" width="80px" alt=""></div>
                            <div class="faktor-item name">{{$item['product_name']}}</div>
                            <div class="faktor-item numb">{{$results['counts'][$key]}}</div>
                            <div class="faktor-item pric">{{$item['product_price']}}</div>
                        </div>
                    @endforeach    
                    <hr>

                    <div class="faktor-details" style="font-size: 11px;padding: 0px;">

                        <div class="faktor-details-item">
                            <div class="title">محصولات :</div>
                            <div class="data ">{{$results['prices']}}</div>
                            <div class="toman">تومان</div>
                        </div>
                        <div class="faktor-details-item">
                            <div class="title">اقلام همراه :</div>
                            <div class="data ">0</div>
                            <div class="toman">تومان</div>
                        </div>
                        <div class="faktor-details-item">
                            <div class="title">بسته بندی :</div>
                            <div class="data ">{{$results['packingprice']}}</div>
                            <div class="toman">تومان</div>
                        </div>
                        <div class="faktor-details-item">
                            <div class="title"> حمل و نقل: </div>
                            <div class="data ">
                            @if($results['sendingprice'] == 0 ) رایگان
                            @elseif($results['sendingprice'] == -1 ) به عهده مشتری
                            @else($results['sendingprice'] >= 1) {{$results['sendingprice']}}
                            @endif
                            </div>
                            <div class="toman">تومان</div>
                        </div>
                        <hr style="color:darkgray;height:0.5px;">
                        <div class="faktor-details-item">
                            <div class="title">جمع فاکتور :</div>
                            <div class="data ">{{$results['totalamountitems']}}</div>
                            <div class="toman">تومان</div>
                        </div>

                </div>                 
                </div>
            </div>

            @if($results['status'] == 'در حال آماده سازی')
            <div class="desc-wrapper">
                <div class="desc-inner" style="position:relative;padding-bottom: 8px;">
                    <h6 class="price-title">بارگذاری فاکتور</h6>
                    <span class="right-show">درصورت تمایل تصویر فاکتور را بارگذاری کنید</span>
                    <div class="factor-div">
                        <div class="factor-img">                               
                            <label for="sellImg1">
                            <input accept="image/*" onchange="showFactorImage(this)" type="file" name="sellFactorImg" id="sellImg1">
                            <img id="thumbFactor" src="{{asset('webapp-assets\images\ic_image_upload.png')}}" alt="">
                            </label>
                        </div> 
                    </div>
                </div>               
            </div>
            <div class="desc-wrapper">
                <div class="desc-inner" style="position:relative;padding-bottom: 8px;">
                    <h6 class="price-title">بارگذاری تصویر</h6>
                    <div class="picture-upload-div">
                        <span class="txt-upload">در صورت نیاز از محل کار انجام شده ، تصویر بارگذاری نمایید</span>
                        <div class="tab-form">
                            <div class="item-wrapp">                             
                                <div class="order-img">
                                    <label for="sellImg2">
                                        <input accept="image/*" onchange="showMyImage1(this)" type="file" name="firstSellImg" id="sellImg2">
                                        <img id="thumbnil1" src="{{asset('webapp-assets/images/image_bg.png')}}" alt="">
                                    </label>
                                    <label for="sellImg3">
                                        <input accept="image/*" onchange="showMyImage2(this)" type="file" name="secondSellImg" id="sellImg3">
                                        <img id="thumbnil2" src="{{asset('webapp-assets/images/image_bg.png')}}" alt="">
                                    </label>
                                    <label for="sellImg4">
                                        <input accept="image/*" onchange="showMyImage3(this)" type="file" name="thirdSellImg" id="sellImg4">
                                        <img id="thumbnil3" src="{{asset('webapp-assets/images/image_bg.png')}}" alt="">
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>               
            </div>
            @endif

            @if($results['status'] == 'تحویل شده' || $results['status'] == 'ارسال شده')
            <div class="desc-wrapper">
                <div class="desc-inner" style="position:relative;padding-bottom:8px;">
                    <h6 class="price-title">فاکتور</h6>
                    <div class="factor-div">
                        <div class="factor-img">
                            @foreach($results['images'] as $images)
                                @if($images['type'] == 'ppf')
                                    <img src="{{asset('https://panel.4sooapp.com/uploads/'.$images['link'])}}" id="myImg" alt="">                    
                                @endif 
                            @endforeach
                        </div>      
                    </div>
                </div>               
            </div>

            <div class="desc-wrapper">
                <div class="desc-inner" style="position:relative;padding-bottom: 8px;">
                    <h6 class="price-title">تصاویر</h6>
                    <h6 class="center-txt-upload">تصاویر ثبت شده از انجام خدمت</h6>
                    <div class="picture-upload-div">
                        <div class="tab-form">
                        <div class="item-wrapp">
                            <div class="order-img-show">
                            @foreach($results['images'] as $key=>$item)
                                @if($item['type'] == 'pp1' || $item['type'] == 'pp2' || $item['type'] == 'pp3')                                                     
                                    <img  id="service{{$item['id']}}" src="{{asset('https://panel.4sooapp.com/uploads/'.$item['link'])}}" alt=""> 
                                @endif    
                            @endforeach
                            </div>
                        </div>
                    </div>
                    </div>
                </div>               
            </div> 
            @endif 

            <div class="desc-wrapper">
                <div class="desc-inner" style="position:relative;padding-bottom: 8px;">
                    <h6 class="price-title">تصاویر</h6>
                    <h6 class="center-txt-upload">تصاویر ثبت شده مشتری</h6>
                    <div class="picture-upload-div">
                        <div class="tab-form">
                        <div class="item-wrapp">                           
                            <div class="order-img-show">
                            @foreach($results['images'] as $key=>$item)
                                @if($item['type'] == 'cp1' || $item['type'] == 'cp2' || $item['type'] == 'cp3')                                                     
                                    <img  id="salesImg{{$item['id']}}" src="{{asset('https://panel.4sooapp.com/uploads/'.$item['link'])}}" alt=""> 
                                @endif    
                            @endforeach
                            </div>
                        </div>
                    </div>
                    </div>
                </div>               
            </div>
                <br><br>
                @if($results['status'] !== 'لغو شده' && $results['status'] !== 'تحویل شده')
                <div class="start-btn">
                    <input type="submit" 
                    @if($results['status'] == 'معلق')
                    value="تایید سفارش" name="confirm"
                    @elseif($results['status'] == 'در حال آماده سازی')
                    value="ارسال" name="send"
                    @elseif($results['status'] == 'تایید فروشنده')
                    value="آماده سازی" name="preparing"
                    @elseif($results['status'] == 'ارسال شده')
                    value="تحویل و تسویه" name="sended"
                    @endif
                    class="start-button">
                </div>
                </form>
                @endif

                <div id="myModal" class="modal">
                    <span class="close">&times;</span>
                    <img class="modal-content" id="img">
                </div>
              
        </div>  
            @endsection   
        
    @section('js')
  

    <script>
function showFactorImage(fileInput) {
    var files = fileInput.files;
    for (var i = 0; i < files.length; i++) {
    var file = files[i];
    var imageType = /image.*/;
    if (!file.type.match(imageType)) {
    continue;
    }
    var img=document.getElementById("thumbFactor");
    img.file = file;
    var reader = new FileReader();
    reader.onload = (function(aImg) {
    return function(e) {
    aImg.src = e.target.result;
    };
    })(img);
    reader.readAsDataURL(file);
    }
}

function showMyImage1(fileInput) {
    var files = fileInput.files;
    for (var i = 0; i < files.length; i++) {
    var file = files[i];
    var imageType = /image.*/;
    if (!file.type.match(imageType)) {
    continue;
    }
    var img=document.getElementById("thumbnil1");
    img.file = file;
    var reader = new FileReader();
    reader.onload = (function(aImg) {
    return function(e) {
    aImg.src = e.target.result;
    };
    })(img);
    reader.readAsDataURL(file);
    }
}

function showMyImage2(fileInput) {
    var files = fileInput.files;
    for (var i = 0; i < files.length; i++) {
    var file = files[i];
    var imageType = /image.*/;
    if (!file.type.match(imageType)) {
    continue;
    }
    var img=document.getElementById("thumbnil2");
    img.file = file;
    var reader = new FileReader();
    reader.onload = (function(aImg) {
    return function(e) {
    aImg.src = e.target.result;
    };
    })(img);
    reader.readAsDataURL(file);
    }
}


function showMyImage3(fileInput) {
    var files = fileInput.files;
    for (var i = 0; i < files.length; i++) {
    var file = files[i];
    var imageType = /image.*/;
    if (!file.type.match(imageType)) {
    continue;
    }
    var img=document.getElementById("thumbnil3");
    img.file = file;
    var reader = new FileReader();
    reader.onload = (function(aImg) {
    return function(e) {
    aImg.src = e.target.result;
    };
    })(img);
    reader.readAsDataURL(file);
    }
}
var modal = document.getElementById("myModal");
var modalImg = document.getElementById("img");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg");
if(img){
img.onclick = function(){
modal.style.display = "block";
modalImg.src = this.src;
} 
}
@foreach($results['products'] as $key=>$item)
    var firstProductsImage = document.getElementById("firstProductsImage{{$item['id']}}");
        if(firstProductsImage){
        firstProductsImage.onclick = function(){
        modal.style.display = "block";
        modalImg.src = this.src;
    }
        }
@endforeach

@foreach ($results['images'] as $key=> $item)
var customer = document.getElementById("service{{$item['id']}}");
if(customer){
customer.onclick = function(){
modal.style.display = "block";
modalImg.src = this.src;
} 
}
@endforeach 

@foreach($results['images'] as $key=>$item)
    var salesImg = document.getElementById("salesImg{{$item['id']}}");
        if(salesImg){
        salesImg.onclick = function(){
        modal.style.display = "block";
        modalImg.src = this.src;
    }
        }
@endforeach

var span = document.getElementsByClassName("close")[0];
span.onclick = function() {
    modal.style.display = "none";
} 
    </script>

    @endsection

