    @extends('layouts.template')
    @section('content')
	
	 <div id="tab-home" class="tab tab-active tab-home" style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}') ; background-size:cover;">
        <!-- home -->
        <div class="img-top">
        	<div class="row top ">
        		<div class="col-md-4 col-sm-4 col-xs-4 right-img">
                    <a href="{{route('dashboard')}}"><img  class="arrow-img"src="{{asset('webapp-assets\images\ic_arrow_back.png')}}" alt=""></a>
                </div>
        		<div class="col-md-4 col-sm-4 col-xs-4 middle-txt">
        			<span>فروش ها</span>
        		</div>
        		<div class="col-md-4 col-sm-4 col-xs-4 left-img">
        			<a href="{{route('sells_archive')}}"><img src="{{asset('webapp-assets/images/ic_archive.png')}}" alt="" class="lft"></a>
        		</div>       		
        	</div>
        		<img src="{{asset('webapp-assets/images/top-bg.png')}}" alt="">
        	</div>
            @if(!empty($results['data']))
                @foreach($results['data'] as $sales)
        	 <div class="orders-wrapper">
                <div class="orders-inner">
                <a href="{{route('sales_details',$sales['orderuniquecode'])}}">
                    <div class="orders-item 
                    @if($sales['status']=='تایید شده')success
                    @elseif($sales['status']=='معلق') danger
                    @elseif($sales['status']=='آماده سازی') orange
                    @else($sales['status']=='ارسال شده') primary
                    @endif
                    ">
                        <div class="head">{{$sales['customer_name']}}</div>
                        <div class="body">
                            <span class="time">{{$sales['delivertime']}}<img src="{{asset('webapp-assets/images/ic_time.png')}}" alt="" class="ic-time"></span>
                            <span class="date">{{\Morilog\Jalali\Jalalian::forge($sales['deliverdate'])->format('%d /%m /%Y')}}<img src="{{asset('webapp-assets/images/ic_date.png')}}" alt="" class="ic-date"></span>
                        </div>
                        <div class="foot">
                            <div class="customer-propertes">
                                <span> <b>مشخصات مشتری</b> :{{$sales['customer_mobile']}} </span>
                            </div>
                            <div class="customer-propertes">
                                <span><b>وضعیت</b>: {{$sales['status']}}</span>
                            </div>
                            <div class="customer-propertes">
                                <span><b>آدرس</b>: {{$sales['address']}}</span>
                            </div>
                        </div>
                    </div>
                </a>
                </div>
            </div>
            @endforeach
            @endif
            
                <br><br><br>
            @include('footer.footer')

     </div>
     @endsection
