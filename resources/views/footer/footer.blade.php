<div class="footer-wrapper">
    <div class="footer-div">
        <div @if (Illuminate\Support\Facades\Route::currentRouteName()=='dashboard' ) class="active-icon" @endif ><a href="{{route('dashboard')}}"><i class="fas fa-home"></i><div>خانه</div></a></div>
        <div @if (Illuminate\Support\Facades\Route::currentRouteName()=='store' ) class="active-icon" @endif ><a href="{{route('store')}}"><i class="fas fa-shopping-bag"></i><div>کالاها</div></a></div>
        <div @if (Illuminate\Support\Facades\Route::currentRouteName()=='sales' ) class="active-icon" @endif><a href="{{route('sales')}}"><i class="fa fa-edit"></i><div>فروش‌ها</div></a></div>
        <div @if (Illuminate\Support\Facades\Route::currentRouteName()=='offers' ) class="active-icon"@endif><a href="{{route('offers')}}"><i class="fa fa-envelope-open" aria-hidden="true"></i><div>پیشنهادها</div></a></div>
        <div @if (Illuminate\Support\Facades\Route::currentRouteName()=='services' ) class="active-icon" @endif ><a href="{{route('services')}}"><i class="fas fa-list"></i><div>خدمت‌ها</div></a></div>     
    </div>
</div>