@extends('layouts.template')
	@section('content')	
        
		<div id="tab-hom" class="tab tab-active tab-home" style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}') ; background-size:cover;">
        <!-- home -->
        	<div class="img-top">
	        	<div class="row top ">
	        		<div class="col-md-4 col-sm-4 col-xs-4 right-img">
	        			<div class="serach-icon-awesome">
	        				<a href="{{route('serviceSearch')}}"><h6><i class="fa fa-search" aria-hidden="true"></i>
								</h6>
							</a>
						</div>	
	        		</div>
	        		<div class="col-md-4 col-sm-4 col-xs-4 middle-txt">
	        			<span>خدمت ها</span>
	        		</div>
	        		<div class="col-md-4 col-sm-4 col-xs-4 p-0 left-img">
	        			<a href="{{route('archive')}}"><img src="{{asset('webapp-assets/images/ic_archive.png')}}" alt="" class="rgt"></a>
	        			<img src="{{asset('webapp-assets/images/ic_archive_date.png')}}" alt="" class="lft">
	        			
	        		</div>       		
	        	</div>
        		<img src="{{asset('webapp-assets/images/top-bg.png')}}" alt="">
        	</div>  
			
			@foreach($results['data'] as $service_data)  
			
	        	 <div class="orders-wrapper">
	                <div class="orders-inner">	                    
	                    <a href="{{route('services_details',$service_data['order_unique_code'])}}">
							<div class="orders-item 
							@if($service_data['order_type']=='شروع نشده')default
							@elseif($service_data['order_type']=='در حال انجام') warning
							@else($service_data['order_type']=='انجام شده') success
							@endif
							">
	                            <div class="head">{{$service_data['service_name']}}</div>
	                            <div class="body">
	                                <span class="time">{{$service_data['final_time']}} <img src="{{asset('webapp-assets/images/ic_time.png')}}" alt="" class="ic-time"></span>
	                                <span class="date">
									{{\Morilog\Jalali\Jalalian::forge($service_data['final_date'])->format('%d/ %m/ %Y')}}
									<img src="{{asset('webapp-assets/images/ic_date.png')}}" alt="" class="ic-date"></span>
	                            </div>
	                            <div class="foot">
	                            	<div class="customer-propertes">
	                            		<span>
										مشخصات مشتری :
										{{$service_data['order_firstname_customer']}}
										{{$service_data['order_lastname_customer']}}						
										</span>
	                            	</div>
	                            	<div class="customer-propertes">
									وضعیت :
	                            		<span>{{$service_data['order_type']}}</span>
	                            	</div>
	                            	<div class="customer-propertes">
									آدرس :
	                            		<span>{{$service_data['order_address']}}</span>
	                            	</div>
	                            </div>
	                        </div>
	                    </a>

	                </div>
	            </div>
				@endforeach
	           
	            <br><br><br>
	            @include('footer.footer')
			</div>    
		<div class="modal-container">
				<div class="modal-width">
					<div class="modal-content">
						<div class="title"><h6><a href="">همه</a></h6></div>
						<ul>
							<li><a href=""> {{$dataTime[0]}}</a></li>
							<li><a href=""> {{$dataTime[1]}}</a></li>
							<li><a href=""> {{$dataTime[2]}}</a></li>
							<li><a href=""> {{$dataTime[3]}}</a></li>
							<li><a href=""> {{$dataTime[4]}}</a></li>
							<li><a href=""> {{$dataTime[5]}}</a></li>
							<li><a href=""> {{$dataTime[6]}}</a></li>
						</ul>	
					</div>
				</div>	
		</div>
		@endsection

	@section('js')

    <script>
		$(document).ready(function(){
  			$(".lft").click(function(){
    		$(".modal-container").show(100);
    		return false;
  			});

  			$(".modal-container").click(function(){
  				$(".modal-container").hide(100);
  			});
  			
			});

	</script>

	@endsection