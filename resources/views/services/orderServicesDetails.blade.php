@extends('layouts.template')
@section('content')
    <form action="{{ route('SendOrderServicesDetails')}}" method="POST">
    @csrf
    <input type="hidden" name="order" value="{{$results['id']}}">
<div id="tab-hom" class="tab tab-active tab-home" style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}') ; background-size:cover;">
<!-- home -->
<div class="img-top" style="margin-bottom: 10px;">
    <div class="row top ">
        <div class="col-md-3 col-sm-3 col-xs-3 right-img">
                <a href="{{route('offers')}}"><img  class="arrow-img"src="{{asset('webapp-assets\images\ic_arrow_back.png')}}" alt=""></a>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6 top-title">
            <span>جزییات سفارش </span>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3 left-img">                        
        </div>              
    </div>
    <img src="{{asset('webapp-assets/images/top-bg.png')}}" alt="">
</div>
<div class="desc-wrapper">
    <div class="desc-inner extra" style="position:relative;">
        <h6 style="text-align:center;">{{ $results['service_name'] }} </h6>
        <hr style="margin-bottom: 0">
        <div class="orderdetails">
            <span class="right"><i style="color:brown;" class="fa fa-id-card"></i> کد سفارش :</span>
            <span class="left">{{$results['order_unique_code']}}</span>
        </div>
        <div class="orderdetails">
            <span class="right"><i style="color:darkred;" class="fa fa-calendar"></i> روز :</span>
            <span class="left">{{\Morilog\Jalali\Jalalian::forge($results['order_date_first'])->format('%A %d %B %Y')}}</span>
        </div>
        <div class="orderdetails">
            <span class="right"><i style="color:deeppink;" class="fa fa-clock"></i> ساعت :</span>
            <span class="left">{{$results['order_time_first']}}</span>
        </div>
        <div class="orderdetails">
            <span class="right"><i style="color:gold;" class="fa fa-map-marker"></i> نشانی :</span>
            <span class="left">{{$results['order_address']}}</span>
        </div>
        @if($results['canoffer'] == 1)
        <div class="orderdetails" style="padding-right: 0; padding-left: 0;">
            <div class="offer-message">
                <img src="{{asset('webapp-assets\images\ic_info.png')}}" alt="">
                <span>آماده دریافت پیشنهاد</span>
            </div>
        </div>
        @else($results['canoffer'] == 2)
        <div class="orderdetails" style="padding-right: 0; padding-left: 0;">
            <div class="offer-message" style="background:#f2851f">
                <img src="{{asset('webapp-assets\images\ic_info.png')}}" alt="">
                <span>شما قبلا پیشنهاد خود را ارسال کرده اید</span>
            </div>
        </div>
        @endif
        <h6 style="text-align:center;">{{ $results['service_name'] }} </h6>
        <div class="orderdetails" style="padding-bottom: 0;">
            <span style="font-size: 13px;">توضیح :</span><br>
            <span style="font-size: 11px;"> {{$results['order_desc']}}</span>
        </div>
    </div>
</div>
@if($results['canoffer'] == 2)
<div class="desc-wrapper">
    <div class="desc-inner extra" style="position:relative;">
        <h6 style="text-align:center;">جزییات پیشنهاد ارسالی </h6>
        <hr style="margin-bottom: 0">
        <div class="orderdetails">
            <span class="right"> قیمت  :</span>
            <span class="left">{{$result['amount']}} </span>
        </div>
        <div class="orderdetails">
            <span class="right"> تاریخ :</span>
            <span class="left"> {{$result['date']}}</span>
        </div>
        <div class="orderdetails">
            <span class="right"> زمان :</span>
            <span class="left">{{$result['time']}}</span>
        </div>
        <div class="orderdetails">
            <span class="right"> تاریخ ثبت :</span>
            <span class="left">{{\Morilog\Jalali\Jalalian::forge($result['created_at'])->format('%A %d %B %Y')}}</span>
        </div>
        <div class="orderdetails">
            <span class="right"> وضعیت :</span>
            <span class="left">{{$result['status']}}</span>
        </div>
        <br>
    </div>
</div>

<div class="desc-wrapper">
    <div class="desc-inner extra" style="position:relative;">
        <h6 style="text-align:center;">{{ $results['service_name'] }} </h6>
        <hr>
        <div class="orderdetails" style="padding:0">
            <span class="right">  تاریخ انتخاب مشتری </span>
            <span class="left">{{\Morilog\Jalali\Jalalian::forge($results['order_date_first'])->format('%d/ %m/ %Y')}}</span>
            <br>
            <span class="right"> توضیح</span>
            <br>
            <p style="color:#444;font-size:12px;padding:0 3px 0 0">{{$results['order_desc']}}</p>
            <br><br>
        </div>
    </div>
</div>
@else($results['canoffer'] == 1)
<div class="desc-wrapper">
    <div class="desc-inner extra" style="position:relative;">
        <h6 style="text-align:center;">انتخاب تاریخ انجام </h6>
        <hr>
        <input checked type="radio" value="first_time" name="simple" id="simple" class="simple-input">
        <label for="simple" id="simplelabel">
            <div class="orderdetails extra" id="topDivTime">
                <div class="row input p-0">
                    <div class="col-md-2 col-sm-2 col-xs-2 date p-0 m-0">
                        <i class='far fa-calendar-alt'></i>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 date p-0 m-0">
                        <span class="simpletime">{{\Morilog\Jalali\Jalalian::forge($results['order_date_first'])->format('%d/ %m/ %Y')}}</span>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 time p-0 m-0">
                        <span class="simpletime"> ساعت :{{$results['order_time_first']}}</span>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2 time p-0 m-0">
                        <i class="fas fa-clock"></i> 
                    </div>
                </div> 
            </div>
        </label>
        <br>
        @if($results['order_time_second'] && $results['order_date_second'])
        <input type="radio" value="second_time" name="simple" id="simple2" class="simple-input">
        <label for="simple2" id="simplelabel2">
            <div class="orderdetails extra" id="topDivTime">
                <div class="row input p-0">
                    <div class="col-md-2 col-sm-2 col-xs-2 date p-0 m-0">
                        <i class='far fa-calendar-alt'></i>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 date p-0 m-0">
                        <span class="simpletime">{{\Morilog\Jalali\Jalalian::forge($results['order_date_second'])->format('%d/ %m/ %Y')}}</span>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 time p-0 m-0">
                        <span class="simpletime"> ساعت :{{$results['order_time_second']}}</span>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2 time p-0 m-0">
                        <i class="fas fa-clock"></i> 
                    </div>
                </div> 
            </div>
        </label>
        <br>
        @endif
        <input type="radio" value="custom" name="simple" id="custom" class="simple-input">
        <label for="custom" id="customlabel">
        <div class="orderdetails extra">
            <div class="row p-0">
                <div class="col-md-2 col-sm-2 col-xs-2 date p-0 m-0">
                    <i class='far fa-calendar-alt'></i>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-8 center p-0 m-0">
                    <span class="customtime">انتخاب زمان سفارشی</span>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-2 time p-0 m-0">
                    <i class="fas fa-clock"></i> 
                </div>
            </div> <br>
                <div style="display:none;" class="day-select">
                    <span>
                    <select name="select_day_time" id="select_day_time">
                        <option value="0">{{$dataTime[0]}}</option>
                        <option value="1">{{$dataTime[1]}}</option>
                        <option value="2">{{$dataTime[2]}}</option>
                        <option value="3">{{$dataTime[3]}}</option>
                        <option value="4">{{$dataTime[4]}}</option>
                        <option value="5">{{$dataTime[5]}}</option>
                        <option value="6">{{$dataTime[6]}}</option>
                    </select></span>
                    <br><br>
                    <div class="row p-0">
                        <div class="col-md-3 col-sm-3 col-xs-3 select p-0 m-0">
                            <input checked type="radio" id="firstClock" name="selectClock" value="8 تا 12">
                            <label for="firstClock"></label>
                            <span>8 تا 12</span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3 select p-0 m-0">
                        <input type="radio" id="secondClock" name="selectClock" value="12 تا 16">
                        <label for="secondClock"></label>
                            <span>12 تا 16</span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3 select p-0 m-0">
                        <input type="radio" id="thirdClock" name="selectClock" value="16 تا 20">
                        <label for="secondClock"></label>
                            <span>16 تا 20</span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3 select p-0 m-0">
                        <input type="radio" id="fourthClock" name="selectClock" value="20 تا 24">
                        <label for="fourthClock"></label>
                            <span>20 تا 24</span>
                        </div>
                    </div>
                </div> 
            <br>
        </div> 
        </label>

    </div>
</div>

@if($results['order_images'])
<div class="desc-wrapper">
    <div class="desc-inner extra-img" style="position:relative;box-shadow:none;background:none;">
        <div class="row p-0">
                @foreach($results['order_images'] as $key=>$item)
                    @if($item['type'] == 'cp1' || $item['type'] == 'cp2' || $item['type'] == 'cp3')
            <div class="col-md-4 col-sm-4 col-xs-4 select p-0 m-0">
                <div class="container-img-div">
                    <img id="offers{{$item['id']}}" src="{{asset('https://panel.4sooapp.com/uploads/'.$item['link'])}}" alt="">
                </div>
            </div>
                    @endif
                @endforeach    
        </div>
    </div>
</div>
@endif
<div class="desc-wrapper">
    <div class="desc-inner extra" style="position:relative;">
        <h6 style="text-align:center;">{{ $results['service_name'] }} </h6>
        <hr style="margin-bottom: 0">
        <div class="orderdetails">
            <span class="right"> قیمت پایه</span>
            <span class="left">تومان</span>
            <br>
            <div class="div-button">
            <input type="text" name="amount" placeholder="مبلغ پیشنهاد خود را وارد کنید">
            </div>
            <div class="div-button-left">
                <p>تومان</p>
            </div>
        </div>

        <div class="form-row">
            <div class="col-12">
                <label for="description">توضیحات</label>
                <textarea style="border:3px solid #0e4f46" class="form-control" name="description" id="description" placeholder="توضیحات خود را وارد نمایید " rows="6"></textarea>
            </div>
        </div>
        <br>
    </div>
</div>

<div class="desc-wrapper">
    <div class="desc-inner extra-alarm" style="position:relative;">
        <div class="right-alarm-div">
        <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
        </div>
        <span class="topdiv">کارمزد ثبت قیمت پیشنهادی 1000 تومان است</span>
        <span class="bottomdiv">برای ارسال پیشنهاد باید به میزان کارمزد تعیین شده شارژ داشته باشید</span>
    </div>
</div>
    <br><br>
    <div class="start-btn">
        <input type="submit"  value="ارسال پیشنهاد" class="start-button">
    </div> 

    <div id="myModal_identify" class="modal-show">
        <!-- The Close Button -->
        <span class="close" id="close_btn">&times;</span>                       
        <!-- Modal Content (The Image) -->
        <img class="modal-content-show" id="imgIdentify">
    </div>              
</div>
</form> 
@endif
@endsection   

@section('js')
    <script>
        $(document).ready(function(){
            $("#simplelabel").click(function(){
                $(".day-select").hide(100);
                return true;
            })
            $("#simplelabel2").click(function(){
                $(".day-select").hide(100);
                return true;
            })
            $("#customlabel").click(function(){
                $(".day-select").show(100);
                return true;
            })
        });
        var modal = document.getElementById("myModal_identify");
        var modalImg = document.getElementById("imgIdentify");

        @foreach($results['order_images'] as $key=>$item)
        var offers = document.getElementById("offers{{$item['id']}}");
        if(offers){
            offers.onclick = function(){
            modal.style.display = "block";
            modalImg.src = this.src;
        }
        }
        @endforeach
        var span = document.getElementsByClassName("close")[0];
        span.onclick = function() {
            modal.style.display = "none";
        } 
    </script>
@endsection