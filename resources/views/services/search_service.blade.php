@extends('layouts.template')
    @section('content')
	 <div id="tab-home" class="tab tab-active tab-home" style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}') ; background-size:cover;">
        <!-- home -->
        <div class="img-top">
        	<div class="row top ">
                <div class="col-md-2 col-sm-2 col-xs-2 right-img">
                    <a href="{{route('services')}}"><img  class="arrow-img"src="{{asset('webapp-assets\images\ic_arrow_back.png')}}" alt=""></a>	
                </div>
        		<div class="col-md-8 col-sm-8 col-xs-8 search-archive">
                    <input type="text" id="search-text" placeholder="جستجو در میان سفارش ها...">
                    <div class="search-img-div"><i class="fas fa-search"></i></div>
                </div> 
                <div class="col-md-2 col-sm-2 col-xs-2 ">
                </div>       		
        	</div>
        		<img src="{{asset('webapp-assets/images/top-bg.png')}}" alt="">
        </div>
        <div id="results">
        </div>

                <br><br><br>
            @include('footer.footer')

     </div>
     
     @endsection

     @section('js')
        <script>
        $(document).ready(function(){
         $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#search-text').keyup(function (event) {
        key = $(this).val();
        if(key === ""){
            $('#results').html('');
        }else{
            $('#results').show();
            $.ajax({
            type:'post',
            url:'{{route("search_service")}}',
            cache: false,
            async: true,
            data:{_token: "{{ csrf_token() }}",key:key},
            success:function(data){
            $('#results').html(data)           
                }           
            });
        }
    });
})
        </script>
     @endsection