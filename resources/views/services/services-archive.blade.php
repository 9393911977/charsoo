	@extends('layouts.template')
	@section('content')	
		
		<div id="tab-hom" class="tab tab-active tab-home" style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}') ; background-size:cover;">
        <!-- home -->
        	<div class="img-top">
	        	<div class="row top ">
	        		<div class="col-md-4 col-sm-4 col-xs-4 right-img">
	        			<div class="serach-icon-awesome">
	        				<i class="fa fa-search" aria-hidden="true"></i>
						</div>	
	        		</div>
	        		<div class="col-md-4 col-sm-4 col-xs-4 middle-txt">
	        			<span> آرشیو</span>
	        		</div>
	        		<div class="col-md-4 col-sm-4 col-xs-4 left-img">
	        			
	        		</div>       		
	        	</div>
        		<img src="{{asset('webapp-assets/images/top-bg.png')}}" alt="">
        	</div>
        	

	        	 <div class="orders-wrapper">
	                <div class="orders-inner">
	                    
	                    <a href="">
	                        <div class="orders-item success">
	                            <div class="head">خدمات هواساز و تغییر فصل</div>
	                            <div class="body">
	                                <!-- <span class="time">8 تا12 <img src="webapp-assets/images/ic_time.png" alt="" class="ic-time"></span>
	                                <span class="date">1399-5-8 <img src="webapp-assets/images/ic_date.png" alt="" class="ic-date"></span> -->
	                                <div class="row">
	                                	<div class="col-md-4 col-sm-4 col-xs-4 right-archive">
	                                		<p>200000 <i class="fas fa-coins"></i></p>                               		
	                                	</div>
	                                	<div class="col-md-4 col-sm-4 col-xs-4 middle-archive">
	                                		<p>18 تا 20<i class="fas fa-clock"></i></p>
	                                	</div>
	                                	<div class="col-md-4 col-sm-4 col-xs-4 left-archive">
	                                		<p>1398/6/3 <i class="fa fa-calendar" aria-hidden="true"></i>
</p>
	                                	</div>
	                                </div>
	                            </div>
	                            <div class="explain-container">
	                            	<p style="font-weight:bold; padding-left:3px;padding-bottom: 2px;">آدرس:<p style="padding-right:0;margin-right:0;padding-bottom: 2px;">احمدآباد، بابک 10</p></p>
	                            	<br style="padding-top: 4px; margin-top: 4px;">
	                            	<p style="font-weight:bold; padding-left:3px;padding-top:0;">توضیحات:<p style="padding-right:0;margin-right:0;padding-top:0;">سرویس لوله ها</p></p>
	                            </div>
	                            <div class="green-div">
	                            	<p class="green-center">پایان کار</p>
	                            	<hr style="margin:0;">
	                            	<div class="row green">
	                                	<div class="col-md-4 col-sm-4 col-xs-4 right-archive">
	                                		<p>200000 <i class="fas fa-coins"></i></p>                               		
	                                	</div>
	                                	<div class="col-md-4 col-sm-4 col-xs-4 middle-archive">
	                                		<p>18 تا 20<i class="fas fa-clock"></i></p>
	                                	</div>
	                                	<div class="col-md-4 col-sm-4 col-xs-4 left-archive">
	                                		<p>1398/6/3 <i class="fa fa-calendar" aria-hidden="true"></i>
											</p>
	                                	</div>
	                                </div>	
	                                <hr style="margin:0;">
	                                <div class="row green-state">
	                                	<div class="col-md-4 col-sm-4 col-xs-4 right-archive">
	                                		<p>وضعیت مالی: </p>                               		
	                                	</div>
	                                	<div class="col-md-4 col-sm-4 col-xs-4 middle-archive">
	                                		<p> مشتری</p><i class="fas fa-check-square"></i>
	                                	</div>
	                                	<div class="col-md-4 col-sm-4 col-xs-4 left-archive">
	                                		<p>چهارسو</p><i class="fa fa-square" aria-hidden="true"></i>
	                                	</div>
	                                </div>
	                            </div>
	                            
	                            
	                        </div>
	                    </a>

	                </div>
	            </div>
	             @include('footer.footer') 
		   </div>  
		   
		   @endsection
