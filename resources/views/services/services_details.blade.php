@extends('layouts.template')
@section('content')	
   
<div id="closeModal" class="my-modal">
    <div class="modal-wrapper">
        <div class="modal-inner">
            <div class="modal-head">
                <span id="closeModal" class="dismis"><b><i class="fa fa-times"></i></b></span>
                اخطار !
            </div>
            <div class="modal-body">از لغو سفارش اطمینان دارید ؟</div>
            <div class="modal-foot"><a style="position: absolute; left: 12px;" class="mybtn" href="">لغو</a></div>
        </div>
    </div>
</div> 
<form action="{{route('send_services_details')}}" method="post" enctype="multipart/form-data">
    @csrf
    <div id="tab-hom" class="tab tab-active tab-home" style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}') ; background-size:cover;">
    <!-- home -->
        <div class="img-top" style="margin-bottom: 10px;">
            <div class="row top ">
                <div class="col-md-3 col-sm-3 col-xs-3 right-img">
                    @if($results['order_type'] == 'تسویه شده' || $results['order_type'] == 'لغو شده' )
                    <a href="{{route('archive')}}"><img class="arrow-img" src="{{asset('webapp-assets\images\ic_arrow_back.png')}}" alt=""></a>
                    @else
                        <a href="{{route('services')}}"><img class="arrow-img "src="{{asset('webapp-assets\images\ic_arrow_back.png')}}" alt=""></a>
                    @endif
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 top-title">
                    <span>جزییات سفارش </span>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-3 left-img">
                    @if($results['order_type'] == 'شروع نشده')
                    <a onclick="return confirm('از عودت سفارش اطمینان دارید ؟')" href="{{route('orderReject',$results['order_unique_code'])}}">
                      <div class="cancellbtn" >
                      <i style="color:#fff;" class="fa fa-reply"></i><br>
                      <p style="padding:0px;" class="cnl">عودت</p>
                      </div>
                    </a>  
                    @endif
                </div>              
            </div>
            <img src="{{asset('webapp-assets/images/top-bg.png')}}" alt="">
        </div>
        <input type="hidden" name="unique_code" value="{{$results['order_unique_code']}}">
        <div class="desc-wrapper">
            <div class="desc-inner" style="position:relative;padding: 18px 15px;">
                <h6 style="text-align:center;">{{$results['service_name']}}</h6>
                <hr style="margin-bottom: 0;">
                <div class="orderdetails">
                    <span class="right"><i style="color:brown;" class="fa fa-id-card"></i> کد سفارش :</span>
                    <span class="left">{{$results['order_unique_code']}}</span>
                </div>
                <div class="orderdetails">
                    <span class="right"><i style="color:darkblue;" class="fa fa-shopping-bag"></i> مشتری:</span>
                    <span class="left"> 
                    {{$results['order_firstname_customer']}}
                    {{$results['order_lastname_customer']}}
                    </span>
                </div>
                <div class="orderdetails">
                    <span class="right"><i style="color:darkgreen;" class="fa fa-phone"></i> تلفن :</span>
                    <span class="left">{{$results['order_show_mobile']}}</span>
                </div>
                <div class="orderdetails">
                    <span class="right"><i style="color:darkred;" class="fa fa-calendar"></i> روز :</span>
                    <span class="left">{{\Morilog\Jalali\Jalalian::forge($results['order_date_first'])->format('%A %d %B %Y')}}</span>
                </div>
                <div class="orderdetails">
                    <span class="right"><i style="color:deeppink;" class="fa fa-clock"></i> ساعت :</span>
                    <span class="left">{{$results['order_time_first']}}</span>
                </div>
                <div class="orderdetails">
                    <span class="right"><i style="color:gold;" class="fa fa-map-marker"></i> نشانی :{{$results['order_address']}}</span>
                    <span class="left"></span>
                </div>
            </div>
        </div>
        @if($results['order_type'] == 'شروع نشده'||
            $results['order_type'] == 'انجام شده' ||
            $results['order_type'] == 'تسویه شده' ||
            $results['order_type'] == 'لغو شده') 
        @endif

        @if($results['order_type'] == 'شروع نشده')
        <div class="desc-wrapper">
            <div class="desc-inner" style="position:relative;padding: 18px 15px;">
                <h6 style="text-align:center;">{{$results['service_name']}}</h6>
                <hr>
                {{--
                <div class="orderdetails">
                    <span class="right-bold">قیمت پایه</span>
                    <span class="left-bold">{{$results['cost']}}</span>
                </div>
                    --}}
                <div class="explain-services">
                    <span>بدلیل حجم بالای کار لطفا تاریخ امروز و فردا را برای انجام کار خود انتخاب نکنید.<br>
                    خدمات دستگاه های هواساز ذیتکو:<br>
                    تعمیر و سرویس دوره ای هواساز<br>
                    تغییر فصل زمستانه و تابستانه<br>
                    رفع عیوب برقی<br>
                    تعویض پد سلولز<br>
                    رفع نشتی آب</span>
                </div>
                
                <h6 class="title-msg"style="">پیام به مشتری</h6>
                <div class="comment">
                    <textarea name="comment" rows="4" placeholder="پیام خود را برای مشتری ارسال کنید."></textarea>
                    
                 </div>
                
            </div>
        </div>
        @endif
        
        @if($results['order_type'] == 'در حال انجام')
        <div class="desc-wrapper">
            <div class="desc-inner" style="position:relative;padding: 14px 15px;">
                <h6 style="text-align:center;"> وضعیت: {{$results['order_type']}}</h6>
                <hr style="margin-bottom: 11px;">
                <div class="orderdetails-state">
                    <h6 class="right-bold-msg">حتما در زمان پایان کار دکمه پایین صفحه را بزنید تا پرداخت هزینه صورت گیرد.</h6>
                </div>
                <div class="description-customer">
                <p>توضیح:</p>
                <p style="font-size:12px;margin-bottom:5px;">{{$results['order_desc']}} </p>
                </div>
                 <span class="right">زمان درخواست مشتری:</span>
                 <div class="orderdetails-state">
                     <div class="orderdetails">
                        <span class="right">ساعت : {{$results['order_time_first']}}</span>
                        <span class="left">تاریخ : {{\Morilog\Jalali\Jalalian::forge($results['order_date_first'])->format('%d/ %m/ %Y')}}</span>
                     </div>
                 </div>
                 <br>
                <span class="right" style="margin-top:5px">زمان شروع:</span>
                <div class="orderdetails">
                        <span class="right">ساعت : {{\Morilog\Jalali\Jalalian::forge($results['order_detail']['order_start_time'])->format('H:i:s')}}</span>
                        <span class="left">تاریخ :{{\Morilog\Jalali\Jalalian::forge($results['order_detail']['order_start_time'])->format('%d/ %m/ %Y')}}</span>
                        
                </div>
            </div>
        </div>
        <div class="desc-wrapper">
            <div class="desc-inner" style="position:relative;padding:8px 12px 8px 12px">
                <h6 class="price-title"> هزینه سرویس</h6>
                <h6 class="value-price">مقدار دستمزد را وارد کنید</h6>
                <div class="div-price-service">
                    <input type="number" id="input-service-prive" name="price_service" >
                    <span class="left-tooman">تومان</span>
                </div>
            </div>
        </div>
        <div class="desc-wrapper">
            <div class="desc-inner" style="position:relative;padding:8px 12px 8px 12px">
                <h6 class="price-title">هزینه لوازم مصرفی </h6>
                <h6 class="value-price">هزینه لوازم مصرف شده برای انجام خدمت را وارد کنید</h6>
                <div class="div-price-service">
                    <input type="number" id="input-service-prive" name="price_tool" >
                    <span class="left-tooman">تومان</span>
                </div>
            </div>
        </div>
        <div class="desc-wrapper">
            <div class="desc-inner" style="position:relative;padding-bottom: 8px;">
                <h6 class="price-title">بارگذاری فاکتور</h6>
                <span class="right-show">تصویر فاکتور را بارگذاری کنید</span>
                <div class="factor-div">

                        <div class="factor-img">                               
                            <label for="upload-factor-img">
                            <input accept="image/*" onchange="showFactorImage(this)" type="file" name="factororderImg" id="upload-factor-img">
                            <img id="thumbFactor" src="{{asset('webapp-assets\images\ic_image_upload.png')}}" alt="">
                            </label>
                        </div> 

                </div>
            </div>               
        </div>
        <div class="desc-wrapper">
            <div class="desc-inner" style="position:relative;padding-bottom: 8px;">
                <h6 class="price-title">بارگذاری تصویر</h6>
                <div class="picture-upload-div">
                    <span class="txt-upload">در صورت نیاز از محل کار انجام شده ، تصویر بارگذاری نمایید</span>

                    <div class="tab-form">
                    <div class="item-wrapp">
                        
                        <div class="order-img">
                            <label for="order-img1">
                                <input accept="image/*" onchange="showMyImage1(this)" type="file" name="firstorderImg" id="order-img1">
                                <img id="thumbnil1" src="{{asset('webapp-assets/images/image_bg.png')}}" alt="">
                            </label>
                            <label for="order-img2">
                                <input accept="image/*" onchange="showMyImage2(this)" type="file" name="secondorderImg" id="order-img2">
                                <img id="thumbnil2" src="{{asset('webapp-assets/images/image_bg.png')}}" alt="">
                            </label>
                            <label for="order-img3">
                                <input accept="image/*" onchange="showMyImage3(this)" type="file" name="thirdorderImg" id="order-img3">
                                <img id="thumbnil3" src="{{asset('webapp-assets/images/image_bg.png')}}" alt="">
                            </label>
                        </div>
                    </div>
                </div>

                </div>
            </div>               
        </div>
        @endif

        @if($results['order_type'] == 'انجام شده')
         <div class="desc-wrapper">
            <div class="desc-inner" style="position:relative;padding: 18px 15px;">
                <h6 style="text-align:center;"> وضعیت: {{$results['order_type']}}</h6>
                <hr style="margin-bottom:0">
                 <span class="right-title">زمان درخواست مشتری:</span>
                 <div class="orderdetails-state">
                     <div class="orderdetails">
                       <span class="right">ساعت : {{$results['order_time_first']}}</span>
                       <span class="left">تاریخ : {{\Morilog\Jalali\Jalalian::forge($results['order_date_first'])->format('%d/ %m/ %Y')}}</span>
                     </div>
                 </div>
                 @if(!empty($results['order_detail']['order_end_time']))
                 <br>
                <span class="right-title">زمان پایان:</span>
                 <div class="orderdetails-state">
                     <div class="orderdetails">
                     <span class="right">ساعت : {{\Morilog\Jalali\Jalalian::forge($results['order_detail']['order_end_time'])->format('%H :i :s')}}</span>
                        <span class="left">تاریخ : {{\Morilog\Jalali\Jalalian::forge($results['order_detail']['order_end_time'])->format('%d/ %m/ %Y')}}</span>
                     </div>
                 </div>
                 @endif
                 <br>
                 <!-- <span class="right-title">زمان دریافت هزینه:</span>
                 <div class="orderdetails-state">
                     <div class="orderdetails">
                        <span class="right">ساعت 8 تا 12:</span>
                        <span class="left">تاریخ : 1399/07/12</span>
                     </div>
                 </div>     -->
            </div>
        </div>
        <div class="desc-wrapper">
            <div class="desc-inner" style="position:relative;padding-bottom:8px;">
                <h6 class="price-title">فاکتور</h6>
                <div class="factor-div">
                    <div class="factor-img">
                    @foreach($results['order_images'] as $images)
                        @if($images['type'] == 'ppf')
                            <img src="{{asset('https://panel.4sooapp.com/uploads/'.$images['link'])}}" id="myImg" alt="">                    
                        @endif 
                    @endforeach
                    </div>      
                </div>
            </div>               
        </div>
        <div class="desc-wrapper">
            <div class="desc-inner" style="position:relative;padding-bottom: 8px;padding-top: 5px;">
                     <div class="orderdetails">
                        <h6 class="title-msg">به پایان رسید!</h6>
                        <h6 class="value-price" style="margin-top: 15px;">زمان: {{\Morilog\Jalali\Jalalian::now()->format('%A %d %B - H:i')}}</h6>
                        <h6 class="value-price" style="margin-top: 8px;">کد سفارش: {{$results['order_unique_code']}}</h6>
                        <h6 class="value-price" style="margin-top: 15px; font-size: 10px;display: block;">بعد از دریافت مبلغ فاکتور گزینه دریافت هزینه را بزنید</h6>
                        <!-- <h6 class="end-txt-right">هزینه سرویس</h6>
                        <h6 class="end-txt-center">0 </h6>
                        <h6 class="end-txt-left">تومان </h6> -->
                        <div class="row mr-0 ml-0 ">
                            <div class="col-md-4 col-sm-4 col-xs-4 p-0 end-txt-right"><h6 class="end-txt-right">هزینه سرویس</h6>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 p-0 end-txt-center"><h6 class="end-txt-right">@if($results['cost'] == null) 0
                            @else{{number_format($results['cost'])}}
                            @endif
                            </h6>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 p-0 end-txt-left"><h6 class="end-txt-right">تومان</h6>
                            </div>
                        </div>
                        <div class="row mr-0 ml-0">
                            <div class="col-md-4 col-sm-4 col-xs-4 p-0 end-txt-right"><h6 class="end-txt-right">اعتبار مشتری</h6>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 p-0 end-txt-center"><h6 class="end-txt-right">@if($results['creditamount'] == null) 0
                            @else
                            {{number_format($results['creditamount'])}}</h6>
                            @endif
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 p-0 end-txt-left"><h6 class="end-txt-right">تومان</h6>
                            </div>
                        </div>
                         <h6 class="value-price" style="margin-top: 15px; font-size: 10px;display: block;">این مبلغ از طرف چهارسو  به حساب شما واریز می شود</h6>
                        
                     </div>
            </div>
        </div>

        <div class="desc-wrapper">
            <div class="desc-inner" style="position:relative;padding-bottom: 8px;">
                <h6 class="price-title">تصاویر</h6>
                <h6 class="center-txt-upload">تصاویر ثبت شده از انجام خدمت</h6>
                <div class="picture-upload-div">
                    <div class="tab-form">
                        <div class="item-wrapp">                          
                            <div class="order-img-show">
                            @foreach($results['order_images'] as $key=>$item)
                                @if($item['type'] == 'pp1' || $item['type'] == 'pp2' || $item['type'] == 'pp3')                                                     
                                <img  id="service{{$item['id']}}" src="{{asset('https://panel.4sooapp.com/uploads/'.$item['link'])}}" alt=""> 
                                 @endif    
                            @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>               
        </div>
        @endif
        
        @if($results['order_type'] == 'تسویه شده' || $results['order_type'] == 'لغو شده' )
        <div class="desc-wrapper">
                <div class="desc-inner" style="position:relative;padding: 18px 10px;">
                    <h6 style="text-align:center;">جزییات درخواست مشتری</h6>
                    <hr style="margin-bottom: 0">
                    <div class="explain-services">
                    <span class="right" style="font-size:13px;font-weight:bold">خدمات درخواستی : </span><br>
                        <span>بدلیل حجم بالای کار لطفا تاریخ امروز و فردا را برای انجام کار خود انتخاب نکنید.
                        خدمات دستگاه های هواساز ذیتکو:<br>
                        تعمیر و سرویس دوره ای هواساز<br>
                        تغییر فصل زمستانه و تابستانه<br>
                        رفع عیوب برقی<br>
                        تعویض پد سلولز<br>
                        رفع نشتی آب</span>
                    <div class="description-customer" >
                    <p>توضیح:</p>
                    <p style="font-size:12px;margin-bottom:5px;padding: 0;">{{$results['order_desc']}} </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="desc-wrapper">
            <div class="desc-inner" style="position:relative;">
                <h6 style="text-align:center;"> وضعیت: {{$results['order_type']}}</h6>
                <hr>
                 <span class="right-title">زمان درخواست مشتری:</span>
                 <div class="orderdetails-state">
                     <div class="orderdetails">
                     <span class="right">ساعت : {{$results['order_time_first']}}</span>
                       <span class="left">تاریخ : {{\Morilog\Jalali\Jalalian::forge($results['order_date_first'])->format('%d/ %m/ %Y')}}</span>
                     </div>
                 </div>
                 @if($results['order_type'] == 'تسویه شده')
                 <br>
                <span class="right-title"> زمان پایان:</span>
                     <div class="orderdetails">
                        <span class="right">ساعت: {{\Morilog\Jalali\Jalalian::forge($results['order_detail']['order_end_time'])->format('H:i ')}}</span>
                        <span class="left">تاریخ : {{\Morilog\Jalali\Jalalian::forge($results['order_detail']['order_end_time'])->format('%d/ %m/ %Y')}}</span>
                     </div> 
                  @else($results['order_type'] == 'لغو شده')
                  <br>
                  <span class="right-title">زمان لغو:</span>
                     <div class="orderdetails">
                        <span class="right">ساعت 8 تا 12:</span>
                        <span class="left">تاریخ : 1399/07/12</span>
                     </div> 
                  @endif                      
            </div>
        </div>
        <div class="desc-wrapper">
            <div class="desc-inner" style="position:relative;padding-bottom: 8px;padding-top: 5px;">
                <div class="orderdetails">
                <div class="how-pay-div-right">
                    <span>نحوه پرداخت:</span><br>
                    <span>میزان نقدی:</span><br>
                    <span>میزان اعتباری:</span>
                </div> 
                <div class="how-pay-div-left">
                    <span>{{$results['paytype']}}</span><br>
                    <span>{{$results['cashamount']}}</span><br>
                    <span>{{$results['creditamount']}}</span>
                </div>                              
                </div>
            </div>
        </div>
        <div class="desc-wrapper">
            <div class="desc-inner" style="position:relative;padding-bottom:8px;">
                <h6 class="price-title">فاکتور</h6>
                <div class="factor-div">
                    <div class="factor-img">
                @foreach($results['order_images'] as $images)
                    @if($images['type'] == 'ppf')
                        <img src="{{asset('https://panel.4sooapp.com/uploads/'.$images['link'])}}" id="myImg" alt="">                    
                    @endif 
                @endforeach
                    </div>      
                </div>
            </div>               
        </div>

        <div class="desc-wrapper">
            <div class="desc-inner" style="position:relative;padding-bottom: 8px;">
                <h6 class="price-title">تصاویر</h6>
                <h6 class="center-txt-upload">تصاویر ثبت شده از انجام خدمت</h6>
                <div class="picture-upload-div">
                    <div class="tab-form">
                    <div class="item-wrapp">
                        <div class="order-img-show">
                        @foreach($results['order_images'] as $key=>$item)
                            @if($item['type'] == 'pp1' || $item['type'] == 'pp2' || $item['type'] == 'pp3')                                                     
                                <img  id="service{{$item['id']}}" src="{{asset('https://panel.4sooapp.com/uploads/'.$item['link'])}}" alt=""> 
                            @endif    
                        @endforeach
                        </div>
                    </div>
                </div>
                </div>
            </div>               
        </div>           
        @endif

        <div class="desc-wrapper">
            <div class="desc-inner" style="position:relative;padding-bottom: 8px;">
                <h6 class="price-title">تصاویر</h6>
                <h6 class="center-txt-upload">تصاویر ثبت شده مشتری</h6>
                <div class="picture-upload-div">
                    <div class="tab-form">
                    <div class="item-wrapp">                           
                        <div class="order-img-show">
                        @foreach($results['order_images'] as $key=>$item)
                            @if($item['type'] == 'cp1' || $item['type'] == 'cp2' || $item['type'] == 'cp3')                                                     
                                <img  id="customer{{$item['id']}}" src="{{asset('https://panel.4sooapp.com/uploads/'.$item['link'])}}" alt=""> 
                            @endif    
                        @endforeach
                        </div>
                    </div>
                </div>
                </div>
            </div>               
        </div>
        
         
        <div id="myModal" class="modal-show">
            <span class="close">&times;</span>
            <img class="modal-content-show" id="img">
        </div>

        
            <br><br>
            @if(!($results['order_type'] == 'تسویه شده' || $results['order_type'] == 'لغو شده'))
            <div class="start-btn">
                <input type="submit"  
                @if($results['order_type'] == 'شروع نشده') value="شروع به کار " name="startOrder"
                @elseif($results['order_type'] == 'در حال انجام') value="پایان خدمت" name="endOrder"
                @elseif($results['order_type'] == 'انجام شده') value="دریافت هزینه" name="recivePrice"
                @endif
                 class="start-button">   
                </form>
            </div> 
            @endif                
        </div>
        
        @endsection
    

@section('js')

<script>
function showFactorImage(fileInput) {
var files = fileInput.files;
for (var i = 0; i < files.length; i++) {
var file = files[i];
var imageType = /image.*/;
if (!file.type.match(imageType)) {
continue;
}
var img=document.getElementById("thumbFactor");
img.file = file;
var reader = new FileReader();
reader.onload = (function(aImg) {
return function(e) {
aImg.src = e.target.result;
};
})(img);
reader.readAsDataURL(file);
}
}

function showMyImage1(fileInput) {
var files = fileInput.files;
for (var i = 0; i < files.length; i++) {
var file = files[i];
var imageType = /image.*/;
if (!file.type.match(imageType)) {
continue;
}
var img=document.getElementById("thumbnil1");
img.file = file;
var reader = new FileReader();
reader.onload = (function(aImg) {
return function(e) {
aImg.src = e.target.result;
};
})(img);
reader.readAsDataURL(file);
}
}

function showMyImage2(fileInput) {
var files = fileInput.files;
for (var i = 0; i < files.length; i++) {
var file = files[i];
var imageType = /image.*/;
if (!file.type.match(imageType)) {
continue;
}
var img=document.getElementById("thumbnil2");
img.file = file;
var reader = new FileReader();
reader.onload = (function(aImg) {
return function(e) {
aImg.src = e.target.result;
};
})(img);
reader.readAsDataURL(file);
}
}


function showMyImage3(fileInput) {
var files = fileInput.files;
for (var i = 0; i < files.length; i++) {
var file = files[i];
var imageType = /image.*/;
if (!file.type.match(imageType)) {
continue;
}
var img=document.getElementById("thumbnil3");
img.file = file;
var reader = new FileReader();
reader.onload = (function(aImg) {
return function(e) {
aImg.src = e.target.result;
};
})(img);
reader.readAsDataURL(file);
}
}
var modal = document.getElementById("myModal");
var modalImg = document.getElementById("img");
var img = document.getElementById("myImg");
if(img){
img.onclick = function(){
modal.style.display = "block";
modalImg.src = this.src;
} 
}
var img1 = document.getElementById("myImg1");
if(img1){
img1.onclick = function(){
modal.style.display = "block";
modalImg.src = this.src;
}
}

var img2 = document.getElementById("myImg2");
if(img2){
img2.onclick = function(){
modal.style.display = "block";
modalImg.src = this.src;
}
}
var img3 = document.getElementById("myImg3");
if(img3){
img3.onclick = function(){
modal.style.display = "block";
modalImg.src = this.src;
}
}
@foreach ($results['order_images'] as $key => $item)
var customer = document.getElementById("customer{{ $item['id'] }}");
if(customer){
customer.onclick = function(){
modal.style.display = "block";
modalImg.src = this.src;
} 
}
@endforeach 

@foreach ($results['order_images'] as $key=> $item)
var customer = document.getElementById("service{{$item['id']}}");
if(customer){
customer.onclick = function(){
modal.style.display = "block";
modalImg.src = this.src;
} 
}
@endforeach
var span = document.getElementsByClassName("close")[0];
span.onclick = function() {
modal.style.display = "none";
} 
       
</script>
@endsection