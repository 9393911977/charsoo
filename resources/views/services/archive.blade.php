	@extends('layouts.template')
	@section('content')	

	 <div id="tab-home" class="tab tab-active tab-home" style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}') ; background-size:cover;">
        <!-- home -->
        <div class="img-top">
        	<div class="row top ">
        		<div class="col-md-4 col-sm-4 col-xs-4 right-img">
        			<div class="serach-icon-awesome">
        			<a href="{{route('SearchArchive')}}"><h6><i class="fa fa-search" aria-hidden="true"></i></a>
					</h6>
					</div>	
        		</div>
        		<div class="col-md-4 col-sm-4 col-xs-4 middle-txt">
        			<span> آرشیو</span>
        		</div>
        		<div class="col-md-4 col-sm-4 col-xs-4 left-img">
        			
        		</div>       		
        	</div>
        		<img src="{{asset('webapp-assets/images/top-bg.png')}}" alt="">
        </div>

	        	<div class="orders-wrapper">
	                <div class="orders-inner">
	                    @foreach($results['data'] as $archive)
	                    <a href="{{route('services_details',$archive['order_unique_code'])}}">
							<div class="orders-item 
							@if($archive['order_type'] == 'تسویه شده') success
							@else($archive['order_type'] == 'لغو شده') danger
							@endif
							">
	                            <div class="head">{{$archive['service_name']}}</div>
	                            <div class="body">
	                                <span class="time">{{$archive['order_time_first']}}<img src="{{asset('webapp-assets/images/ic_time.png')}}" alt="" class="ic-time"></span>
	                                <span class="date">{{\Morilog\Jalali\Jalalian::forge($archive['order_date_first'])->format('%d/ %m/ %Y')}}<img src="{{asset('webapp-assets/images/ic_date.png')}}" alt="" class="ic-date"></span>
	                            </div>
	                            <div class="foot">
	                            	<div class="customer-propertes">
										<span> <b>مشخصات مشتری</b> :  
										{{$archive['order_firstname_customer']}}
										{{$archive['order_lastname_customer']}}
									</span>
	                            	</div>
	                            	<div class="customer-propertes">
	                            		<span><b>وضعیت</b>: {{$archive['order_type']}}</span>
	                            	</div>
	                            	<div class="customer-propertes">
	                            		<span><b>آدرس</b>: {{$archive['order_address']}}</span>
	                            	</div>
	                            </div>
	                        </div>
	                    </a>
						@endforeach
	                </div>
	            </div>
	            <br><br><br>
	            @include('footer.footer') 
	 </div>
	@endsection
