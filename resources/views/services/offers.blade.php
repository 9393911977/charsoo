@extends('layouts.template')
@section('content')
    
     <div id="tab-home" class="tab tab-active tab-home" style="background-image:url('{{asset('webapp-assets/images/login_bg.png')}}') ; background-size:cover;">
        <!-- home -->
        <div class="img-top">
            <div class="row top ">

                <div class="col-md-4 col-sm-4 col-xs-4 right-img">
                    <a href="{{route('dashboard')}}"><img  class="arrow-img"src="{{asset('webapp-assets\images\ic_arrow_back.png')}}" alt=""></a>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4 middle-txt">
                    <span>پیشنهادها</span>
                </div> 
                <div class="col-md-4 col-sm-4 col-xs-4 middle-txt">
                </div>           
            </div>
                <img src="{{asset('webapp-assets/images/top-bg.png')}}" alt="">
            </div>
             <div class="orders-wrapper">
                <div class="orders-inner">
                    @foreach($results['data'] as $offers)
                    <a href="{{route('orderServicesDetails' ,$offers['id'])}}">

                        <div class="orders-item success">
                            <div class="head">خدمات هواساز و تغییر فصل</div>
                            <div class="body">
                                <span class="time">{{$offers['order_time_first']}}<img src="{{asset('webapp-assets/images/ic_time.png')}}" alt="" class="ic-time"></span>
                                <span class="date">{{\Morilog\Jalali\Jalalian::forge($offers['order_date_first'])->format('%d/ %m/ %Y')}}<img src="{{asset('webapp-assets/images/ic_date.png')}}" alt="" class="ic-date"></span>
                            </div>
                            <div class="foot">
                                <div class="customer-propertes">
                                    <span>مشخصات مشتری: {{$offers['order_firstname_customer']}} {{$offers['order_lastname_customer']}} </span>
                                </div>
                                <div class="customer-propertes">
                                    <span>وضعیت: {{$offers['order_type']}}</span>
                                </div>
                                <div class="customer-propertes">
                                    <span>آدرس: {{$offers['order_address']}}</span>
                                </div>
                            </div>
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
                <br><br><br>
            @include('footer.footer')

     </div>

 @endsection