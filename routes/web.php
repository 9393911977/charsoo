<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\PersonalController;
use App\Http\Controllers\SalesController;
use App\Http\Controllers\ServicesController;
use App\Http\Controllers\StoresController;

// registeration
Route::get('/',[ MainController::class,'loader'])->name('loader');
Route::get('/getPersonalNumber',[ MainController::class,'getPersonalNumber'])->name('getPersonalNumber');
Route::post('/submitPersonalNumber',[MainController::class,'submitPersonalNumber'])->name('submitPersonalNumber');
Route::get('/getCode',[MainController::class,'getCode'])->name('getCode');
Route::post('/submitCode',[MainController::class,'submitCode'])->name('submitCode');
Route::get('/register',[MainController::class,'register'])->name('register');  
Route::post('/submitRegister',[MainController::class,'submitRegister'])->name('submitRegister');
Route::get('/logout',[MainController::class,'logout'])->name('logout');

Route::get('/dashboard', [PersonalController::class,'dashboard'])->name('dashboard');
Route::get('/profile',[PersonalController::class,'profile'])->name('profile');
Route::post('/submit_profile_send_data',[PersonalController::class,'submit_profile_send_data'])->name('submit_profile_send_data');
Route::get('/account_info',[PersonalController::class,'account_info'])->name('account_info');
Route::post('/send_account_info',[PersonalController::class,'send_account_info'])->name('send_account_info');
Route::get('/activity_details',[PersonalController::class,'activity_details'])->name('activity_details');
Route::get('/income_turnover/{type}',[PersonalController::class,'income_turnover'])->name('income_turnover');
Route::get('/invite_friends',[PersonalController::class,'invite_friends'])->name('invite_friends');

Route::get('/increase_charge',[PersonalController::class,'increase_charge'])->name('increase_charge');
Route::post('/send_increase_charge',[PersonalController::class,'send_increase_charge'])->name('send_increase_charge');
Route::get('/my_skills',[PersonalController::class,'my_skills'])->name('my_skills');

Route::get('/services',[ServicesController::class, 'services'])->name('services');
Route::get('/services/search',[ServicesController::class, 'serviceSearch'])->name('serviceSearch');
Route::post('/search_service',[ServicesController::class, 'search_service'])->name('search_service');
Route::get('/archive',[ServicesController::class, 'archive'])->name('archive');
Route::get('/SearchArchive',[ServicesController::class, 'SearchArchive'])->name('SearchArchive');
Route::post('/search_archive',[ServicesController::class, 'search_archive'])->name('search_archive');
Route::get('/services_details/{id}',[ServicesController::class, 'services_details'])->name('services_details');
Route::post('/send_services_details',[ServicesController::class, 'send_services_details'])->name('send_services_details');
Route::get('/offers',[ServicesController::class, 'offers'])->name('offers');
Route::get('/orderServicesDetails/{id}',[ServicesController::class, 'orderServicesDetails'])->name('orderServicesDetails');
Route::post('/SendOrderServicesDetails',[ServicesController::class, 'SendOrderServicesDetails'])->name('SendOrderServicesDetails');
Route::get('/orderReject/{code}',[ServicesController::class, 'orderReject'])->name('orderReject');

Route::get('/sales',[SalesController::class, 'sales'])->name('sales');
Route::get('/sales_details/{id}',[SalesController::class,'sales_details'])->name('sales_details');
Route::post('/send_sales_details',[SalesController::class,'send_sales_details'])->name('send_sales_details');
Route::get('/sells_cancell/{id}',[SalesController::class,'sells_cancell'])->name('sells_cancell');
Route::get('/sells_archive',[SalesController::class,'sells_archive'])->name('sells_archive');

Route::get('/store',[StoresController::class,'store'])->name('store');
Route::get('/createStore',[StoresController::class,'createStore'])->name('createStore');
Route::post('/submitStore',[StoresController::class,'submitStore'])->name('submitStore');
Route::get('/changeStoreStatus/{status}',[StoresController::class,'changeStoreStatus'])->name('changeStoreStatus');
Route::get('/createProduct',[StoresController::class,'createProduct'])->name('createProduct');
Route::post('/submitProduct',[StoresController::class,'submitProduct'])->name('submitProduct');
Route::get('/editProduct/{product_id}',[StoresController::class,'editProduct'])->name('editProduct');
Route::post('/submitEditProduct',[StoresController::class,'submitEditProduct'])->name('submitEditProduct');
Route::post('/changeProductPrice',[StoresController::class,'changeProductPrice'])->name('changeProductPrice');
Route::get('/changeProductStatus/{product_id}/{status}',[StoresController::class,'changeProductStatus'])->name('changeProductStatus');
Route::get('/deleteProduct/{product_id}',[StoresController::class,'deleteProduct'])->name('deleteProduct');
Route::get('/storeWarehouses',[StoresController::class,'storeWarehouses'])->name('storeWarehouses');
Route::post('/submitStoreWarehouses',[StoresController::class,'submitStoreWarehouses'])->name('submitStoreWarehouses');
Route::get('/storeCategories',[StoresController::class,'storeCategories'])->name('storeCategories');
Route::post('/submitStoreCategories',[StoresController::class,'submitStoreCategories'])->name('submitStoreCategories');

Route::get('/services_archive',[ServicesController::class, 'services_archive'])->name('services_archive');