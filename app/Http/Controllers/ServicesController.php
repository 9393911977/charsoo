<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use carbon\Carbon;

class ServicesController extends Controller
{
    public function services(Request $request){
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        $chs = curl_init('https://panel.4sooapp.com/api/personal/allRelatedOrders');
        curl_setopt($chs, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chs, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $results = curl_exec($chs);
        $errs = curl_error($chs);
        $results = json_decode($results, true);
        curl_close($chs);
        // dd($results);

        $today = Carbon::now();
        $tomorrow = Carbon::now()->addDays(1);
        $threeNext = Carbon::now()->addDays(2);
        $fourNext = Carbon::now()->addDays(3);
        $fiveNext = Carbon::now()->addDays(4);
        $sixNext = Carbon::now()->addDays(5);
        $sevenNext = Carbon::now()->addDays(6);
        
        $shamsiOne = \Morilog\Jalali\Jalalian::forge($today)->format('%A %d %B');
        $shamsiTwo = \Morilog\Jalali\Jalalian::forge($tomorrow)->format('%A %d %B');
        $shamsiThree = \Morilog\Jalali\Jalalian::forge($threeNext)->format('%A %d %B');
        $shamsiFour = \Morilog\Jalali\Jalalian::forge($fourNext)->format('%A %d %B');
        $shamsiFive = \Morilog\Jalali\Jalalian::forge($fiveNext)->format('%A %d %B');
        $shamsSix = \Morilog\Jalali\Jalalian::forge($sixNext)->format('%A %d %B');
        $shamsiSeven = \Morilog\Jalali\Jalalian::forge($sevenNext)->format('%A %d %B');
        $dataTime = array($shamsiOne ,$shamsiTwo,$shamsiThree,$shamsiFour,$shamsiFive,$shamsSix,$shamsiSeven);

        return view('services.services',compact('results' ,'dataTime'));
    }
    public function serviceSearch(Request $request){
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        return view('services.search_service');
    }
    public function search_service(Request $request){
        $Api_Token= $request->cookie('TokenCode');
        $search = $request->key;
        $data = array('search' => $search ,'type'=>1 );
        $jsonData = json_encode($data);
        $ch = curl_init('https://panel.4sooapp.com/api/personal/searchorders');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($jsonData),
        'Authorization: Bearer '.$Api_Token
        ));
        $result = curl_exec($ch);
        $err = curl_error($ch);
        $result = json_decode($result, true);
        curl_close($ch);


        $response = '
        <div class="orders-wrapper">
        <div class="orders-inner">
        ';
        foreach($result['data'] as $service_data) {
            $response .= '
            <a href=" '.route('services_details' , $service_data['order_unique_code']).' ">
            <div class="orders-item 
            '.($service_data['order_type']=='شروع نشده' ? 'default' : '').'
            '.($service_data['order_type']=='در حال انجام' ? 'warning': '').'
            '.($service_data['order_type']=='انجام شده' ? 'success' : '').'              
            ">
                <div class="head">'.$service_data['service_name'].'</div>
                <div class="body">
                    <span class="time">'.$service_data['final_time'].'<img src="'.asset('webapp-assets/images/ic_time.png').'" alt="" class="ic-time"></span>
                    <span class="date">
                    '.\Morilog\Jalali\Jalalian::forge($service_data['final_date'])->format('%d/ %m/ %Y').'
                    <img src="'.asset('webapp-assets/images/ic_date.png').'" alt="" class="ic-date"></span>
                </div>
                <div class="foot">
                    <div class="customer-propertes">
                        <span>
                        مشخصات مشتری :
                        '.$service_data['order_firstname_customer'].'
                        '.$service_data['order_lastname_customer'].'						
                        </span>
                    </div>
                    <div class="customer-propertes">
                    وضعیت :
                        <span>'.$service_data['order_type'].'</span>
                    </div>
                    <div class="customer-propertes">
                    آدرس :
                        <span>'.$service_data['order_address'].'</span>
                    </div>
                </div>
            </div>
        </a>
            ';
        }              
            
            $response .= '
            </div>
            </div>
            ';
        return $response;
    }
    public function archive(Request $request){
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }

        $Api_Token= $request->cookie('TokenCode');
        $chs = curl_init('https://panel.4sooapp.com/api/personal/finishedOrders');
        curl_setopt($chs, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chs, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $results = curl_exec($chs);
        $errs = curl_error($chs);
        $results = json_decode($results, true);
        curl_close($chs);
        // dd($results);
        return view('services.archive',compact('results'));
    }
    public function SearchArchive(Request $request){
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
            return view('services.search_archive');     
    }
    public function search_archive(Request $request){
        $Api_Token= $request->cookie('TokenCode');
        $search = $request->key;
        $data = array('search' => $search ,'type'=>3 );
        $jsonData = json_encode($data);
        $ch = curl_init('https://panel.4sooapp.com/api/personal/searchorders');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($jsonData),
        'Authorization: Bearer '.$Api_Token
        ));
        $result = curl_exec($ch);
        $err = curl_error($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        // dd($result);

        $search = '
        <div class="orders-wrapper">
	                <div class="orders-inner">
        ';
	                    foreach($result['data'] as $archive){
                            $search .='
                            <a href="'.route('services_details',$archive['order_unique_code']).'">
                            <div class="orders-item                            
                            '.($archive['order_type'] == 'تسویه شده' ? 'success' : '') .'
                            '.($archive['order_type'] == 'لغو شده' ? 'danger' : '') .'
							">
	                            <div class="head">'.$archive['service_name'].'</div>
	                            <div class="body">
	                                <span class="time">'.$archive['order_time_first'].'<img src="'.asset('webapp-assets/images/ic_time.png').'" alt="" class="ic-time"></span>
	                                <span class="date">'.\Morilog\Jalali\Jalalian::forge($archive['order_date_first'])->format('%d/ %m/ %Y').'<img src="'.asset('webapp-assets/images/ic_date.png').'" alt="" class="ic-date"></span>
	                            </div>
	                            <div class="foot">
	                            	<div class="customer-propertes">
										<span> <b>مشخصات مشتری</b> :  
										'.$archive['order_firstname_customer'].'
										'.$archive['order_lastname_customer'].'
									</span>
	                            	</div>
	                            	<div class="customer-propertes">
	                            		<span><b>وضعیت</b>: '.$archive['order_type'].'</span>
	                            	</div>
	                            	<div class="customer-propertes">
	                            		<span><b>آدرس</b>: '.$archive['order_address'].'</span>
	                            	</div>
	                            </div>
	                        </div>
	                    </a> 
                            ';
                        }
	                             
        $search .= '
        </div>
        </div>
        '; 
        return $search;       
    }
    public function services_details(Request $request){
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        $idCode = $request->id;
        $data = array('order_code' => $idCode);
        $jsonData = json_encode($data);
        $chs = curl_init('https://panel.4sooapp.com/api/personal/getorder');
        curl_setopt($chs, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($chs, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chs, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $results = curl_exec($chs);
        $errs = curl_error($chs);
        $results = json_decode($results, true);
        curl_close($chs);
        // dd($results);
        return view('services.services_details',compact('results'));
    }
    public function send_services_details(Request $request){

        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }

        $Api_Token= $request->cookie('TokenCode');

        $comment = $request->comment;
        $unique_code = $request->unique_code;
        
        $start_order = $request->startOrder;
        $end_order = $request->endOrder;
        $recive_price = $request->recivePrice;


        if($start_order != null){
            $data = array('order_code' => $unique_code ,'positionslat'=>'','positionslon'=>'','description'=>$comment );
            $jsonData = json_encode($data);
            $ch = curl_init('https://panel.4sooapp.com/api/personal/startOrder');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonData),
            'Authorization: Bearer '.$Api_Token
            ));
            $result = curl_exec($ch);
            $err = curl_error($ch);
            $result = json_decode($result, true);
            curl_close($ch);
        }
        elseif($end_order != null ){
            
            $price_service = $request->price_service;
            $price_tool = $request->price_tool;
            $dataE = array('order_code' => $unique_code ,'positionslat'=>'',
            'positionslon'=>'','description'=>'','order_cast'=>$price_service,'pieces_cast'=>$price_tool);
            $jsonDataE = json_encode($dataE);
            $chE = curl_init('https://panel.4sooapp.com/api/personal/endOrder');
            curl_setopt($chE, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($chE, CURLOPT_POSTFIELDS, $jsonDataE);
            curl_setopt($chE, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($chE, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonDataE),
            'Authorization: Bearer '.$Api_Token
            ));
            $resultE = curl_exec($chE);
            $errE = curl_error($chE);
            $resultE = json_decode($resultE, true);
            curl_close($chE);
            
            if($request->has('factororderImg')){
                $data_f = array('image_type_code' => 'ppf','order_code'=>$unique_code ,'image'=> new \CurlFile($_FILES['factororderImg']['tmp_name'],
                $_FILES['factororderImg']['type'], $_FILES['factororderImg']['name']));
                $ch_f = curl_init('https://panel.4sooapp.com/api/personal/uploadImages');
                curl_setopt($ch_f, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch_f, CURLOPT_POSTFIELDS, $data_f);
                curl_setopt($ch_f, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch_f, CURLOPT_HTTPHEADER, array(
                'Content-Type: multipart/form-data',
                "Authorization: Bearer ".$Api_Token
                ));
                $result_f = curl_exec($ch_f);
                $err_f = curl_error($ch_f);
                $result_f = json_decode($result_f, true);
                curl_close($ch_f);
                // dd($result_f );
            }

            if($request->has('firstorderImg')){
                $data_f = array('image_type_code' => 'pp1','order_code'=>$unique_code ,'image'=> new \CurlFile($_FILES['firstorderImg']['tmp_name'],
                $_FILES['firstorderImg']['type'], $_FILES['firstorderImg']['name']));
                $ch_f = curl_init('https://panel.4sooapp.com/api/personal/uploadImages');
                curl_setopt($ch_f, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch_f, CURLOPT_POSTFIELDS, $data_f);
                curl_setopt($ch_f, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch_f, CURLOPT_HTTPHEADER, array(
                'Content-Type: multipart/form-data',
                "Authorization: Bearer ".$Api_Token
                ));
                $result_f = curl_exec($ch_f);
                $err_f = curl_error($ch_f);
                $result_f = json_decode($result_f, true);
                curl_close($ch_f);
                // dd($result_f );
            }

            if($request->has('secondorderImg')){
                $data_f = array('image_type_code' => 'pp2','order_code'=>$unique_code ,'image'=> new \CurlFile($_FILES['secondorderImg']['tmp_name'],
                $_FILES['secondorderImg']['type'], $_FILES['secondorderImg']['name']));
                $ch_f = curl_init('https://panel.4sooapp.com/api/personal/uploadImages');
                curl_setopt($ch_f, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch_f, CURLOPT_POSTFIELDS, $data_f);
                curl_setopt($ch_f, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch_f, CURLOPT_HTTPHEADER, array(
                'Content-Type: multipart/form-data',
                "Authorization: Bearer ".$Api_Token
                ));
                $result_f = curl_exec($ch_f);
                $err_f = curl_error($ch_f);
                $result_f = json_decode($result_f, true);
                curl_close($ch_f);
                // dd($result_f );
            }

            if($request->has('thirdorderImg')){
                $data_f = array('image_type_code' => 'pp3','order_code'=>$unique_code ,'image'=> new \CurlFile($_FILES['thirdorderImg']['tmp_name'],
                $_FILES['thirdorderImg']['type'], $_FILES['thirdorderImg']['name']));
                $ch_f = curl_init('https://panel.4sooapp.com/api/personal/uploadImages');
                curl_setopt($ch_f, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch_f, CURLOPT_POSTFIELDS, $data_f);
                curl_setopt($ch_f, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch_f, CURLOPT_HTTPHEADER, array(
                'Content-Type: multipart/form-data',
                "Authorization: Bearer ".$Api_Token
                ));
                $result_f = curl_exec($ch_f);
                $err_f = curl_error($ch_f);
                $result_f = json_decode($result_f, true);
                curl_close($ch_f);
                // dd($result_f );
            }
        }
        elseif($recive_price != null){
            $dataR = array('order_code' => $unique_code);
            $jsonDataR = json_encode($dataR);
            $chR = curl_init('https://panel.4sooapp.com/api/personal/reckoningorder');
            curl_setopt($chR, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($chR, CURLOPT_POSTFIELDS, $jsonDataR);
            curl_setopt($chR, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($chR, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonDataR),
            'Authorization: Bearer '.$Api_Token
            ));
            $resultR = curl_exec($chR);
            $errR = curl_error($chR);
            $resultR = json_decode($resultR, true);
            curl_close($chR);
            // dd($resultR);
        }
        return redirect(route('services'));
    }
    public function offers(Request $request){
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        $chs = curl_init('https://panel.4sooapp.com/api/personal/offeringOrders');
        curl_setopt($chs, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chs, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $results = curl_exec($chs);
        $errs = curl_error($chs);
        $results = json_decode($results, true);
        curl_close($chs);
        
        return view('services.offers',compact('results'));
    }
    public function orderServicesDetails(Request $request){
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        $id = $request->id;
        $datas = array('order'=>$id);
        $jsonDatas = json_encode($datas);
        $chs = curl_init('https://panel.4sooapp.com/api/personal/offerorder');
        curl_setopt($chs, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($chs, CURLOPT_POSTFIELDS, $jsonDatas);
        curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chs, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $results = curl_exec($chs);
        $errs = curl_error($chs);
        $results = json_decode($results, true);
        curl_close($chs);

        $today = Carbon::now();
        $tomorrow = Carbon::now()->addDays(1);
        $threeNext = Carbon::now()->addDays(2);
        $fourNext = Carbon::now()->addDays(3);
        $fiveNext = Carbon::now()->addDays(4);
        $sixNext = Carbon::now()->addDays(5);
        $sevenNext = Carbon::now()->addDays(6);
        
        $shamsiOne = \Morilog\Jalali\Jalalian::forge($today)->format('%A %d %B');
        $shamsiTwo = \Morilog\Jalali\Jalalian::forge($tomorrow)->format('%A %d %B');
        $shamsiThree = \Morilog\Jalali\Jalalian::forge($threeNext)->format('%A %d %B');
        $shamsiFour = \Morilog\Jalali\Jalalian::forge($fourNext)->format('%A %d %B');
        $shamsiFive = \Morilog\Jalali\Jalalian::forge($fiveNext)->format('%A %d %B');
        $shamsSix = \Morilog\Jalali\Jalalian::forge($sixNext)->format('%A %d %B');
        $shamsiSeven = \Morilog\Jalali\Jalalian::forge($sevenNext)->format('%A %d %B');
        $dataTime = array($shamsiOne ,$shamsiTwo,$shamsiThree,$shamsiFour,$shamsiFive,$shamsSix,$shamsiSeven);
        
        if($results['canoffer'] == 2){
            $id = $results['id'];
            $data = array('order'=>$id);
            $jsonData = json_encode($datas);
            $ch = curl_init('https://panel.4sooapp.com/api/personal/offer');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
            $result = curl_exec($ch);
            $err = curl_error($ch);
            $result = json_decode($result, true);
            curl_close($ch);
            return view ('services.orderServicesDetails',compact('results' ,'dataTime','result'));
        }
        // dd($results);
        return view ('services.orderServicesDetails',compact('results' ,'dataTime'));
    }
    public function SendOrderServicesDetails(Request $request){
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        if(is_null($request->amount) || empty($request->amount)){
            return back()->with("error","مبلغ پیشنهادی خود را وارد نمایید");
        }
        $Api_Token= $request->cookie('TokenCode');

        if($request->simple == "custom"){
            $date = $request->select_day_time;
            $time = $request->selectClock;
        }elseif($request->simple == "first_time"){
            $date = 11;
            $time = 0;
        }elseif($request->simple == "second_time"){
            $date = 12;
            $time = 0;
        }

        $data = array(
            'order' => $request->order ,
            'description' => $request->description ,
            'amount'=>$request->amount ,
            'date'=>$date ,
            'time'=>$time
        );
        $jsonData = json_encode($data);
        $ch = curl_init('https://panel.4sooapp.com/api/personal/offerorder');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($jsonData),
        'Authorization: Bearer '.$Api_Token
        ));
        $result = curl_exec($ch);
        $err = curl_error($ch);
        $result = json_decode($result, true);
        curl_close($ch);

        return redirect(route('offers'));

    }
    public function services_archive(){
        return view('services.services_archive');
    }
    public function orderReject(Request $request){
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');

        $data = array(
            'order_code' => $request->code
        );
        $jsonData = json_encode($data);
        $ch = curl_init('https://panel.4sooapp.com/api/personal/rejectOrder');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($jsonData),
        'Authorization: Bearer '.$Api_Token
        ));
        $result = curl_exec($ch);
        $err = curl_error($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        if(isset($result['data'])){
            return redirect(route('services'))->with('success' , 'سفارش با موفقیت عودت داده شد .');
        }else{
            return redirect(route('services'))->with('error' , 'خطا هنگام عودت سفارش');
        }
    }
}
