<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PersonalController extends Controller
{
    //
    public function dashboard(Request $request){

        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $date = \Morilog\Jalali\Jalalian::now()->format('%A %d %B');
        $Api_Token= $request->cookie('TokenCode');
        $chs = curl_init('https://panel.4sooapp.com/api/personal/dashboarddetail');
        curl_setopt($chs, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chs, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $results = curl_exec($chs);
        $errs = curl_error($chs);
        $results = json_decode($results, true);
        curl_close($chs);
        $incomeLen = strlen($results['incomecash']);
        $chargeLen = strlen($results['chargecash']);

        if($incomeLen <= 3){
            $incomecash = 0;
        }elseif($incomeLen==4){
            $incomecash = substr($results['incomecash'] , 0 , 1);
        }elseif($incomeLen==5){
            $incomecash = substr($results['incomecash'] , 0 , 2);
        }elseif($incomeLen==6){
            $incomecash = substr($results['incomecash'] , 0 , 3);
        }elseif($incomeLen==7){
            $incomecash = substr($results['incomecash'] , 0 , 4);
        }

        if($chargeLen <= 3){
            $chargecash = 0;
        }elseif($chargeLen==4){
            $chargecash = substr($results['chargecash'] , 0 , 1);
        }elseif($chargeLen==5){
            $chargecash = substr($results['chargecash'] , 0 , 2);
        }elseif($chargeLen==6){
            $chargecash = substr($results['chargecash'] , 0 , 3);
        }elseif($chargeLen==7){
            $chargecash = substr($results['chargecash'] , 0 , 4);
        }
 
        return view('.personal.dashboard',compact('results','date' ,'chargecash','incomecash'));
        
    }
    public function profile(Request $request){

        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        $chs = curl_init('https://panel.4sooapp.com/api/personal/getData');
        //$chs = curl_init('https://sandbox.zarinpal.com/pg/rest/WebGate/PaymentRequest.json');
        // curl_setopt($chs, CURLOPT_USERAGENT, 'ZarinPal Rest Api v1');
        curl_setopt($chs, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($chs, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $results = curl_exec($chs);
        $errs = curl_error($chs);
        $results = json_decode($results, true);
        curl_close($chs);
        // dd($results); 
        $newarray = [];
        $newarray[] = $results['f_identify_checkout'];
        $newarray[] = $results['s_identify_checkout'];
        $newarray[] = $results['duty_checkout'];
        $newarray[] = $results['antecedent_checkout'];
        $newarray[] = $results['f_national_checkout'];
        $newarray[] = $results['b_national_checkout'];
        $collection = collect($newarray);
        $filtered = $collection->filter(function ($value, $key) {
        return $value == 0;
        });
        $collection = collect($filtered);
        $defects=$collection->count();
        
        // api for cities
        $chCities = curl_init('https://panel.4sooapp.com/api/getCities');
        curl_setopt($chCities, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($chCities, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chCities, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $cityResults = curl_exec($chCities);
        $errs = curl_error($chCities);
        $cityResults = json_decode($cityResults, true);
        curl_close($chCities);
        // dd($cityResults);
        $month=$results['personal_work_experience_month'];
        $year = $results['personal_work_experience_year'];
        if($year !== null && $year !== 0){
            $year = $year * 12;
            if($month !== null && $month !== 0){
                $year = $year + $month;
            }
        }
        return view('personal.profile',compact('results','defects','cityResults','year'));
        
    }


    public function submit_profile_send_data(Request $request){
        $Api_Token= $request->cookie('TokenCode');
        // dd($request);

        $firstname = $request->fname;
        $lastname = $request->lname;
        $national_code = $request->national_code;
        $birthday = $request->birthday;
        $marriage = $request->marriage;
        $study_status = $request->study_status;
        $city_name = $request->city_name;
        $office_phone = $request->office_phone;
        $post_code = $request->post_code;
        $home_phone = $request->home_phone;
        $adress = $request->adress;
        
        $data = array(
            'personal_firstname' => $firstname ,
            'personal_lastname' =>$lastname ,
            'personal_national_code' =>$national_code ,
            'personal_birthday' =>$birthday ,
            'personal_marriage' =>$marriage ,
            'personal_last_diploma' =>$study_status ,
            'personal_city' =>$city_name ,
            'personal_office_phone' =>$office_phone ,
            'personal_postal_code' =>$post_code ,
            'personal_home_phone' =>$home_phone ,
            'personal_address' =>$adress ,
        );
        $jsonData = json_encode($data);
        $ch = curl_init('https://panel.4sooapp.com/api/personal/updatedata');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($jsonData),
        'Authorization: Bearer '.$Api_Token
        ));
        $result = curl_exec($ch);
        $err = curl_error($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        // dd($result);
        // api for profile image
        
        if ($request->profileImg && $request->profileImg !== null) {
            $datap = array('personal_profile'=> new \CurlFile($_FILES['profileImg']['tmp_name'], 
            $_FILES['profileImg']['type'], $_FILES['profileImg']['name']));
            $chp = curl_init('https://panel.4sooapp.com/api/personal/updateprofile');
            curl_setopt($chp, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($chp, CURLOPT_POSTFIELDS, $datap);
            curl_setopt($chp, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($chp, CURLOPT_HTTPHEADER, array(
            'Content-Type: multipart/form-data',
            "Authorization: Bearer ".$Api_Token
            ));
            $resultp = curl_exec($chp);
            $errp = curl_error($chp);
            $resultp = json_decode($resultp, true);
            curl_close($chp);
        }
        // api for first page card
        if ($request->identity_card_first_pic && $request->identity_card_first_pic !== null) {
            $data_fc = array('personal_identity_card_first_pic'=> new \CurlFile($_FILES['identity_card_first_pic']['tmp_name'],
             $_FILES['identity_card_first_pic']['type'], $_FILES['identity_card_first_pic']['name']));
            $ch_fc = curl_init('https://panel.4sooapp.com/api/personal/identityCardFirst');
            curl_setopt($ch_fc, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch_fc, CURLOPT_POSTFIELDS, $data_fc);
            curl_setopt($ch_fc, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch_fc, CURLOPT_HTTPHEADER, array(
            'Content-Type: multipart/form-data',
            "Authorization: Bearer ".$Api_Token
            ));
            $result_fc = curl_exec($ch_fc);
            $err_fc = curl_error($ch_fc);
            $result_fc = json_decode($result_fc, true);
            curl_close($ch_fc);
        }
        // api for second page card
        if ($request->identity_card_second_pic && $request->identity_card_second_pic !== null) {
            $data_sc = array('personal_identity_card_second_pic'=> new \CurlFile($_FILES['identity_card_second_pic']['tmp_name'],
             $_FILES['identity_card_second_pic']['type'], $_FILES['identity_card_second_pic']['name']));
            $ch_sc = curl_init('https://panel.4sooapp.com/api/personal/identityCardSecond');
            curl_setopt($ch_sc, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch_sc, CURLOPT_POSTFIELDS, $data_sc);
            curl_setopt($ch_sc, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch_sc, CURLOPT_HTTPHEADER, array(
            'Content-Type: multipart/form-data',
            "Authorization: Bearer ".$Api_Token
            ));
            $result_sc = curl_exec($ch_sc);
            $err_sc = curl_error($ch_sc);
            $result_sc = json_decode($result_sc, true);
            curl_close($ch_sc);
        }
        // api for status duty
        if ($request->status_duty && $request->status_duty !== null) {
            $data_sd = array('personal_status_duty'=> new \CurlFile($_FILES['status_duty']['tmp_name'],
             $_FILES['status_duty']['type'], $_FILES['status_duty']['name']));
            $ch_sd = curl_init('https://panel.4sooapp.com/api/personal/statusDuty');
            curl_setopt($ch_sd, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch_sd, CURLOPT_POSTFIELDS, $data_sd);
            curl_setopt($ch_sd, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch_sd, CURLOPT_HTTPHEADER, array(
            'Content-Type: multipart/form-data',
            "Authorization: Bearer ".$Api_Token
            ));
            $result_sd = curl_exec($ch_sd);
            $err_sd = curl_error($ch_sd);
            $result_sd = json_decode($result_sd, true);
            curl_close($ch_sd);
        }
        // api for background status
        if ($request->backgrounds_status && $request->backgrounds_status !== null) {
            $data_bs = array('personal_backgrounds_status'=> new \CurlFile($_FILES['backgrounds_status']['tmp_name'],
             $_FILES['backgrounds_status']['type'], $_FILES['backgrounds_status']['name']));
            $ch_bs = curl_init('https://panel.4sooapp.com/api/personal/backgroundsStatus');
            curl_setopt($ch_bs, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch_bs, CURLOPT_POSTFIELDS, $data_bs);
            curl_setopt($ch_bs, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch_bs, CURLOPT_HTTPHEADER, array(
            'Content-Type: multipart/form-data',
            "Authorization: Bearer ".$Api_Token
            ));
            $result_bs = curl_exec($ch_bs);
            $err_bs = curl_error($ch_bs);
            $result_bs = json_decode($result_bs, true);
            curl_close($ch_bs);
        }
        // api for front of national card
        if ($request->national_card_front_pic && $request->national_card_front_pic !== null) {
            $data_fnc = array('personal_national_card_front_pic'=> new \CurlFile($_FILES['national_card_front_pic']['tmp_name'],
             $_FILES['national_card_front_pic']['type'], $_FILES['national_card_front_pic']['name']));
            $ch_fnc = curl_init('https://panel.4sooapp.com/api/personal/nationalCardFront');
            curl_setopt($ch_fnc, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch_fnc, CURLOPT_POSTFIELDS, $data_fnc);
            curl_setopt($ch_fnc, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch_fnc, CURLOPT_HTTPHEADER, array(
            'Content-Type: multipart/form-data',
            "Authorization: Bearer ".$Api_Token
            ));
            $result_fnc = curl_exec($ch_fnc);
            $err_fnc = curl_error($ch_fnc);
            $result_fnc = json_decode($result_fnc, true);
            curl_close($ch_fnc);
        }
        // api for back of national card
        if ($request->national_card_back_pic && $request->national_card_back_pic !== null) {
            $data_bnc = array('personal_national_card_back_pic'=> new \CurlFile($_FILES['national_card_back_pic']['tmp_name'],
             $_FILES['national_card_back_pic']['type'], $_FILES['national_card_back_pic']['name']));
            $ch_bnc = curl_init('https://panel.4sooapp.com/api/personal/nationalCardBack');
            curl_setopt($ch_bnc, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch_bnc, CURLOPT_POSTFIELDS, $data_bnc);
            curl_setopt($ch_bnc, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch_bnc, CURLOPT_HTTPHEADER, array(
            'Content-Type: multipart/form-data',
            "Authorization: Bearer ".$Api_Token
            ));
            $result_bnc = curl_exec($ch_bnc);
            $err_bnc = curl_error($ch_bnc);
            $result_bnc = json_decode($result_bnc, true);
            curl_close($ch_bnc);
        }
        return redirect(route('dashboard'));
    }
   

    public function account_info(Request $request){
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        $chs = curl_init('https://panel.4sooapp.com/api/personal/bankinfo');
        curl_setopt($chs, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chs, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $results = curl_exec($chs);
        $errs = curl_error($chs);
        $results = json_decode($results, true);
        curl_close($chs);
        // dd($results);
        if(isset($results['shaba'])){
            $check = true;
        }else{
            $check = false;
        }
        return view('personal.account_info',compact('results','check'));
    }
    public function send_account_info(Request $request){
        $Api_Token= $request->cookie('TokenCode');
        $shaba_number = $request->shaba_number;
        $account_name = $request->account_name;
        $bank_name = $request->bank_name;
        $data = array('shaba'=>$shaba_number ,'name'=>$account_name ,'bankname'=>$bank_name);
        $jsonData = json_encode($data);
        $chs = curl_init('https://panel.4sooapp.com/api/personal/bankinfosave');
        curl_setopt($chs, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chs, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($chs, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $results = curl_exec($chs);
        $errs = curl_error($chs);
        $results = json_decode($results, true);
        curl_close($chs);
        $msg = 'اطلاعات با موفقیت ارسال شد';
        return redirect(route('account_info'))->with('success',$msg);
    }
    public function activity_details(Request $request){
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        $chs = curl_init('https://panel.4sooapp.com/api/personal/statistics');
        curl_setopt($chs, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chs, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $results = curl_exec($chs);
        $errs = curl_error($chs);
        $results = json_decode($results, true);
        curl_close($chs);

        $scores = $results['score']/20;
        $intScores = (int)$scores;
        $remain = $results['score']%20;
        return view('personal.activity_details',compact('results','intScores','remain'));
    }

    public function income_turnover(Request $request){
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        
        $chs = curl_init('https://panel.4sooapp.com/api/personal/useraccounts');
        curl_setopt($chs, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chs, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $result = curl_exec($chs);
        $errs = curl_error($chs);
        $result = json_decode($result, true);
        curl_close($chs);
        $arrayLenght = count($result['data']);

        $data = array('type' => 'شارژ');
        $jsonData = json_encode($data);
        $ch = curl_init('https://panel.4sooapp.com/api/personal/useraccounts/transactions');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($jsonData),
        'Authorization: Bearer '.$Api_Token
        ));
        $results = curl_exec($ch);
        $err = curl_error($ch);
        $results = json_decode($results, true);
        curl_close($ch);
        $chargeCount = count($results['data']);
        // dd($results);

        $transactions = 0;
            $charge = 0;
            $Deduction = 0;
            $Remaining = 0;
        if(!empty($results)){
            foreach($results['data'] as $item){
                if($item['method'] == 'اعتباری'){
                    $transactions += 1;
                    if($item['type'] == 'واریز'){
                        $charge += $item['amount'];
                        $Remaining += $item['amount'];
                    }elseif($item['type'] == 'برداشت'){
                        $Deduction += $item['amount'];
                        $Remaining -= $item['amount'];
                    }
                }
            }
        }    
            
        $datab = array('type' => 'درآمد');
        $jsonDatab = json_encode($datab);
        $chb = curl_init('https://panel.4sooapp.com/api/personal/useraccounts/transactions');
        curl_setopt($chb, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($chb, CURLOPT_POSTFIELDS, $jsonDatab);
        curl_setopt($chb, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chb, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($jsonDatab),
        'Authorization: Bearer '.$Api_Token
        ));
        $resultsb = curl_exec($chb);
        $errb = curl_error($chb);
        $resultsb = json_decode($resultsb, true);
        curl_close($chb);

        $transactionsB = 0;
            $chargeB = 0;
            $DeductionB = 0;
            $RemainingB = 0;
        if(!empty($resultsb)){
            foreach($resultsb['data'] as $itemB){
                if($itemB['method'] == 'اعتباری'){
                    $transactionsB += 1;
                    if($itemB['type'] == 'واریز'){
                        $chargeB += $itemB['amount'];
                        $RemainingB += $itemB['amount'];
                    }elseif($itemB['type'] == 'برداشت'){
                        $DeductionB += $itemB['amount'];
                        $RemainingB -= $itemB['amount'];
                    }
                }
            }
        }    
        // dd($resultsb);
        
        $datak = array('type' => 'کسورات قانونی');
        $jsonDatak = json_encode($datak);
        $chk = curl_init('https://panel.4sooapp.com/api/personal/useraccounts/transactions');
        curl_setopt($chk, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($chk, CURLOPT_POSTFIELDS, $jsonDatak);
        curl_setopt($chk, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chk, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($jsonDatak),
        'Authorization: Bearer '.$Api_Token
        ));
        $resultsk = curl_exec($chk);
        $errk = curl_error($chk);
        $resultsk = json_decode($resultsk, true);
        curl_close($chk);

        $transactionsK = 0;
            $chargeK = 0;
            $DeductionK = 0;
            $RemainingK = 0;
        if(!empty($resultsk)){
            foreach($resultsk['data'] as $itemK){
                if($itemK['method'] == 'اعتباری'){
                    $transactionsK += 1;
                    if($itemK['type'] == 'واریز'){
                        $chargeK += $itemK['amount'];
                        $RemainingK += $itemK['amount'];
                    }elseif($itemK['type'] == 'برداشت'){
                        $DeductionK += $itemK['amount'];
                        $RemainingK -= $itemK['amount'];
                    }
                }
            }
        }    
        $inc = $request->type;
        return view('personal.income_turnover',compact(
         'arrayLenght','results','resultsb' ,'resultsk','inc',
         'transactions','charge','Deduction','Remaining',
         'transactionsB','chargeB','DeductionB','RemainingB',
         'transactionsK','chargeK','DeductionK','RemainingK',
         'chargeCount'
    ));
    
    }
    public function increase_charge(Request $request ){
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        $chs = curl_init('https://panel.4sooapp.com/api/personal/dashboarddetail');
        curl_setopt($chs, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chs, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $results = curl_exec($chs);
        $errs = curl_error($chs);
        $results = json_decode($results, true);
        curl_close($chs);
        // dd($results); 

        return view('personal.increase_charge',compact('results'));
    }
    public function send_increase_charge(Request $request){
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        $payMethod = $request->pay;
        $amount = $request->amount;
        // dd($payMethod);
        // dd($amount);
        if($payMethod == null || $amount == null){
            $error = 'نحوه شارژ و مبلغ مورد نظر را انتخاب کنید';
            return redirect(route('increase_charge'))->with('error' , $error);
        }

        if($payMethod == 'bank'){
        $worker = 'worker';
        $datab = array('amount'=>$amount ,'type'=>$worker);
        $jsonDatab = json_encode($datab);
        $chsb = curl_init('https://panel.4sooapp.com/api/pay/token');
        curl_setopt($chsb, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($chsb, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chsb, CURLOPT_POSTFIELDS, $jsonDatab);
        curl_setopt($chsb, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $resultsb = curl_exec($chsb);
        $errsb = curl_error($chsb);
        $resultsb = json_decode($resultsb, true);
        curl_close($chsb);

        // dd($resultsb);
        $code = $resultsb['code'];
            return redirect('https://panel.4sooapp.com/pay?paytoken='.$code);

        }elseif($payMethod == 'income'){     
            $data = array('amount'=>$amount );
            $jsonData = json_encode($data);
            $chs = curl_init('https://panel.4sooapp.com/api/pay/intoch');
            curl_setopt($chs, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($chs, CURLOPT_POSTFIELDS, $jsonData);
            curl_setopt($chs, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
            $results = curl_exec($chs);
            $errs = curl_error($chs);
            $results = json_decode($results, true);
            curl_close($chs);
            // dd($results);
            if(isset($results['message'])){
                $error = $results['message'];
                return redirect(route('increase_charge'))->with('error' , $error);
            }else{
                return redirect(route('dashboard'));
            }
        }
    }
    public function my_skills(){
        return view('personal.my_skills');
    }
    public function invite_friends(){
        return view('personal.invite_friends');
    }
}
