<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SalesController extends Controller
{
    //
    public function sales(Request $request){
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        $chs = curl_init('https://panel.4sooapp.com/api/store/ordersp');
        curl_setopt($chs, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chs, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $results = curl_exec($chs);
        $errs = curl_error($chs);
        $results = json_decode($results, true);
        curl_close($chs);
        // dd($results);
        return view('sales.sales',compact('results'));
    }
    public function sales_details(Request $request){
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        $id = $request->id;
        // dd($id);
        $datas = array('code'=>$id);
        $jsonDatas = json_encode($datas);
        $chs = curl_init('https://panel.4sooapp.com/api/store/goodsorderdetail');
        curl_setopt($chs, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($chs, CURLOPT_POSTFIELDS, $jsonDatas);
        curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chs, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $results = curl_exec($chs);
        $errs = curl_error($chs);
        $results = json_decode($results, true);
        curl_close($chs);
        // dd($results);
        return view('sales.sales_details',compact('results'));
    }
    public function send_sales_details(Request $request){
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        
        $confirm =$request->confirm;
        $preparing =$request->preparing;
        $send =$request->send;
        $sended =$request->sended;
        $uniqueCode =$request->uniqueCode;
        $deliverCode =$request->deliverCode;

        if($confirm !== null){
            $data = array('code'=>$uniqueCode);
            $jsonData = json_encode($data);
            $ch = curl_init('https://panel.4sooapp.com/api/store/orderaccept');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
            $result = curl_exec($ch);
            $err = curl_error($ch);
            $result = json_decode($result, true);
            curl_close($ch);
            return redirect(route('sales'));
        }
        if($preparing !== null){
            $datap = array('code'=>$uniqueCode);
            $jsonDatap = json_encode($datap);
            $chp = curl_init('https://panel.4sooapp.com/api/store/orderprepare');
            curl_setopt($chp, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($chp, CURLOPT_POSTFIELDS, $jsonDatap);
            curl_setopt($chp, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($chp, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
            $resultp = curl_exec($chp);
            $errp = curl_error($chp);
            $resultp = json_decode($resultp, true);
            curl_close($chp);
            if(isset($resultp['message'])){
                return back()->with('error' , $resultp['message']);
            }else{
                return redirect(route('sales'));
            }
        }
        if($send !== null){
            $data = array('code'=>$uniqueCode);
            $jsonData = json_encode($data);
            $ch = curl_init('https://panel.4sooapp.com/api/store/ordersend');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
            $result = curl_exec($ch);
            $err = curl_error($ch);
            $result = json_decode($result, true);
            curl_close($ch);
            
            if($request->has('sellFactorImg')){
                $factorImg = $request->sellFactorImg;
                $data_f = array('type' => 'ppf','code'=>$uniqueCode ,'image'=> new \CurlFile($_FILES['sellFactorImg']['tmp_name'],
                $_FILES['sellFactorImg']['type'], $_FILES['sellFactorImg']['name']));
                $ch_f = curl_init('https://panel.4sooapp.com/api/store/uploadpic');
                curl_setopt($ch_f, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch_f, CURLOPT_POSTFIELDS, $data_f);
                curl_setopt($ch_f, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch_f, CURLOPT_HTTPHEADER, array(
                'Content-Type: multipart/form-data',
                "Authorization: Bearer ".$Api_Token
                ));
                $result_f = curl_exec($ch_f);
                $err_f = curl_error($ch_f);
                $result_f = json_decode($result_f, true);
                curl_close($ch_f);
            }
            if($request->has('firstSellImg')){
                $firstSellImg = $request->firstSellImg;                        
                $data_a = array('type' => 'pp1','code'=>$uniqueCode ,'image'=> new \CurlFile($_FILES['firstSellImg']['tmp_name'],
                $_FILES['firstSellImg']['type'], $_FILES['firstSellImg']['name']));
                $ch_a = curl_init('https://panel.4sooapp.com/api/store/uploadpic');
                curl_setopt($ch_a, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch_a, CURLOPT_POSTFIELDS, $data_a);
                curl_setopt($ch_a, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch_a, CURLOPT_HTTPHEADER, array(
                'Content-Type: multipart/form-data',
                "Authorization: Bearer ".$Api_Token
                ));
                $result_a = curl_exec($ch_a);
                $err_a = curl_error($ch_a);
                $result_a = json_decode($result_a, true);
                curl_close($ch_a);
                // dd($result_f );
            }
            if($request->has('secondSellImg')){
                $secondSellImg = $request->secondSellImg;
                $data_b = array('type' => 'pp2','code'=>$uniqueCode ,'image'=> new \CurlFile($_FILES['secondSellImg']['tmp_name'],
                $_FILES['secondSellImg']['type'], $_FILES['secondSellImg']['name']));
                $ch_b = curl_init('https://panel.4sooapp.com/api/store/uploadpic');
                curl_setopt($ch_b, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch_b, CURLOPT_POSTFIELDS, $data_b);
                curl_setopt($ch_b, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch_b, CURLOPT_HTTPHEADER, array(
                'Content-Type: multipart/form-data',
                "Authorization: Bearer ".$Api_Token
                ));
                $result_b = curl_exec($ch_b);
                $err_b = curl_error($ch_b);
                $result_b = json_decode($result_b, true);
                curl_close($ch_b);
            } 
            if($request->has('thirdSellImg')){
                $thirdSellImg = $request->thirdSellImg;
                $data_c = array('type' => 'pp3','code'=>$uniqueCode ,'image'=> new \CurlFile($_FILES['thirdSellImg']['tmp_name'],
                $_FILES['thirdSellImg']['type'], $_FILES['thirdSellImg']['name']));
                $ch_c = curl_init('https://panel.4sooapp.com/api/store/uploadpic');
                curl_setopt($ch_c, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch_c, CURLOPT_POSTFIELDS, $data_c);
                curl_setopt($ch_c, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch_c, CURLOPT_HTTPHEADER, array(
                'Content-Type: multipart/form-data',
                "Authorization: Bearer ".$Api_Token
                ));
                $result_c = curl_exec($ch_c);
                $err_c = curl_error($ch_c);
                $result_c = json_decode($result_c, true);
                curl_close($ch_c);
            }
            return redirect(route('sales'));
        }
        if($sended !== null){
            $data = array('code'=>$uniqueCode , 'confirmcode'=>$deliverCode);
            $jsonData = json_encode($data);
            $ch = curl_init('https://panel.4sooapp.com/api/store/orderdeliver');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
            $result = curl_exec($ch);
            $err = curl_error($ch);
            $result = json_decode($result, true);
            curl_close($ch);
            // dd($result);
            $codeResponse = $result['code'];
            $error = $result['error'];
            if($codeResponse == '400'){
                return redirect(route('sales_details' , $uniqueCode))->with('error' , $error);
            }elseif($codeResponse == '200'){
                return redirect(route('sales'));
            }
        }
        
    }
    public function sells_archive(Request $request){
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        $ch = curl_init('https://panel.4sooapp.com/api/store/ordersap');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $result = curl_exec($ch);
        $err = curl_error($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        // dd($result);
        return view('sales.sells_archive',compact('result'));
    }
    public function sells_cancell(Request $request){
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        $code = $request->id;
        $datas = array('code'=>$code);
        $jsonDatas = json_encode($datas);
        $chs = curl_init('https://panel.4sooapp.com/api/store/ordercancelp');
        curl_setopt($chs, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($chs, CURLOPT_POSTFIELDS, $jsonDatas);
        curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chs, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $results = curl_exec($chs);
        $errs = curl_error($chs);
        $results = json_decode($results, true);
        curl_close($chs);
        if($results !== null){
            $message = 'سفارش ما با موفقیت لغو شد';
            return redirect(route('sales_details', $code))->with('success',$message);
        }
    }
    

}
