<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StoresController extends Controller
{
    public function store(Request $request)
    {
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        $chs = curl_init('https://panel.4sooapp.com/api/personal/getpersonalstore');
        curl_setopt($chs, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chs, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $results = curl_exec($chs);
        $errs = curl_error($chs);
        $results = json_decode($results, true);
        curl_close($chs);
        // dd($results);
        // $results = "error";
        return view('stores.store',compact('results'));
    }
    public function createStore(Request $request)
    {
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        $chs = curl_init('https://panel.4sooapp.com/api/personal/getpersonalstore');
        curl_setopt($chs, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chs, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $results = curl_exec($chs);
        $errs = curl_error($chs);
        $results = json_decode($results, true);
        curl_close($chs);
        // dd($results);
        if($results == "error"){
            return view('stores.storeCreate');
        }else{
            return redirect(route("store"));
        }
    }
    public function submitStore(Request $request)
    {
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        $data = array(
            'name'=>$request->name,
            'number'=>$request->number,
            'description'=>$request->description,
            'address'=>$request->address,
        );
        $jsonData = json_encode($data);
        $ch = curl_init('https://panel.4sooapp.com/api/personal/createpersonalstore');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $result = curl_exec($ch);
        $err = curl_error($ch);
        $result = json_decode($result, true);
        curl_close($ch);

        if($result["status"] == "ok"){
            $datap = array(
                'store_picture'=> new \CurlFile($_FILES['picture']['tmp_name'], $_FILES['picture']['type'], $_FILES['picture']['name']),
                'store_id' => $result["store"]["id"]
            );
            $chp = curl_init('https://panel.4sooapp.com/api/personal/uploadstorepicture');
            curl_setopt($chp, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($chp, CURLOPT_POSTFIELDS, $datap);
            curl_setopt($chp, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($chp, CURLOPT_HTTPHEADER, array(
            'Content-Type: multipart/form-data',
            "Authorization: Bearer ".$Api_Token
            ));
            $resultp = curl_exec($chp);
            $errp = curl_error($chp);
            $resultp = json_decode($resultp, true);
            curl_close($chp);

            return redirect(route("store"));
        }else{
            return back()->with("error","خطا , لطفا دوباره تلاش کنید.");
        }
    }
    public function changeStoreStatus(Request $request)
    {
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        $data = array(
            'status'=>$request->status
        );
        $jsonData = json_encode($data);
        $ch = curl_init('https://panel.4sooapp.com/api/personalstore/status');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $result = curl_exec($ch);
        $err = curl_error($ch);
        $result = json_decode($result, true);
        curl_close($ch);

        if($result == "error"){
            return back()->with("error","خطا , لطفا دوباره تلاش کنید.");
        }else{
            return redirect(route("store"))->with("success" , "انجام شد .");
        }
    }
    public function changeProductPrice(Request $request)
    {
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        if(empty($request->withoutoff)){
            return back()->with("error","فیلد قیمت بدون تخفیف الزامی است");
        }
        if(!is_null($request->withoff)){
            $mainprice = $request->withoff;
            $beforoffprice = $request->withoutoff;
        }else{
            $mainprice = $request->withoutoff;
            $beforoffprice = null;
        }
        $Api_Token= $request->cookie('TokenCode');
        $data = array(
            'product_id'=>$request->product_id,
            'new_price'=>$mainprice,
            'new_beforoff_price'=>$beforoffprice,
        );
        $jsonData = json_encode($data);
        $ch = curl_init('https://panel.4sooapp.com/api/Store/productchangePrice');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $result = curl_exec($ch);
        $err = curl_error($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        if(isset($result["code"])){
            return redirect(route("store"))->with("success" , "انجام شد .");
        }else{
            return back()->with("error","انجام نشد");
        }

    }
    public function changeProductStatus(Request $request)
    {
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        $data = array(
            'product_id'=>$request->product_id,
            'status'=>$request->status
        );
        $jsonData = json_encode($data);
        $ch = curl_init('https://panel.4sooapp.com/api/personal/setproductstatus');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $result = curl_exec($ch);
        $err = curl_error($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        if($result== "done"){
            return redirect(route("store"))->with("success" , "انجام شد .");
        }else{
            return back()->with("error","انجام نشد");
        }

    }
    public function storeCategories(Request $request)
    {
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        $chs = curl_init('https://panel.4sooapp.com/api/personal/getpersonalstore/categories');
        curl_setopt($chs, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chs, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $results = curl_exec($chs);
        $errs = curl_error($chs);
        $results = json_decode($results, true);
        curl_close($chs);
        // dd($results);
        return view("stores.storeCategories",compact('results'));
    }
    public function submitStoreCategories(Request $request)
    {
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        $jsonData = json_encode($request->all());
        $ch = curl_init('https://panel.4sooapp.com/api/personal/storecategories/editdata');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $result = curl_exec($ch);
        $err = curl_error($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        if($result == "ok"){
            return redirect(route("storeCategories"))->with("success" , "انجام شد .");
        }else{
            return back()->with("error","انجام نشد");
        }
    }
    public function storeWarehouses(Request $request)
    {
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        $chs = curl_init('https://panel.4sooapp.com/api/personal/getpersonalstore/categories');
        curl_setopt($chs, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chs, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $results = curl_exec($chs);
        $errs = curl_error($chs);
        $results = json_decode($results, true);
        curl_close($chs);
        // dd($results);
        return view("stores.storeWarehouses",compact('results'));
    }
    public function submitStoreWarehouses(Request $request)
    {
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        $jsonData = json_encode($request->all());
        $ch = curl_init('https://panel.4sooapp.com/api/personal/storewarehouses/editdata');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $result = curl_exec($ch);
        $err = curl_error($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        if($result == "ok"){
            return redirect(route("storeWarehouses"))->with("success" , "انجام شد .");
        }else{
            return back()->with("error","انجام نشد");
        }
    }
    public function createProduct(Request $request)
    {
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        $chs = curl_init('https://panel.4sooapp.com/api/personal/getpersonalstore');
        curl_setopt($chs, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chs, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $results = curl_exec($chs);
        $errs = curl_error($chs);
        $results = json_decode($results, true);
        curl_close($chs);
        // dd($results);
        return view('stores.productCreate' , compact('results'));
    }
    public function submitProduct(Request $request)
    {
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        if(!is_null($request->withoff_price)){
            $mainprice = $request->withoff_price;
            $beforoffprice = $request->withoutoff_price;
        }else{
            $mainprice = $request->withoutoff_price;
            $beforoffprice = null;
        }
        if($request->product_type == "primary_product"){
            $data = array(
                'product_name'=>$request->product_name,
                'product_price'=>$mainprice,
                'product_beforeoff_price'=>$beforoffprice,
                'product_description'=>$request->description,
                'type'=>"primary_product",
            );
        }else{
            $data = array(
                'product_name'=>$request->product_name,
                'product_price'=>$request->withoutoff_price,
                'product_beforeoff_price'=>null,
                'product_description'=>"",
                'type'=>"secondary_product",
            );
        }
        $jsonData = json_encode($data);
        $ch = curl_init('https://panel.4sooapp.com/api/Store/productcreate');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $result = curl_exec($ch);
        $err = curl_error($ch);
        $result = json_decode($result, true);
        curl_close($ch);

        if($result["code"] == 200){
            $datap = array(
                'image'=> new \CurlFile($_FILES['product_image']['tmp_name'], $_FILES['product_image']['type'], $_FILES['product_image']['name']),
                'product_edit_id' => $result["data"]
            );
            $chp = curl_init('https://panel.4sooapp.com/api/Store/uploadproductimage');
            curl_setopt($chp, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($chp, CURLOPT_POSTFIELDS, $datap);
            curl_setopt($chp, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($chp, CURLOPT_HTTPHEADER, array(
            'Content-Type: multipart/form-data',
            "Authorization: Bearer ".$Api_Token
            ));
            $resultp = curl_exec($chp);
            $errp = curl_error($chp);
            $resultp = json_decode($resultp, true);
            curl_close($chp);

            if($request->product_type == "primary_product"){
                if($request->hasFile("product_image1")){
                    $datap = array(
                        'image'=> new \CurlFile($_FILES['product_image1']['tmp_name'], $_FILES['product_image1']['type'], $_FILES['product_image1']['name']),
                        'product_edit_id' => $result["data"]
                    );
                    $chp = curl_init('https://panel.4sooapp.com/api/Store/uploadproductimagetwo');
                    curl_setopt($chp, CURLOPT_CUSTOMREQUEST, 'POST');
                    curl_setopt($chp, CURLOPT_POSTFIELDS, $datap);
                    curl_setopt($chp, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($chp, CURLOPT_HTTPHEADER, array(
                    'Content-Type: multipart/form-data',
                    "Authorization: Bearer ".$Api_Token
                    ));
                    $resultp = curl_exec($chp);
                    $errp = curl_error($chp);
                    $resultp = json_decode($resultp, true);
                    curl_close($chp);
                }
                if($request->hasFile("product_image2")){
                    $datap = array(
                        'image'=> new \CurlFile($_FILES['product_image2']['tmp_name'], $_FILES['product_image2']['type'], $_FILES['product_image2']['name']),
                        'product_edit_id' => $result["data"]
                    );
                    $chp = curl_init('https://panel.4sooapp.com/api/Store/uploadproductimagethree');
                    curl_setopt($chp, CURLOPT_CUSTOMREQUEST, 'POST');
                    curl_setopt($chp, CURLOPT_POSTFIELDS, $datap);
                    curl_setopt($chp, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($chp, CURLOPT_HTTPHEADER, array(
                    'Content-Type: multipart/form-data',
                    "Authorization: Bearer ".$Api_Token
                    ));
                    $resultp = curl_exec($chp);
                    $errp = curl_error($chp);
                    $resultp = json_decode($resultp, true);
                    curl_close($chp);
                }
            }


            $request->request->add(['product_id'=>$result["product_id"]]);
            // send other product details
            $detail_data = $request->all();
            $detail_jsonData = json_encode($detail_data);
            $detail_ch = curl_init('https://panel.4sooapp.com/api/Store/productedit/editdetails');
            curl_setopt($detail_ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($detail_ch, CURLOPT_POSTFIELDS, $detail_jsonData);
            curl_setopt($detail_ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($detail_ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
            $detail_result = curl_exec($detail_ch);
            $detail_err = curl_error($detail_ch);
            $detail_result = json_decode($detail_result, true);
            curl_close($detail_ch);

            return redirect(route("store"))->with("success",$result['message']);
        }else{
            return back()->with("error","خطا , لطفا دوباره تلاش کنید.");
        }
    }
    public function editProduct(Request $request)
    {
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        $chs = curl_init('https://panel.4sooapp.com/api/personal/getpersonalstore/getproduct?product_id='.$request->product_id);
        curl_setopt($chs, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chs, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $results = curl_exec($chs);
        $errs = curl_error($chs);
        $results = json_decode($results, true);
        curl_close($chs);
        // dd($results);
        return view('stores.productEdit',compact('results'));
    }
    public function submitEditProduct(Request $request)
    {
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        
        $image_submitted = 0;
        if($request->hasFile("product_image")){
            $image_submitted = 1;
        }
        if($request->hasFile("product_image1")){
            $image_submitted = 1;
        }
        if($request->hasFile("product_image2")){
            $image_submitted = 1;
        }

        if($request->product_type == "primary_product"){
            $data = array(
                'id'=>$request->product_id,
                'product_name'=>$request->product_name,
                'product_description'=>$request->description,
                'type'=>"primary_product",
                'image_submitted'=>$image_submitted,
            );
        }else{
            $data = array(
                'id'=>$request->product_id,
                'product_name'=>$request->product_name,
                'product_description'=>"",
                'type'=>"secondary_product",
                'image_submitted'=>$image_submitted,
            );
        }
        $jsonData = json_encode($data);
        $ch = curl_init('https://panel.4sooapp.com/api/Store/productedit');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $result = curl_exec($ch);
        $err = curl_error($ch);
        $result = json_decode($result, true);
        curl_close($ch);

        // send other product details
        $detail_data = $request->all();
        $detail_jsonData = json_encode($detail_data);
        $detail_ch = curl_init('https://panel.4sooapp.com/api/Store/productedit/editdetails');
        curl_setopt($detail_ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($detail_ch, CURLOPT_POSTFIELDS, $detail_jsonData);
        curl_setopt($detail_ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($detail_ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $detail_result = curl_exec($detail_ch);
        $detail_err = curl_error($detail_ch);
        $detail_result = json_decode($detail_result, true);
        curl_close($detail_ch);


        if($result["code"] == 200){
            if($request->hasFile("product_image")){
                $datap = array(
                    'image'=> new \CurlFile($_FILES['product_image']['tmp_name'], $_FILES['product_image']['type'], $_FILES['product_image']['name']),
                    'product_edit_id' => $result["data"]
                );
                $chp = curl_init('https://panel.4sooapp.com/api/Store/uploadproductimage');
                curl_setopt($chp, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($chp, CURLOPT_POSTFIELDS, $datap);
                curl_setopt($chp, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($chp, CURLOPT_HTTPHEADER, array(
                'Content-Type: multipart/form-data',
                "Authorization: Bearer ".$Api_Token
                ));
                $resultp = curl_exec($chp);
                $errp = curl_error($chp);
                $resultp = json_decode($resultp, true);
                curl_close($chp);
            }

            if($request->product_type == "primary_product"){
                if($request->hasFile("product_image1")){
                    $datap = array(
                        'image'=> new \CurlFile($_FILES['product_image1']['tmp_name'], $_FILES['product_image1']['type'], $_FILES['product_image1']['name']),
                        'product_edit_id' => $result["data"]
                    );
                    $chp = curl_init('https://panel.4sooapp.com/api/Store/uploadproductimagetwo');
                    curl_setopt($chp, CURLOPT_CUSTOMREQUEST, 'POST');
                    curl_setopt($chp, CURLOPT_POSTFIELDS, $datap);
                    curl_setopt($chp, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($chp, CURLOPT_HTTPHEADER, array(
                    'Content-Type: multipart/form-data',
                    "Authorization: Bearer ".$Api_Token
                    ));
                    $resultp = curl_exec($chp);
                    $errp = curl_error($chp);
                    $resultp = json_decode($resultp, true);
                    curl_close($chp);
                }
                if($request->hasFile("product_image2")){
                    $datap = array(
                        'image'=> new \CurlFile($_FILES['product_image2']['tmp_name'], $_FILES['product_image2']['type'], $_FILES['product_image2']['name']),
                        'product_edit_id' => $result["data"]
                    );
                    $chp = curl_init('https://panel.4sooapp.com/api/Store/uploadproductimagethree');
                    curl_setopt($chp, CURLOPT_CUSTOMREQUEST, 'POST');
                    curl_setopt($chp, CURLOPT_POSTFIELDS, $datap);
                    curl_setopt($chp, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($chp, CURLOPT_HTTPHEADER, array(
                    'Content-Type: multipart/form-data',
                    "Authorization: Bearer ".$Api_Token
                    ));
                    $resultp = curl_exec($chp);
                    $errp = curl_error($chp);
                    $resultp = json_decode($resultp, true);
                    curl_close($chp);
                }
            }
            return redirect(route("store"))->with("success",$result['message']);
        }elseif($result["code"] == 400){
            return redirect(route("store"))->with("success",$result['message']);
        }else{
            return back()->with("error","خطا , لطفا دوباره تلاش کنید.");
        }
    }
    public function deleteProduct(Request $request)
    {
        if(!$request->hasCookie('TokenCode')){
            return redirect(route('getPersonalNumber'));
        }
        $Api_Token= $request->cookie('TokenCode');
        
        $data = array(
            'product_id'=>$request->product_id
        );
        $jsonData = json_encode($data);
        $ch = curl_init('https://panel.4sooapp.com/api/Store/productdelete');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json','Authorization: Bearer '.$Api_Token));
        $result = curl_exec($ch);
        $err = curl_error($ch);
        $result = json_decode($result, true);
        curl_close($ch);

        if($result["code"] == 200){
            return redirect(route("store"))->with("success",$result['message']);
        }else{
            return back()->with("error","خطا , لطفا دوباره تلاش کنید.");
        }
    }
}