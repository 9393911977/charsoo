<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Auth;

class MainController extends Controller
{
    public function loader(Request $request){
        if ($request->hasCookie('TokenCode')) {
            return redirect(route('dashboard'));
        }else{
            return redirect(route('getPersonalNumber'));
        }
    }
    public function getPersonalNumber(Request $request){
        // if ($request->hasCookie('TokenCode')) {
        //     return redirect(route('dashboard'));
        // }
        return view('main.get_personal_number');
    }
    public function submitPersonalNumber(Request $request){
        if ($request->hasCookie('TokenCode')) {
            return redirect(route('dashboard'));
        }
        $phone = $request->phone;
        $data = array('phone' => $phone , 'app' => 'وب اپ خدمت رسان');
        $jsonData = json_encode($data);
        $ch = curl_init('https://panel.4sooapp.com/api/sendcode');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($jsonData)
        ));
        $result = curl_exec($ch);
        $err = curl_error($ch);
        $result = json_decode($result, true);
        curl_close($ch); 

        if($result=='ok'){
        $minutes = 5;       
        Cookie::queue(Cookie::make('phonerecived', $phone , $minutes));
        return redirect(route('getCode'));
        }else{
            return rediret(route('getPersonalNumber'));
        }
    }
    
    public function getCode(Request $request){
        if ($request->hasCookie('TokenCode')) {
            return redirect(route('dashboard'));
        }
        if($request->hasCookie('phonerecived')){
            $phone=$request->cookie('phonerecived');
            return view('main.get_code',compact('phone'));
        }else{
            return redirect(route('getPersonalNumber'));
        }
    }
    public function submitCode(Request $request){
        if ($request->hasCookie('TokenCode')) {
            return redirect(route('dashboard'));
        }
        $code = $request->code;
        $phone = $request->phone_number;
        $data = array('code' => $code ,'mobile' => $phone , 'fcmtoken' => '');
        $jsonData = json_encode($data);
        $ch = curl_init('https://panel.4sooapp.com/api/personal/verify');
        //$ch = curl_init('https://sandbox.zarinpal.com/pg/rest/WebGate/PaymentRequest.json');
        // curl_setopt($ch, CURLOPT_USERAGENT, 'ZarinPal Rest Api v1');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($jsonData)
        ));
        $result = curl_exec($ch);
        $err = curl_error($ch);
        $result = json_decode($result, true);
        curl_close($ch);

        if(isset($result['message'])){

            $msg = $result['message'];
            return redirect(route('getCode'))->with('error' , $msg);

        }elseif(isset($result['code'])){

            if (empty($result['code'])) {      
                Cookie::queue(Cookie::make('registerAllow', 'ok' , 5));
                return redirect(route('register'));
            }else{
                $minutes = 60*24*30*12*100;
                Cookie::queue(Cookie::make('TokenCode', $result['code'] , $minutes));
                return redirect(route('dashboard'));
            }
        } 

    }
    public function register(Request $request){
        if ($request->hasCookie('TokenCode')) {
            return redirect(route('dashboard'));
        }
        if (!$request->hasCookie('phonerecived')) {
            return redirect(route('getPersonalNumber'));
        }
        if($request->hasCookie('registerAllow')){
            $ch = curl_init('https://panel.4sooapp.com/api/getCities');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json',));
            $result = curl_exec($ch);
            $err = curl_error($ch);
            $result = json_decode($result, true);
            curl_close($ch);
            $phone= $request->cookie('phonerecived');
            return view('main.register',compact('result','phone'));
        }else{
            return redirect(route('getPersonalNumber'));
        }
        
        
    }

    public function submitRegister(Request $request){
        if ($request->hasCookie('TokenCode')) {
            return redirect(route('dashboard'));
        }
        $firstname = $request->firstname;
        $lastname = $request->lastname;
        $city = $request->cities;
        $phone = $request->number;
        $data = array('p_firstname' => $firstname ,'p_lastname' => $lastname ,'p_mobile' => $phone ,
        'p_city' => $city , 'fcmtoken' => '');
        $jsonData = json_encode($data);
        $ch = curl_init('https://panel.4sooapp.com/api/personal/register');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($jsonData)));
        $result = curl_exec($ch);
        $err = curl_error($ch);
        $result = json_decode($result, true);
        curl_close($ch);

        if(!empty($result['code'])){
            $minutes = 60*24*30*12*100;
            Cookie::queue(Cookie::make('TokenCode' , $result['code'],$minutes));
            return redirect(route('dashboard'));
        }else{
            return redirect(route('getPersonalNumber'));
        }

    }
    public function logout(Request $request)
    {
        if ($request->hasCookie('TokenCode')) {
            Cookie::queue(Cookie::forget('TokenCode'));
            return redirect(route('getPersonalNumber'));
        }

    }
}
