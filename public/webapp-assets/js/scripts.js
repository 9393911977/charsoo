$(document).ready(function(){

    $('.panel-open').click(function () {
      $('.sidebar').show();
      $('.sidebar').animate({
          right : '260px'
      },400);
      $('.panel-backdrop').show();
      $('.panel-backdrop').css('opacity','0.5');
      $('.sidebar').css('position','fixed');

      $('.panel-backdrop').click(function(){
        $('.sidebar').animate({
          right : '0px'
        },400 , function(){
          $('.sidebar').hide();
        });
      $('.sidebar').css('position','absolute');
      $(this).hide();
      });
    });
      



    $("#backLink").click(function(event) {
      event.preventDefault();
      history.back(1);
      // location.reload();
    });



      
    function reset() {
      alertify.set({
      labels : {
        ok     : "OK",
        cancel : "Cancel"
      },
      delay : 5000,
      buttonReverse : false,
      buttonFocus   : "ok"
    });
    }
      


    $("input#number").keyup(function(){
      val = $(this).val();
      string = val.toString();
      sliced = string.slice(0,2);
      if(sliced !== '0' && sliced !== '09'){
        $(this).val(null);
        reset();
      alertify.error('شماره وارد شده صحیح نمی باشد');
      return false;
      }
      if (string.length > 11) {
        $(this).val(null);
        reset();
      alertify.error('شماره وارد شده صحیح نمی باشد');
      return false;
      }
    });


    $(".thumb-picture").click(function(){
      imgsrc = $(this).attr('src');
      $(this).parents(".products-content").next().find('.main-picture').attr('src',imgsrc);
    });

    $(".invinput").click(function(){
      count = $(this).attr('data-inv');
      $(this).parents(".storeCatswrapper").find('.inv').html(count)
    });

    
    $(".openModal").click(function(){
      id = $(this).attr("data-id");
      $("#inputid").val(id);
      $(".my-modal").fadeIn();
    });
    $("#closeModal").click(function(event){
      $("#withoff").val("");
      $("#withoutoff").val("");
      if(event.target == this){
        $(".my-modal").hide();
      }
    });

    $(document).on('click','.clone-bottom',function(e){
      e.preventDefault()

      let cloned = $(this).siblings('.product-cate').clone()
      cloned.find('input[type="text"]').val('')
      $(this).prev('.clonecat').append(cloned)
    });

    $(document).on('click','.clone-inv',function(e){
      e.preventDefault()
      let cloned = $(this).siblings('.warehouse-detail').clone()
      cloned.find('input[type="text"]').val('')
      cloned.find('input[type="number"]').val('')
      $(this).prev('.clone').append(cloned)
    });

    $("#code_form").submit(function(){
      
      val = $("input#four_code").val();
      code = val.toString();
      if (code.length !== 4 ) {
        $(this).val(null);
        reset();
      alertify.error('شماره نباید بیشتر از چهار رقم باشد ');
      return false;
      }
      return true;
    });

    $("#registerForm").submit(function(){
      valname = $("input#firstname").val();
      valfamily = $("input#lastname").val();
      valcities = $("#cities").val();
      var check = $("#checkRegister").val();
      city = valcities.toString();
      name = valname.toString();
      lastname = valfamily.toString();
      if (name.length == 0 || lastname.length == 0 || city.length == 0) {
      alertify.error('فیلد ها نمی تواند خالی باشد.');
      return false;
      }
      if( $('input[type="checkbox"]').is(':checked')){
        var check = 1;
      }else{
        var check = 0;
      }
      if (check == 0){
        alertify.error('لطفا موافقت خود را اعلام کنید.');
        return false;
      }
      return true;
    });

    $("#accunt_form").submit(function(){
      val1 = $("input#shaba_number").val();
      val2 = $("input#accunt_name").val();
      val3 = $("input#bank_name").val();
      shaba_number = val1.toString();
      accunt_name = val2.toString();
      bank_name = val3.toString();
      if (shaba_number.length == 0 || accunt_name.length == 0 || bank_name.length == 0 ) {
        $(this).val(null);
        reset();
      alertify.error('فیلد ها نمی توانند خالی باشند');
      return false;
      }
      return true;
    });

    $("#deliverForm").submit(function(){
      val1 = $("input#deliverCode").val();
      deliver = val1.toString();
      if (deliver.length == 0) {
        $(this).val(null);
        reset();
      alertify.error('کد تایید تحویل را وارد کنید');
      return false;
      }
      return true;
    });
        
})

jQuery(document).ready(function(){
    jQuery('.scrollbar-macosx').scrollbar();
});

var slideIndex = 0;
showSlides();

function showSlides() {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";  
  }
  slideIndex++;
  if (slideIndex > slides.length) {slideIndex = 1}    
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
  setTimeout(showSlides, 20000); // Change image every 2 seconds


}

var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";
  }
  x[slideIndex-1].style.display = "block";

  for (i = 0; i < dots.length; i++) {
  dots[i].className = dots[i].className.replace(" active", "");
  }
  x[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
}